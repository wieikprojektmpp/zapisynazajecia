package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.SystemAnnouncement;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.SystemAnnouncementService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class SystemAnnouncementServiceImpl extends BaseServiceImpl implements SystemAnnouncementService {
    @Transactional(readOnly = false)
    public long createSystemAnnouncement(SystemAnnouncement announcement) throws ZNZException {
        try {
            return systemAnnouncementDao.create(announcement);
        } catch (Exception e) {
            LOGGER.error("Error creating system announcemet:" + announcement.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating system announcemet.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateSystemAnnouncement(SystemAnnouncement announcement) throws ZNZException {
        try {
            systemAnnouncementDao.update(announcement);
        } catch (Exception e) {
            LOGGER.error("Error updating system announcemet:" + announcement.toString()  + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating system announcemet.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSystemAnnouncement(SystemAnnouncement announcement) throws ZNZException {
        try {
            systemAnnouncementDao.delete(announcement);
        } catch (Exception e) {
            LOGGER.error("Error deleting system announcemet:" + announcement.toString()  + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting system announceme.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSystemAnnouncementById(long id) throws ZNZException {
        try {
            systemAnnouncementDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error getting system announcemet with id:" +id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting system announcemet.",e);
        }
    }

    public List<SystemAnnouncement> getAllSystemAnnouncements() throws ZNZException {
        try {
            return systemAnnouncementDao.getAll();
        } catch (Exception e) {
            LOGGER.error("Error getting all system announcements" + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting all system announcements.",e);
        }
    }

    public SystemAnnouncement getAnnouncement(long id) throws ZNZException {
        try {
            return systemAnnouncementDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting announcement with id" + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting announcement.",e);
        }
    }
}
