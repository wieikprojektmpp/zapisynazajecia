package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Subject;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.TermService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class TermServiceImpl extends BaseServiceImpl implements TermService {
    @Transactional(readOnly = false)
    public long createTerm(Term term) throws ZNZException {
        try {
            return termDao.create(term);
        } catch (Exception e) {
            LOGGER.error("Error creating term: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating term.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateTerm(Term term) throws ZNZException {
        try {
            termDao.update(term);
        } catch (Exception e) {
            LOGGER.error("Error updating term: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating term.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteTerm(Term term) throws ZNZException {
        try {
            termDao.delete(term);
        } catch (Exception e) {
            LOGGER.error("Error deleting term: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting term.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteTermById(long id) throws ZNZException {
        try {
            termDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting term with id:" + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting term.",e);
        }
    }

    public Term getTerm(long id) throws ZNZException {
        try {
            return termDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting term with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting term.",e);
        }
    }

    public List<Term> getTermsForSubject(Subject subject) throws ZNZException {
        try {
            return termDao.getTermsForSubject(subject);
        } catch (Exception e) {
            LOGGER.error("Error getting terms for subject: " + subject.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting terms for subject.",e);
        }
    }

}
