package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Argument;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.ArgumentService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class ArgumentServiceImpl extends BaseServiceImpl implements ArgumentService {
    @Transactional(readOnly = false)
    public long createArgument(Argument argument) throws ZNZException {
        try {
            return argumentDao.create(argument);
        } catch (Exception e) {
            LOGGER.error("Error creating argument: " + argument.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating argument.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateArgument(Argument argument) throws ZNZException {
        try {
            argumentDao.update(argument);
        } catch (Exception e) {
            LOGGER.error("Error updating argument: " + argument.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating argument.",e);
        }
    }

    public Argument getArgument(long id) throws ZNZException {
        try {
            return argumentDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting argument with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting argument.",e);
        }
    }

    public List<Argument> getArgumentsForSemester(Semester semester) throws ZNZException {
        try {
            return argumentDao.getArgumentsForSemester(semester);
        } catch (Exception e) {
            LOGGER.error("Error xxx" + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error xxx.",e);
        }
    }
}
