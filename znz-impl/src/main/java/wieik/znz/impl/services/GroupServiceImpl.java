package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.GroupService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class GroupServiceImpl extends BaseServiceImpl implements GroupService {
    @Transactional(readOnly = false)
    public long createGroup(Group group) throws ZNZException {
        try {
            return groupDao.create(group);
        } catch (Exception e) {
            LOGGER.error("Error creating group: " + group.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating group.", e);
        }
    }

    @Transactional(readOnly = false)
    public void updatGroup(Group group) throws ZNZException {
        try {
            groupDao.update(group);
        } catch (Exception e) {
            LOGGER.error("Error updating group: " + group.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating group.", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteGroup(Group group) throws ZNZException {
        try {
            groupDao.delete(group);
        } catch (Exception e) {
            LOGGER.error("Error deleting group: " + group.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting group.", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteGroupById(long id) throws ZNZException {
        try {
            groupDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting group with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting group.", e);
        }
    }

    public List<Group> getGroupsForStudent(Student student) throws ZNZException {
        try {
            return groupDao.getGroupsForStudent(student);
        } catch (Exception e) {
            LOGGER.error("Error finding groups for student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding groups for student.", e);
        }
    }

    public List<Group> getGroupsForYear(Year year) throws ZNZException {
        try {
            return groupDao.getGroupsForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error finding groups for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding groups for year.", e);
        }
    }

    public Group getGroup(long id) throws ZNZException {
        try {
            return groupDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding group with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding group.", e);
        }
    }

    @Transactional(readOnly = false)
    public void addStudentToGroup(Student student, Group group) throws ZNZException {
        try {
            groupDao.addStudentToGroup(student,group);
        } catch (Exception e) {
            LOGGER.error("Error adding student: " + student + " to group: " + group.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error adding student to group.", e);
        }
    }
}
