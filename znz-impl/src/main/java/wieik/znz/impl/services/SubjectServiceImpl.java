package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Subject;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.SubjectService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class SubjectServiceImpl extends BaseServiceImpl implements SubjectService {
    @Transactional(readOnly = false)
    public long createSubject(Subject subject) throws ZNZException {
        try {
            return subjectDao.create(subject);
        } catch (Exception e) {
            LOGGER.error("Error creating subject: " + subject.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating subject.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateSubject(Subject subject) throws ZNZException {
        try {
            subjectDao.update(subject);
        } catch (Exception e) {
            LOGGER.error("Error updating subject: " + subject.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating subject.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSubject(Subject subject) throws ZNZException {
        try {
            subjectDao.delete(subject);
        } catch (Exception e) {
            LOGGER.error("Error deleting subject: " + subject.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting subject.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSubjectById(long id) throws ZNZException {
        try {
            subjectDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting subject with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting subject.",e);
        }
    }

    public Subject getSubject(long id) throws ZNZException {
        try {
            return subjectDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting subject with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting subject.",e);
        }
    }

    public List<Subject> getSubjectsForSemester(Semester semester) throws ZNZException {
        try {
            return subjectDao.getSubjectsForSemester(semester);
        } catch (Exception e) {
            LOGGER.error("Error getting subjects for semester: " + semester.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting subjects for semester.",e);
        }
    }
}
