package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.*;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.genetic.GeneticAllocation;
import wieik.znz.genetic.phenotype.PhAllocation;
import wieik.znz.genetic.phenotype.PhSubject;
import wieik.znz.genetic.phenotype.PhTerm;
import wieik.znz.genetic.vo.ApplicationVO;
import wieik.znz.genetic.vo.SemesterVO;
import wieik.znz.genetic.vo.SubjectVO;
import wieik.znz.genetic.vo.TermVO;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-19.
 */

@Service
@Transactional(readOnly = true)
public class GeneticAllocationServiceImpl extends BaseServiceImpl implements wieik.znz.api.services.GeneticAllocationService {
    private final static int MAX_ITERATIONS = 100;

    private List<SubjectVO> makeSubjectVOList(Semester semester) {
        List<SubjectVO> subjectVOs = new LinkedList<SubjectVO>();
        List<Subject> subjects = new LinkedList<Subject>();
        subjects.addAll(semester.getPrzedmioty());

        // Tworzenie listy SubjectVO
        for (Subject subject : subjects) {
            List<Term> terms = new LinkedList<Term>();
            List<TermVO> termVOs = new LinkedList<TermVO>();
            terms.addAll(subject.getTerminy());

            // Tworzenie listy TermVO
            for (Term term : terms) {
                List<Application> applications = new LinkedList<Application>();
                applications.addAll(term.getZgloszenia());
                List<ApplicationVO> applicationVOs = new LinkedList<ApplicationVO>();

                // Tworzenie listy ApplicationVO
                for (Application application : applications) {
                    applicationVOs.add(new ApplicationVO(
                            application.getZgloszenieId(),
                            application.getStudentId().getStudentId(),
                            application.getTyp_wyboru(),
                            application.getArgumentId().getPriorytet()
                    ));
                }

                termVOs.add(new TermVO(
                        term.getTerminId(),
                        term.getDzien(),
                        term.getGodzRozp(),
                        term.getGodzZak(),
                        term.getRodzajTygodnia(),
                        applicationVOs
                ));
            }

            subjectVOs.add(new SubjectVO(subject.getPrzedmiotId(), termVOs, subject.getMinStudentow(), subject.getMaxStudentow()));
        }
        return subjectVOs;
    }



    @Transactional(readOnly = false)
    public void startAllocation(Semester semester) throws ZNZException{
        try {
            // Tworzenie listy id studentow
            List<Student> students = studentDao.getStudentsForYear(semester.getRokId());
            List<Long> studentsIds = new LinkedList<Long>();
            for (Student student : students) {
                studentsIds.add(student.getStudentId());
            }

            // Tworzenie SemesterVO
            SemesterVO semesterVO = new SemesterVO(semester.getSemestrId(), studentsIds, makeSubjectVOList(semester));

            // Tworzenie klasy genetycznego przydzialu
            GeneticAllocation genetic = new GeneticAllocation(semesterVO, 100);

            // EWOLUCJA
            List<PhSubject> phSubjects = genetic.evolve().getPhSubjects();

            // Zamiana wyniku ewolucji na obikty domeny
            List<Allocation> allocations = new LinkedList<Allocation>();
            for (PhSubject phSubject : phSubjects) {
                for (PhTerm phTerm : phSubject.getPhTerms()) {
                    for (PhAllocation phAllocation : phTerm.getPhAllocations()) {
                        Allocation allocation = new Allocation();
                        allocation.setCheciZmiany(null);
                        allocation.setTerminId(termDao.get(phTerm.getId()));
                        allocation.setFinalny(false);
                        allocation.setStudentId(studentDao.get(phAllocation.getStudentId()));

                        allocations.add(allocation);
                    }
                }
            }

            createAllocations(allocations);

        } catch (Exception e) {
            LOGGER.error("ERROR IN GENETIC ALLOCATION: " + e.toString());
            throw new ZNZException("Error in genetic allocation.", e);
        }
    }

    private void createAllocations(List<Allocation> allocations) {
        for(Allocation allocation : allocations) {
            allocationDao.create(allocation);
        }
    }
}
