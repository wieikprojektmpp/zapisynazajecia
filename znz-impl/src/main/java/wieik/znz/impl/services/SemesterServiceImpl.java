package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Term;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.SemesterService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class SemesterServiceImpl extends BaseServiceImpl implements SemesterService {
    @Transactional(readOnly = false)
    public long createSemester(Semester semester) throws ZNZException {
        try {
            return semesterDao.create(semester);
        } catch (Exception e) {
            LOGGER.error("Error creating semester: " + semester.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating semester.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateSemester(Semester semester) throws ZNZException {
        try {
            semesterDao.update(semester);
        } catch (Exception e) {
            LOGGER.error("Error updating semester: " + semester.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating semester.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSemester(Semester semester) throws ZNZException {
        try {
            semesterDao.delete(semester);
        } catch (Exception e) {
            LOGGER.error("Error deleting semester: " + semester.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting semester.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSemesterById(long id) throws ZNZException {
        try {
            semesterDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting semester with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting semester.",e);
        }
    }

    public Semester getSemester(long id) throws ZNZException {
        try {
            return semesterDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting semester with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting semester.",e);
        }
    }

    public Semester getNewestSemester(Year year) throws ZNZException {
        try {
            List<Semester> semesters = semesterDao.getSemestersForYear(year);
            int max = 0;
            Semester maxS = null;
            for(Semester s : semesters) {
                if (s.getNrSemestru() > max) {
                    max = s.getNrSemestru();
                    maxS = s;
                }
            }
            return maxS;
        } catch (Exception e) {
            LOGGER.error("Error getting newest semester for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting newest semester.",e);
        }
    }

    public List<Semester> getSemestersForYear(Year year) throws ZNZException {
        try {
            return semesterDao.getSemestersForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error getting semesters for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting semesters for year.",e);
        }
    }

    public long getSemesterIdByTerm(Term term) throws ZNZException {
        try {
            return semesterDao.getSemesterIdByTerm(term);
        } catch (Exception e) {
            LOGGER.error("Error getting semesters for year: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting semesters for year.",e);
        }
    }
}
