package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.YearService;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class YearServiceImpl extends BaseServiceImpl implements YearService {
    @Transactional(readOnly = false)
    public long createYear(Year year) throws ZNZException {
        try {
            return yearDao.create(year);
        } catch (Exception e) {
            LOGGER.error("Error creating year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating year.", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateYear(Year year) throws ZNZException {
        try {
            yearDao.update(year);
        } catch (Exception e) {
            LOGGER.error("Error updating year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating year.", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteYear(Year year) throws ZNZException {
        try {
            yearDao.delete(year);
        } catch (Exception e) {
            LOGGER.error("Error deleting year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting year.", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteYearById(long id) throws ZNZException {
        try {
            yearDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting year with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting year.", e);
        }
    }

    public Year getYear(long id) throws ZNZException {
        try {
            return yearDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding year with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding year.", e);
        }
    }

    public List<Year> getYearsForCourse(Course course) throws ZNZException {
        try {
            return yearDao.getYearsForCourse(course);
        } catch (Exception e) {
            LOGGER.error("Error finding years for course: " + course.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding years for course.", e);
        }
    }


    public List<Year> getYearsForStudent(Student student) throws ZNZException {
        try {
            List<Group> groups = groupDao.getGroupsForStudent(student);
            List<Year> years = new LinkedList<Year>();
            for (Group group : groups) {
                years.add(yearDao.getYearForGroup(group));
            }
            return years;
        } catch (Exception e) {
            LOGGER.error("Error finding years for student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding years for student.", e);
        }
    }

    public List<Year> getAllYears() throws ZNZException {
        try {
            return yearDao.getAll();
        } catch (Exception e) {
            LOGGER.error("Error geting all years." + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error geting all year.", e);
        }
    }

    public Year getYearBySemesterId(long id) throws ZNZException {
        try {
            return yearDao.getYearBySemesterId(id);
        } catch (Exception e) {
            LOGGER.error("Error finding year with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding year.", e);
        }
    }

    public long getYearForAnnouncementWithId(long id) throws ZNZException {
        try {
            return yearDao.getYearIdForAnnouncementWithId(id);
        } catch (Exception e) {
            LOGGER.error("Error finding year with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding year.", e);
        }
    }
}
