package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.SchoolService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class SchoolServiceImpl extends BaseServiceImpl implements SchoolService {
    public School getSchoolByName(String name) throws ZNZException {
        try {
            return schoolDao.findByName(name);
        } catch (Exception e) {
            LOGGER.error("Error finding school with name:" + name + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding school with name:" + name, e);
        }
    }

    @Transactional(readOnly = false)
    public long createSchool(School school) throws ZNZException {
        try {
            return schoolDao.create(school);
        } catch (Exception e) {
            LOGGER.error("Error creating school: " + school.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating school", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateSchool(School school) throws ZNZException {
        try {
            schoolDao.update(school);
        } catch (Exception e) {
            LOGGER.error("Error updating school:" + school.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating school:" + school.toString(), e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSchool(School school) throws ZNZException {
        try {
            schoolDao.delete(school);
        } catch (Exception e) {
            LOGGER.error("Error deleting school:" + school.toString() + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting school:" + school.toString(), e);
        }
    }

    public List<School> getAllSchools() throws ZNZException {
        try {
            return schoolDao.getAll();
        } catch (Exception e) {
            LOGGER.error("Error getting all schools. " + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting all schools. ", e);
        }
    }

    public School getSchool(long id) throws ZNZException {
        try {
            return schoolDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding school with id:" + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding school with id:" + id, e);
        }
    }

    public School getSchoolForDepartment(Department department) throws ZNZException {
        try {
            return schoolDao.getSchoolForDepartment(department);
        } catch (Exception e) {
            LOGGER.error("Error finding school for department: " + department.toString() +  "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error school  for department:", e);
        }
    }
}
