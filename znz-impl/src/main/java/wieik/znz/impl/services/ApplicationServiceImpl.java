package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Application;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.ApplicationService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class ApplicationServiceImpl extends BaseServiceImpl implements ApplicationService {
    @Transactional(readOnly = false)
    public void createApplications(List<Application> applications) throws ZNZException {
        Application ab = null;
        try {
            for (Application a : applications) {
                ab = a;
                applicationDao.create(a);
            }
        } catch (Exception e) {
            LOGGER.error("Error creating application: " + ab.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating applications.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateApplications(List<Application> applications) throws ZNZException {
        Application ap = null;
        try {
            for (Application a : applications) {
                ap = a;
                applicationDao.update(a);
            }
        } catch (Exception e) {
            LOGGER.error("Error updating application: " + ap.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating applications.",e);
        }
    }

    public List<Application> getApplicationsForTerm(Term term) throws ZNZException {
        try {
            return applicationDao.getApplicationsForTerm(term);
        } catch (Exception e) {
            LOGGER.error("Error getting applications for term: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting applications for term.",e);
        }
    }

    public List<Application> getApplicationForStudentInSemester(Student student, Semester semester) throws ZNZException {
        try {
            return applicationDao.getApplicationsForStudentInSemester(student, semester);
        } catch (Exception e) {
            LOGGER.error("Error getting applications for student: " + student.toString() + " in semester: " + semester.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting applications for student in semester.",e);
        }
    }

    public int coutApplicationForTerm(Term term) throws ZNZException {
        try {
            return applicationDao.countForTerm(term);
        } catch (Exception e) {
            LOGGER.error("Error xxx" + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error xxx.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteApplication(Application application) throws ZNZException {
        try {
            applicationDao.delete(application);
        } catch (Exception e) {
            LOGGER.error("Error deleting application: " + application.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting application.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteApplicationById(long id) throws ZNZException {
        try {
            applicationDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting application with id:" + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting application.",e);
        }
    }
}
