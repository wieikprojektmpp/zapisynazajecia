package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Moderator;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.ModeratorService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class ModeratorServiceImpl extends BaseServiceImpl implements ModeratorService {
    @Transactional(readOnly = false)
    public long createModerator(Moderator moderator) throws ZNZException {
        try {
            return moderatorDao.create(moderator);
        } catch (Exception e) {
            LOGGER.error("Error creating moderator: " + moderator.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating moderator.", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateModerator(Moderator moderator) throws ZNZException {
        try {
            moderatorDao.update(moderator);
        } catch (Exception e) {
            LOGGER.error("Error updating moderator: " + moderator.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating moderator.", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteModerator(Moderator moderator) throws ZNZException {
        try {
            moderatorDao.delete(moderator);
        } catch (Exception e) {
            LOGGER.error("Error deleting moderator: " + moderator.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting moderator.", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteModeratorById(long id) throws ZNZException {
        try {
            moderatorDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting moderator with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting moderator.", e);
        }
    }

    public List<Moderator> getModeratorForYear(Year year) throws ZNZException {
        try {
            return moderatorDao.getModeratorsForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error finding moderators for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding moderators for year.", e);
        }
    }

    public Moderator getModerator(long id) throws ZNZException {
        try {
            return moderatorDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding moderator with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding moderator.", e);
        }
    }

    public boolean isStudentModeratorOfYear(Student student, Year year) throws ZNZException {
        try {
            List<Moderator> moderators = moderatorDao.getModeratorsForYear(year);
            for (Moderator m : moderators) {
                Student s = studentDao.getModerator(m);
                if (s.getStudentId() == student.getStudentId()) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            LOGGER.error("Error checking is student a moderator for year" + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Errorchecking is student a moderator for year.", e);
        }
    }
}
