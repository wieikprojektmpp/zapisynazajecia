package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.SwapService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class SwapServiceImpl extends BaseServiceImpl implements SwapService {


    @Transactional(readOnly = false)
    public long createSwap(Swap swap) throws ZNZException {
        try {
            return swapDao.create(swap);
        } catch (Exception e) {
            LOGGER.error("Error creating swap: " + swap.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating swap.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateSwap(Swap swap) throws ZNZException {
        try {
            swapDao.update(swap);
        } catch (Exception e) {
            LOGGER.error("Error updating swap: " + swap.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating swap.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSwap(Swap swap) throws ZNZException {
        try {
            swapDao.delete(swap);
        } catch (Exception e) {
            LOGGER.error("Error deleting swap: " + swap.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting swap.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteSwapById(long id) throws ZNZException {
        try {
            swapDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting swap with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting swap.",e);
        }
    }

    public Swap getSwap(long id) throws ZNZException {
        try {
            return swapDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting swap with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting swap.",e);
        }
    }

    public List<Swap> getSwapsForStudent(Student student) throws ZNZException {
        try {
            return swapDao.getSwapsForStudent(student);
        } catch (Exception e) {
            LOGGER.error("Error getting swap for student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting swap.",e);
        }
    }

    public List<Swap> getSwapsForTerm(Term term) throws ZNZException {
        try {
            return swapDao.getSwapsForTerm(term);
        } catch (Exception e) {
            LOGGER.error("Error getting swap for term: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting swap for term.",e);
        }
    }

    public Swap getSwapForAllocation(Allocation allocation) throws ZNZException {
        try {
            return swapDao.getSwapForAllocation(allocation);
        } catch (Exception e) {
            LOGGER.error("Error getting swap for allocation: " + allocation.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting swap for allocation.",e);
        }
    }

    @Transactional(readOnly = false)
    public void realizeSwapsForTerm(Term term) throws ZNZException {
        try {
            // pobranie zamian dla terminu
            // zamiany NA termin
            List<Swap> swapsTo = swapDao.getFreeToSwapsForTerm(term);
            // zamiany Z terminu
            List<Swap> swapsFrom = swapDao.getFreeFromSwapsForTerm(term);
            if (swapsTo == null || swapsFrom == null || swapsTo.size() == 0 || swapsFrom.size() == 0)
                return;

            for (Swap swapTo : swapsTo) {
                int i = 0;
                // dopoki sa zamiany z terminu na ktory chcemy sie zapisac
                while (i < swapsFrom.size() && !swapsFrom.get(i).isZamkniete()) {
                    Swap swapFrom = swapsFrom.get(i);
                    // termin z ktorego zmienia s1
                    Term t1 = termDao.getFromTermForSwap(swapTo);
                    // termin na ktory zmienia s2
                    Term t2 = termDao.getToTermForSwap(swapFrom);

                    // jesli to te same terminy to zamieniamy
                    if (t1.getTerminId() == t2.getTerminId()) {

                        Student s1 = studentDao.getStudentForSwap(swapTo);
                        Student s2 = studentDao.getStudentForSwap(swapFrom);

                        Allocation a1 = allocationDao.getAllocationForSwap(swapTo);
                        Allocation a2 = allocationDao.getAllocationForSwap(swapFrom);
                        Allocation newA1 = new Allocation();
                        newA1.setStudentId(s1);
                        newA1.setTerminId(t2);
                        newA1.setFinalny(false);
                        Allocation newA2 = new Allocation();
                        newA2.setStudentId(s2);
                        newA2.setTerminId(term);
                        newA2.setFinalny(false);

                        // usun stare przydzialy
                        allocationDao.delete(a1);
                        allocationDao.delete(a2);
                        allocationDao.create(newA1);
                        allocationDao.create(newA2);

                        //zaktualizuj zamiany
                        swapTo.setZamkniete(true);
                        swapFrom.setZamkniete(true);
                        swapDao.update(swapTo);
                        swapDao.update(swapFrom);

                        // pozostale zamiany dla przydzialow zamykamy
                        List<Swap> restOfSwapsForAllocation = swapDao.getSwapsForAllocation(a1);
                        for (Swap s : restOfSwapsForAllocation) {
                            s.setZamkniete(true);
                            swapDao.update(s);
                        }

                        restOfSwapsForAllocation = swapDao.getSwapsForAllocation(a2);
                        for (Swap s : restOfSwapsForAllocation) {
                            s.setZamkniete(true);
                            swapDao.update(s);
                        }

                        i = swapsFrom.size();
                    }
                    ++i;
                }

            }


        } catch (Exception e) {
            LOGGER.error("Error realizing swaps " + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error realizing swaps.",e);
        }
    }
}
