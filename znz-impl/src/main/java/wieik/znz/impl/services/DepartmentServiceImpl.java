package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.DepartmentService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class DepartmentServiceImpl extends BaseServiceImpl implements DepartmentService {
    @Transactional(readOnly = false)
    public long createDepartment(Department department) throws ZNZException {
        try {
            return departmentDao.create(department);
        } catch (Exception e) {
            LOGGER.error("Error creating department: " + department.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating department", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateDepartment(Department department) throws ZNZException {
        try {
            departmentDao.update(department);
        } catch (Exception e) {
            LOGGER.error("Error updating department: " + department.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating department", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteDepartment(Department department) throws ZNZException {
        try {
            departmentDao.delete(department);
        } catch (Exception e) {
            LOGGER.error("Error deleting department: " + department.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting department", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteDepartmentByID(long id) throws ZNZException {
        try {
            departmentDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting department by id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting department", e);
        }
    }

    public List<Department> getDepartmentsForSchool(School school) throws ZNZException {
        try {
            return departmentDao.getDepartmentsForSchool(school);
        } catch (Exception e) {
            LOGGER.error("Error finding departments for school: " + school.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding departments for school", e);
        }
    }

    public Department getDepartment(long id) throws ZNZException {
        try {
            return departmentDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding department with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding department with id", e);
        }
    }

    public Department getDepartmentByNameAndSchoolId(String name, long schoolId) throws ZNZException {
        try {
            return departmentDao.findByNameAndSchoolId(name, schoolId);
        } catch (Exception e) {
            LOGGER.error("Error finding department by name: " + name + " for school with id: " + schoolId + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding department by name and school id", e);
        }
    }

    public Department getDepartmentForCourse(Course course) throws ZNZException{
        try {
            return departmentDao.getDepartmentForCourse(course);
        } catch (Exception e) {
            LOGGER.error("Error finding department for course: " + course.toString() +  "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding department for course:", e);
        }
    }
}
