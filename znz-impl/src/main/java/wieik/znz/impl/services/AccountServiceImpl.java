package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import wieik.znz.api.domain.Account;
import wieik.znz.api.domain.Student;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.AccountService;
import wieik.znz.dao.AccountDao;

/**
 * Created by Sebastian on 2014-10-31.
 */
@Service
@Transactional(readOnly = true)
public class AccountServiceImpl extends BaseServiceImpl implements AccountService {

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Transactional(readOnly = false)
    public boolean registerAccount(Account account, String password, Errors errors, Student student) throws ZNZException{
        try {

            validateUsername(account.getUsername(), errors);
            boolean valid = !errors.hasErrors();
            if (valid) {
                LOGGER.debug("dane prawidlowe");
                Student studentFromDB = studentDao.getByName(student.getImie(), student.getNazwisko());
                if (studentFromDB!=null) {
                    student = studentFromDB;
                }
                else {
                    student.setPrzydzialy(null);
                    student.setZgloszenia(null);
                    long id = studentDao.create(student);
                    student = studentDao.get(id);
                }
                account.setStudent(student);
                accountDao.create(account, password);
            }
            return valid;
        } catch ( Exception e) {
            LOGGER.error("Error registering account: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error registering account", e);
        }
    }

    private void validateUsername(String username, Errors errors) {
        if (accountDao.findByUsername(username) != null) {
            errors.rejectValue("username", "error.duplicate", new String[] { username }, null);
            LOGGER.debug("Nazwa uzytkownika zajeta");
        }
    }
}
