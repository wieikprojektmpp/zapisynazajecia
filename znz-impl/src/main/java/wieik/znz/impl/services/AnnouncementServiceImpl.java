package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Announcement;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.AnnouncementService;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class AnnouncementServiceImpl extends BaseServiceImpl implements AnnouncementService {
    @Transactional(readOnly = false)
    public long createAnnouncement(Announcement announcement) throws ZNZException {
        try {
            return announcementDao.create(announcement);
        } catch (Exception e) {
            LOGGER.error("Error creating announcement: " + announcement.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating announcement", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateAnnouncement(Announcement announcement) throws ZNZException {
        try {
            announcementDao.update(announcement);
        } catch (Exception e) {
            LOGGER.error("Error updating announcement: " + announcement.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating announcement", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteAnnouncement(Announcement announcement) throws ZNZException {
        try {
            announcementDao.delete(announcement);
        } catch (Exception e) {
            LOGGER.error("Error deleting announcement: " + announcement.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting announcement", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteAnnouncementById(long id) throws ZNZException {
        try {
            announcementDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting announcement with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting announcement", e);
        }
    }

    public List<Announcement> getAnnouncementForYear(Year year) throws ZNZException {
        try {
            return announcementDao.getAnnouncementsForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error finding announcements for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding announcement", e);
        }
    }

    public List<Announcement> getAnnouncementForStudent(Student student) throws ZNZException {
        try {
            List<Group> groups = groupDao.getGroupsForStudent(student);
            List<Announcement> announcements = new LinkedList<Announcement>();
            for (Group group : groups) {
                Year year = yearDao.getYearForGroup(group);
                announcements.addAll(announcementDao.getAnnouncementsForYear(year));
            }
            return announcements;
        } catch (Exception e) {
            LOGGER.error("Error finding announcements for student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding announcement", e);
        }
    }

    public Announcement getAnnouncement(long id) throws ZNZException {
        try {
            return announcementDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding announcement with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding announcement", e);
        }
    }

    public long getYearIdForAnnouncement(Announcement announcement) throws ZNZException {
        try {
            return announcementDao.getYearIdForAnnouncement(announcement);
        } catch (Exception e) {
            LOGGER.error("Error finding announcement: " + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding announcement", e);
        }
    }
}
