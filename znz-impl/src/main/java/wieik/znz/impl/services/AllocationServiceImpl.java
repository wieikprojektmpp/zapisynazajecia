package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.NotFoundException;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.AllocationService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class AllocationServiceImpl extends BaseServiceImpl implements AllocationService {

    @Transactional(readOnly = false)
    public long createAllocation(Allocation allocation) throws ZNZException {
        try {
            return allocationDao.create(allocation);
        } catch (Exception e) {
            LOGGER.error("Error creating allocation: " + allocation.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating allocation", e);
        }
    }

    public Allocation getAllocation(long id) throws ZNZException {
        Allocation allocation = null;
        try {
            allocation = allocationDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding allocation with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding allocation", e);
        }
        if (allocation == null) {
            throw new NotFoundException("Cannot fing allocation with id: " + id);
        }
        return allocation;
    }

    public List<Allocation> getAllForTerm(Term term) throws ZNZException {
        try {
            return allocationDao.getAllForTerm(term);
        } catch (Exception e) {
            LOGGER.error("Error finding allocations for term: " + term.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding allocations", e);
        }
    }

    public List<Allocation> getAllForStudent(Student student) throws ZNZException {
        try {
            return allocationDao.getAllForStudent(student);
        } catch (Exception e) {
            LOGGER.error("Error finding allocations for student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding allocations", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateAllocation(Allocation allocation) throws ZNZException {
        try {
            allocationDao.update(allocation);
        } catch (Exception e) {
            LOGGER.error("Error updating allocation: " + allocation.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating allocation", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteAllocation(Allocation allocation) throws ZNZException {
        try {
            allocationDao.delete(allocation);
        } catch (Exception e) {
            LOGGER.error("Error deleting allocation: " + allocation.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting allocation", e);
        }

    }

    @Transactional(readOnly = false)
    public void deleteAllocationById(long id) throws ZNZException {
        try {
            allocationDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting allocation with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting allocation", e);
        }
    }


}
