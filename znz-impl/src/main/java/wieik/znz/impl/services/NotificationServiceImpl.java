package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Notification;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.NotificationService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class NotificationServiceImpl extends BaseServiceImpl implements NotificationService {
    @Transactional(readOnly = false)
    public long createNotification(Notification notification) throws ZNZException {
        try {
            return notificationDao.create(notification);
        } catch (Exception e) {
            LOGGER.error("Error creating notification: " + e.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating notification.",e);
        }
    }

    @Transactional(readOnly = false)
    public void updateNotification(Notification notification) throws ZNZException {
        try {
            notificationDao.update(notification);
        } catch (Exception e) {
            LOGGER.error("Error updating notification:" + notification.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating notification.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteNotification(Notification notification) throws ZNZException {
        try {
            notificationDao.delete(notification);
        } catch (Exception e) {
            LOGGER.error("Error deleting notification: " + notification.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting notification.",e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteNotificationById(long id) throws ZNZException {
        try {
            notificationDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting notification with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting notification.",e);
        }
    }

    public List<Notification> getNotificationsForYear(Year year) throws ZNZException {
        try {
            return notificationDao.getNotificationsForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error getting notifications for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting notifications for year.",e);
        }
    }

    public Notification getNotification(long id) throws ZNZException {
        try {
            return notificationDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error getting notification with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error getting notification.",e);
        }
    }
}
