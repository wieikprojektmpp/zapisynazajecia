package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.CourseService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class CourseServiceImpl extends BaseServiceImpl implements CourseService {
    @Transactional(readOnly = false)
    public long createCourse(Course course) throws ZNZException {
        try {
            return courseDao.create(course);
        } catch (Exception e) {
            LOGGER.error("Error creating course: " + course.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating course", e);
        }
    }

    @Transactional(readOnly = false)
    public void updateCourse(Course course) throws ZNZException {
        try {
            courseDao.update(course);
        } catch (Exception e) {
            LOGGER.error("Error updating course: " + course.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating course", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteCourse(Course course) throws ZNZException {
        try {
            courseDao.delete(course);
        } catch (Exception e) {
            LOGGER.error("Error deleting course: " + course.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting course", e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteCourseByID(long id) throws ZNZException {
        try {
            courseDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting course with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting course", e);
        }

    }

    public List<Course> getCoursesForDepartment(Department department) throws ZNZException {
        try {
            return courseDao.getCoursesForDepartment(department);
        }catch (Exception e) {
            LOGGER.error("Error finding courses for department: " + department.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding courses for department", e);
        }
    }

    public Course getCourse(long id) throws ZNZException {
        try {
            return courseDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding course with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding course", e);
        }
    }

    public Course getCourseByNameAndDepartmentId(String courseName, long departmentId) throws ZNZException {
        try {
            return courseDao.findByNameAndDepartmentId(courseName, departmentId);
        } catch (Exception e) {
            LOGGER.error("Error finding course with name: " + courseName + " for departmentId: " + departmentId + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding course", e);
        }
    }

    public Course getCourseForYear(Year year) throws ZNZException{
        try {
            return courseDao.getCourseForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error finding course for year: " + year + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding course", e);
        }
    }
}
