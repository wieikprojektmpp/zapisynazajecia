package wieik.znz.impl.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wieik.znz.dao.*;

import javax.inject.Inject;

/**
 * Created by Sebastian on 2014-10-31.
 */
public class BaseServiceImpl {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Inject
    protected AccountDao accountDao;

    @Inject
    protected AllocationDao allocationDao;

    @Inject
    protected AnnouncementDao announcementDao;

    @Inject
    protected ApplicationDao applicationDao;

    @Inject
    protected ArgumentDao argumentDao;

    @Inject
    protected CourseDao courseDao;

    @Inject
    protected DepartmentDao departmentDao;

    @Inject
    protected GroupDao groupDao;

    @Inject
    protected ModeratorDao moderatorDao;

    @Inject
    protected NotificationDao notificationDao;

    @Inject
    protected SchoolDao schoolDao;

    @Inject
    protected SemesterDao semesterDao;

    @Inject
    protected StudentDao studentDao;

    @Inject
    protected SubjectDao subjectDao;

    @Inject
    protected SwapDao swapDao;

    @Inject
    protected SystemAnnouncementDao systemAnnouncementDao;

    @Inject
    protected TermDao termDao;

    @Inject
    protected YearDao yearDao;

    public AccountDao getAccountDao() {
        return accountDao;
    }

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public AllocationDao getAllocationDao() {
        return allocationDao;
    }

    public void setAllocationDao(AllocationDao allocationDao) {
        this.allocationDao = allocationDao;
    }

    public AnnouncementDao getAnnouncementDao() {
        return announcementDao;
    }

    public void setAnnouncementDao(AnnouncementDao announcementDao) {
        this.announcementDao = announcementDao;
    }

    public ApplicationDao getApplicationDao() {
        return applicationDao;
    }

    public void setApplicationDao(ApplicationDao applicationDao) {
        this.applicationDao = applicationDao;
    }

    public ArgumentDao getArgumentDao() {
        return argumentDao;
    }

    public void setArgumentDao(ArgumentDao argumentDao) {
        this.argumentDao = argumentDao;
    }

    public CourseDao getCourseDao() {
        return courseDao;
    }

    public void setCourseDao(CourseDao courseDao) {
        this.courseDao = courseDao;
    }

    public DepartmentDao getDepartmentDao() {
        return departmentDao;
    }

    public void setDepartmentDao(DepartmentDao departmentDao) {
        this.departmentDao = departmentDao;
    }

    public GroupDao getGroupDao() {
        return groupDao;
    }

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public ModeratorDao getModeratorDao() {
        return moderatorDao;
    }

    public void setModeratorDao(ModeratorDao moderatorDao) {
        this.moderatorDao = moderatorDao;
    }

    public NotificationDao getNotificationDao() {
        return notificationDao;
    }

    public void setNotificationDao(NotificationDao notificationDao) {
        this.notificationDao = notificationDao;
    }

    public SchoolDao getSchoolDao() {
        return schoolDao;
    }

    public void setSchoolDao(SchoolDao schoolDao) {
        this.schoolDao = schoolDao;
    }

    public SemesterDao getSemesterDao() {
        return semesterDao;
    }

    public void setSemesterDao(SemesterDao semesterDao) {
        this.semesterDao = semesterDao;
    }

    public StudentDao getStudentDao() {
        return studentDao;
    }

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public SubjectDao getSubjectDao() {
        return subjectDao;
    }

    public void setSubjectDao(SubjectDao subjectDao) {
        this.subjectDao = subjectDao;
    }

    public SwapDao getSwapDao() {
        return swapDao;
    }

    public void setSwapDao(SwapDao swapDao) {
        this.swapDao = swapDao;
    }

    public SystemAnnouncementDao getSystemAnnouncementDao() {
        return systemAnnouncementDao;
    }

    public void setSystemAnnouncementDao(SystemAnnouncementDao systemAnnouncementDao) {
        this.systemAnnouncementDao = systemAnnouncementDao;
    }

    public TermDao getTermDao() {
        return termDao;
    }

    public void setTermDao(TermDao termDao) {
        this.termDao = termDao;
    }

    public YearDao getYearDao() {
        return yearDao;
    }

    public void setYearDao(YearDao yearDao) {
        this.yearDao = yearDao;
    }
}
