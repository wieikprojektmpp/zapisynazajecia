package wieik.znz.impl.services;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.StudentService;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
@Service
@Transactional(readOnly = true)
public class StudentServiceImpl extends BaseServiceImpl implements StudentService {

    public Student getStudentByUsername(String username) throws ZNZException {
        try {
            return studentDao.getStudentByUsername(username);
        } catch (Exception e) {
            LOGGER.error("Error finding student by username: " + username + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding student by username: " + username , e);
        }
    }

    @Transactional(readOnly = false)
    public long createStudent(Student student) throws ZNZException {
        try {
            return studentDao.create(student);
        } catch (Exception e) {
            LOGGER.error("Error creating student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error creating student" + student.toString(), e);
        }
    }

    @Transactional(readOnly = false)
    public void updateStudent(Student student) throws ZNZException {
        try {
            studentDao.update(student);
        } catch (Exception e) {
            LOGGER.error("Error updating student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error updating student" + student.toString(), e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteStudent(Student student) throws ZNZException {
        try {
            studentDao.delete(student);
        } catch (Exception e) {
            LOGGER.error("Error deleting student: " + student.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting student: " + student.toString(), e);
        }
    }

    @Transactional(readOnly = false)
    public void deleteStudentById(long id) throws ZNZException {
        try {
            studentDao.deleteById(id);
        } catch (Exception e) {
            LOGGER.error("Error deleting student with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error deleting student with id: " + id , e);
        }
    }

    public Student getStudent(long id) throws ZNZException {
        try {
            return studentDao.get(id);
        } catch (Exception e) {
            LOGGER.error("Error finding student with id: " + id + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding student with id: " + id , e);
        }
    }

    public List<Student> getStudentsForGroup(Group group) throws ZNZException {
        try {
            return studentDao.getStudentsForGroup(group);
        } catch (Exception e) {
            LOGGER.error("Error finding students for group: " + group.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding students for group: " + group.toString(), e);
        }
    }

    public List<Student> getStudentsForYear(Year year) throws ZNZException {
        try {
            return studentDao.getStudentsForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error finding students for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error finding students for year: " + year.toString(), e);
        }
    }

    public boolean isInYear(Student student,  Year year) throws ZNZException {
        try {
            List<Student> students = studentDao.getStudentsForYear(year);
            for (Student s : students) {
                if (s.getStudentId() == student.getStudentId())
                    return true;
            }
            return false;
        } catch (Exception e) {
            LOGGER.error("Error checking is student in year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error checking is student in year: " + year.toString(), e);
        }
    }

    public int countStudentsForYear(Year year) throws ZNZException {
        try {
            return studentDao.countStudentsForYear(year);
        } catch (Exception e) {
            LOGGER.error("Error counting students for year: " + year.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error counting students for year: " + year.toString(), e);
        }
    }

    public Student getStudentForAllocation(Allocation a) throws ZNZException {
        try {
            return studentDao.getStudentForAllocation(a);
        } catch (Exception e) {
            LOGGER.error("Error counting students for year: " + a.toString() + "\nError: " + e.toString());
            e.printStackTrace();
            throw new ZNZException("Error counting students for year:", e);
        }
    }
}
