<html>
    <head>
        <title><spring:message code="loginRequired.pageTitle" /></title>
    <head>
    <body>
        <%@ include file="subhead.jspf" %>
        <h1><spring:message code="loginRequired.pageTitle" /></h1>
    </body>
</html>