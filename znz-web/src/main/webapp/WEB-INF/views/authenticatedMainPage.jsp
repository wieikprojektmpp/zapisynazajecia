<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">

<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="authenticatedMainPage.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
 <%@ include file="subhead.jspf" %>
    


    <div class="container">
        <br><br><div class="well "><br><br>
  <div class="row"> 
    <div class="col-md-6">  
<div class="panel panel-info">
 <div class="panel-heading"><h4>Ogloszenia</h4></div>
 <div class="panel-body">

    <c:forEach var="announcement" items="${announcements}">

        <h2><c:out value="${announcement.nazwa}"/></h2><br>
        <c:out value="${announcement.tresc}"/><br>
        <c:out value="${announcement.dataPublikacji}"/><br><br> <hr>
    </c:forEach>
</div></div></div>
<div class="col-md-6"> 
    <h2><spring:message code="authenticatedMainPage.yoursGroups" /></h2>
    <br>
    <div class="list-group">
    <c:forEach var="year" items="${yearsMap}">
            <a class="list-group-item list-group-item-success" href="grupa?id=<c:out value="${year.key}" />"><c:out value="${year.value}" /></a>
            <br>
        </c:forEach></div>
<br>
    <a class="btn btn-info btn-lg" href="dolacz_do_grupy"><spring:message code="authenticatedMainPage.joinGroup" /></a>
</div>
</div>

</div>
</div>

</body>
</html>