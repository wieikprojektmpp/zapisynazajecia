<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>
<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="xxx.pageTitle" />
<spring:message var="submit" code="xxx.label.submit" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}


.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;

  
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}


</style>



<%@ include file="subhead.jspf" %>
    <h1><c:out value="${yearName}" /></h1><br><br>


    <form:form action="${submitUrl}" modelAttribute="form" class="form-signin" >
    <spring:message code="xxx.label.title" /><br>
    <form:input path="title" class="form-control" /><br>
    <spring:message code="xxx.label.content" /><br>
    <form:input path="content" class="form-control" rows="6" /><br>
    <input type="submit" class="btn btn-info btn-lg btn-primary btn-block" value="${submit}"></input>
    </form:form>



</body>
</html>