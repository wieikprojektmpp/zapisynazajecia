<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>
<c:url var="submitRegistrationUrl" value="/registration.html" />

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="registrationForm.pageTitle" />
<spring:message var="msgAllFieldsRequired" code="registrationForm.message.allFieldsRequired" />
<spring:message var="register" code="registrationForm.label.register" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>

<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}


</style>


<div class="container">


   

    <form:form action="${submitRegistrationUrl}" modelAttribute="accountForm" class="form-signin">
     <h2 class="form-signin-heading"><c:out value="${pageTitle}" /></h2>
   	    <form:errors path="*">
   			<spring:message code="error.global" />
   		</form:errors>
        <br/>
   		<spring:message code="registrationForm.label.username" />
   		<form:input path="username" class="form-control"  />
   		<form:errors path="username" htmlEscape="false" />

   		<spring:message code="registrationForm.label.firstName" />
           		<form:input path="firstName" class="form-control"/>
           		<form:errors path="firstName" htmlEscape="false" />

           		<spring:message code="registrationForm.label.lastName" />
                   		<form:input path="lastName" class="form-control"/>
                   		<form:errors path="lastName" htmlEscape="false" />
        <spring:message code="registrationForm.label.password" />
        <form:password path="password" class="form-control" showPassword="false" />
    	<form:errors path="password" htmlEscape="false" />

        
        <spring:message code="registrationForm.label.confirmPassword" />
    	<form:password path="confirmPassword" class="form-control" showPassword="false"/>
        
   		<spring:message code="registrationForm.label.email" />
   		<form:input path="email" class="form-control" />
   		<form:errors path="email"  htmlEscape="false" />

        
   		<form:checkbox id="reminder" path="reminder" />
   		<label for="reminder"><spring:message code="registrationForm.label.reminder" /></label>
        <br/>
   		<form:checkbox id="notification" path="notification" />
   		<label for="notification"><spring:message code="registrationForm.label.notification" /></label>
        <br/>
        <input type="submit" class="btn btn-lg btn-primary btn-block" value="${register}"></input>
	</form:form>
</div>
</body>
</html>