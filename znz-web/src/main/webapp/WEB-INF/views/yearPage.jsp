<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>
<spring:message var="submit" code="yearPage.label.submit" />

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="yearPage.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<style>
.bottom-buffer { margin-bottom:300px; }

</style>
</style>


<%@ include file="subhead.jspf" %>
  <div class="container">
        <br><br><div class="well ">
        

    <h1><c:out value="${pageTitle}" /></h1>
    <c:if test="${moderator}">
        <a class="btn btn-warning btn-lg" href="zarzadzanie_grupa?id=${year.rokId}"><spring:message code="yearPage.yearAdministration" /></a><br><br>
    </c:if>

<div class="row">
    <div class="col-md-6">  
<div class="panel panel-primary">
    <div class="panel-heading"><spring:message code="yearPage.announcements" /></div>
      <div class="panel-body">
     <c:forEach var="announcement" items="${announcements}">
  
            <h3><c:out value="${announcement.nazwa}"/></h3><br>
           
            <c:out value="${announcement.tresc}"/><br><br>
            <strong><spring:message code="yearPage.announcement.publicationDate" /></strong><br>
            <c:out value="${announcement.dataPublikacji}"/><hr><br>
        </c:forEach></div></div></div>
        
  <div class="col-md-6">  
<div class="panel panel-info">
  <div class="panel-heading">Dane grupy</div>
  <ul class="list-group">
        <li class="list-group-item"><spring:message code="yearPage.startYear" /><br>
         <c:out value="${year.rokRozp}"/><br></li>
          <li class="list-group-item"><spring:message code="yearPage.endYear" /><br>
         <c:out value="${year.rokZak}"/><br></li>
          <li class="list-group-item"><spring:message code="yearPage.password" /><br>
         <c:out value="${year.password}"/><br></li></ul>
</div>

<spring:message code="yearPage.registation" />
<c:if test="${not empty semester}">
<c:choose>
    <c:when test="${state == 0}">
       <spring:message code="yearPage.registation.A" /><br>
       <c:out value="${semester.rozpoczecieZapisow}"/><br>
    </c:when>
    <c:when test="${state == 1}">
           <spring:message code="yearPage.registation.B" /><br>
           <c:out value="${semester.zakonczenieZapisow}"/><br>
           <a class="btn btn-lg btn-success" href="zapisy?id=${semester.semestrId}"><spring:message code="yearPage.registation.link" /></a><br>
        </c:when>
    <c:when test="${state == 2}">
           <spring:message code="yearPage.registation.C" /><br>
           <c:out value="${semester.zakonczenieZamian}"/><br>
           <a class="btn btn-lg btn-success" href="wyniki?id=${semester.semestrId}"><spring:message code="yearPage.results" /></a><br>
        </c:when>
    <c:when test="${state == 3}">
           <spring:message code="yearPage.registation.D" /><br>
          <a class="btn btn-lg btn-success" href="wyniki?id=${semester.semestrId}"><spring:message code="yearPage.results" /></a><br>
        </c:when>
    <c:when test="${state == 4}">
           <spring:message code="yearPage.registation.F" /><br>
           <a class="btn btn-lg btn-success"href="wyniki?id=${semester.semestrId}"><spring:message code="yearPage.results" /></a><br>
        </c:when>
    <c:when test="${state == 5}">
           <spring:message code="yearPage.registation.G" /><br>
           <a class="btn btn-lg btn-success" href="wyniki?id=${semester.semestrId}"><spring:message code="yearPage.results" /></a><br>
        </c:when>
</c:choose>
</c:if>

</div>
</div>
</div>
</div>
</div>
</body>
</html>