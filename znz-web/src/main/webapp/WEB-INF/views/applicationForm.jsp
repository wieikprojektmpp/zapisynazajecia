<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="xxx.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<%@ include file="subhead.jspf" %>
    <h1><c:out value="${pageTitle}" /></h1><br><br>

<form:form action="/ZNZ/zapisy" modelAttribute="form" method="post">
    <form:hidden path="studentId"/>
    <form:hidden path="id"/>
<c:forEach items="${form.subjects}" var="subject" varStatus="gridRow">
    <form:hidden path="subjects[${gridRow.index}].id"/>
    Przedmiot <c:out value="${form.subjects[gridRow.index].name}" /><br>
    Opis <c:out value="${form.subjects[gridRow.index].desc}" /><br>
    min <c:out value="${form.subjects[gridRow.index].min}" /><br>
    max <c:out value="${form.subjects[gridRow.index].max}" /><br>
    Termin1:<form:select path="subjects[${gridRow.index}].term1"  items="${form.subjects[gridRow.index].terms}" /><br>
    Argument:<form:select path="subjects[${gridRow.index}].arg1"  items="${form.arguments}" /><br>

    <form:radiobutton path="subjects[${gridRow.index}].type2" value="0" />jesli nie to
    <form:radiobutton path="subjects[${gridRow.index}].type2" value="1" />lub
    Termin2:<form:select path="subjects[${gridRow.index}].term2"  items="${form.subjects[gridRow.index].terms}" /><br>
    Argument:<form:select path="subjects[${gridRow.index}].arg2"  items="${form.arguments}" /><br>

    <form:radiobutton path="subjects[${gridRow.index}].type3" value="0" />jesli nie to
    <form:radiobutton path="subjects[${gridRow.index}].type3" value="1" />lub
    Termin3:<form:select path="subjects[${gridRow.index}].term3"  items="${form.subjects[gridRow.index].terms}" /><br>
    Argument:<form:select path="subjects[${gridRow.index}].arg3"  items="${form.arguments}" /><br>


</c:forEach>


<input type="submit"/>
</form:form>



</body>
</html>