<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="xxx.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<%@ include file="subhead.jspf" %>
    <h1><c:out value="${pageTitle}" /></h1><br><br>

    <%-- TABELKA,studenci w jednym wierszu --%>

    <c:forEach items="${semester.subjects}" var="subject" varStatus="subjectRow">
        <c:out value="${subject.name}" /><br/>
        <c:forEach items="${subject.terms}" var="term" varStatus="termRow">
            <c:out value="${term.name}" />
            <c:forEach items="${term.students}" var="student" varStatus="studentRow">
                <c:out value="${student}" />,
            </c:forEach>
            <br/>
        </c:forEach>
        <br/>
        <%-- formularz zamian --%>
        <c:if test="${state == 2}">
            <form:form action="wyniki/chec_zamiany" modelAttribute="semester.subjects[${subjectRow.index}].swapForm" method="POST" commandName="swapForm">
                <form:hidden path="studentId" />
                <form:hidden path="fromAllocationId" />
                <form:select path="toTermId" items="${semester.subjects[subjectRow.index].availableTerms}" />
                <input type="submit" value="${submit}">
            </form:form>

        </c:if>
    </c:forEach>



</body>
</html>