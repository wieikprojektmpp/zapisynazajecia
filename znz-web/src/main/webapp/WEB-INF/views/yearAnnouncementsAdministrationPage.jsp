<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="yearAnnouncementsAdministrationPage.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<style>

</style>

<%@ include file="subhead.jspf" %>
    

    <%-- MENU --%>

        <div class="container">
        <br><br><div class="well ">
<h1><c:out value="${yearName}" /></h1><br><br>
  <ul class="nav nav-tabs">
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.mainPage" /></a></li>
<li role="presentation" class="active"><a href=""><spring:message code="yearAdministrationPage.announcemets" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/powiadomienia?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.notifications" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/starosci?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.moderators" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/semestry?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.semesters" /></a></li>
 </ul>
    <%-- TRESC --%>
<br><br>
<%-- OGLOSZENIA --%>
<a class="btn btn-success btn-lg btn-primary " href="/ZNZ/zarzadzanie_grupa/formularz_ogloszenia?rokId=<c:out value="${yearId}" />">Nowe ogloszenie</a><br><br>
<br>

<div class="row">
    <div class="col-md-6">  

<div class="panel panel-primary">
     <c:forEach var="announcement" items="${announcements}">
     
    <div class="panel-heading">  <strong><spring:message code="yearPage.announcement.name" /></strong>

           <c:out value="${announcement.nazwa}"/><br></div>
            <strong><spring:message code="yearPage.announcement.content" /><br></strong>
            <c:out value="${announcement.tresc}"/><br>
            <strong><spring:message code="yearPage.announcement.publicationDate" /></strong><br>
            <c:out value="${announcement.dataPublikacji}"/><br><br>
        </c:forEach></div>
        <br><br>
</div>
<%-- TABELA --%>

  <div class="col-md-6">  
<div class="panel panel-info">
 <div class="panel-heading">Edycja ogloszen</div>
<c:forEach var="announcement" items="${announcements}">
<li class="list-group-item"><strong><c:out value="${announcement.nazwa}"/></strong> |
<c:out value="${announcement.dataPublikacji}"/> 
<a class="label label-warning btn pull-right " href="/ZNZ/zarzadzanie_grupa/formularz_ogloszenia?id=<c:out value="${announcement.ogloszenieId}" />">Edytuj</a> 
<a class="label label-danger btn pull-right " href="/ZNZ/zarzadzanie_grupa/usun_ogloszenie?id=<c:out value="${announcement.ogloszenieId}" />">Usun</a></li><br>

</c:forEach>
</div>
</div>
</div>
</div>
</div>
</body>
</html>