<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">

<%-- zmienne --%>


<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="xxx.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>

<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}



.form-signin {
  max-width: 340px;
  padding: 15px;
  margin: 0 auto;

  
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}






</style>









<%@ include file="subhead.jspf" %>
    


    <%-- FORMULARZ --%>
    <form:form action="${postUrl}" modelAttribute="semesterForm" method="post" class="form-signin">
<h2 class="form-signin-heading"><c:out value="${pageTitle}" /></h2><br><br>
    <c:forEach items="${semesterForm.subjects}" var="subject" varStatus="gridRow">

        <h4>Przedmiot <c:out value="${gridRow.index}"/></h4><br>
<form:hidden path="subjects[${gridRow.index}].id"/>
        Nazwa:
        <form:input path="subjects[${gridRow.index}].name" class="form-control"/><br>

        Opis:
        <form:input path="subjects[${gridRow.index}].description" class="form-control"/><br>

        Min studentow:
        <form:input path="subjects[${gridRow.index}].minStudents" class="form-control"/><br>

        Max studentow:
        <form:input path="subjects[${gridRow.index}].maxStudents" class="form-control"/><br>

        <c:forEach items="${subject.terms}" var="term" varStatus="termRow"><br>

            <h4>Termin <c:out value="${termRow.index}"/></h4><br>
    <form:hidden path="subjects[${gridRow.index}].terms[${termRow.index}].id"/>
             Dzien tygodnia:
             <form:input path="subjects[${gridRow.index}].terms[${termRow.index}].day" class="pull-right"/>
<br><br>
             godz rozpoczecia:
             <form:input path="subjects[${gridRow.index}].terms[${termRow.index}].starts" class="pull-right"/>
<br><br>
             godz rozpoczecia:
             <form:input path="subjects[${gridRow.index}].terms[${termRow.index}].ends" class="pull-right"/>
<br><br>
            Parzysty/nieparzysty
             <form:input path="subjects[${gridRow.index}].terms[${termRow.index}].week" class="pull-right"/>

<br><br><hr><br>
        </c:forEach>

    </c:forEach>
    <input type="submit" class="btn btn-lg btn-primary btn-block"/>
    </form:form>

    <%-- KONIEC --%>
</body>
</html>