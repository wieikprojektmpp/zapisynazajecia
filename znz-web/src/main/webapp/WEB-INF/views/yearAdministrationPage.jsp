<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="yearAdministrationPage.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<style>
.list-group{width: 50%;}
</style>	
<%@ include file="subhead.jspf" %>
   

    <%-- MENU --%>
    <div class="container">
        <br><br><div class="well ">
         <h1><c:out value="${yearName}" /></h1><br><br>
    <ul class="nav nav-tabs">
<li role="presentation" class="active"><a href=""><spring:message code="yearAdministrationPage.mainPage" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/ogloszenia?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.announcemets" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/powiadomienia?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.notifications" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/starosci?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.moderators" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/semestry?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.semesters" /></a></li>
</ul>

    <%-- TRESC --%>
<br><br>

<ul class="list-group">

  <li class="list-group-item"><spring:message code="yearAdministrationPage.studentsNumber" /> <span class="badge"><c:out value="${studentsNumber}" /></span></li><br>

  <li class="list-group-item"><spring:message code="yearAdministrationPage.semesterNumber" /><span class="badge"><c:out value="${semesterNumber}" /></span></li><br>

  <li class="list-group-item list-group-item-success"><spring:message code="yearAdministrationPage.password" /> : <c:out value="${password}" /></li><br>
  </ul>
</div>
</div>


</body>
</html>