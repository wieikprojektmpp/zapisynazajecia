<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>
<c:url var="submitYearForm" value="/dolacz_do_grupy.html" />


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>

<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}


.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;

  
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}


</style>


<body>
<%@ include file="subhead.jspf" %>
   
<div class="container">

<spring:message var="submit" code="joinGroupForm.label.submit" />

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="joinGroupForm.pageTitle" />



    <form:form action="${submitYearForm}" modelAttribute="form" class="form-signin" role="form">
 <h2 class="form-signin-heading"><c:out value="${pageTitle}" /></h2><br><br>

        <spring:message code="joinGroupForm.label.selectYear" />
        <form:select path="groupId" class="form-control"  items="${years}" />
        <br>
        <spring:message code="joinGroupForm.label.password"  />
        <form:input path="password" class="form-control"/>
        <br>
        <input type="submit" class="btn btn-lg btn-primary btn-block" value="${submit}"></input><br>
        <a class="btn btn-info btn-lg btn-primary btn-block" href="nowa_grupa"><spring:message code="joinGroupForm.newYear" /></a>
    </form:form>
</div>


</body>
</html>