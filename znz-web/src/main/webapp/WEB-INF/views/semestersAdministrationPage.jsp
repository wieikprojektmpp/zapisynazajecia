<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">

<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="xxx.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<style>
select.form-control{width: 30%;}
</style>

<%@ include file="subhead.jspf" %>
    

    <%-- MENU --%>
 <div class="container">

        <br><br><div class="well ">
        <h1><c:out value="${pageTitle}" /></h1><br><br>
      <ul class="nav nav-tabs">
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.mainPage" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/ogloszenia?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.announcemets" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/powiadomienia?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.notifications" /></a></li>
<li role="presentation"><a href="/ZNZ/zarzadzanie_grupa/starosci?id=<c:out value="${yearId}" />"><spring:message code="yearAdministrationPage.moderators" /></a></li>
<li role="presentation" class="active" ><a href=""><spring:message code="yearAdministrationPage.semesters" /></a></li>
</ul>
    <%-- TRESC --%>

<br><br>
<%-- TABELA --%>




  


<table class="table table-bordered">
      <thead>
        <tr >
          <th>semestr</th>
          <th>stan</th>
          <th>edycja zapisow</th>
          <th>plan</th>
          <th>nastepny stan</th>
          <th>argumenty</th>
        </tr>
      </thead>
      <tbody>
        <tr class="success">
<c:forEach var="semester" items="${semesters}">


    <td><c:out value="${semester.nrSemestru}"/></td>
    <c:choose>
        <c:when test="${semester.stan == 0}">
        <td>W przygotowaniu</td>
        <td><a href="/ZNZ/zarzadzanie_grupa/edytuj_semestr?yearId=<c:out value="${yearId}" />&id=<c:out value="${semester.semestrId}"/>">Edycja</a></td>
        </td>-</td>
         <td><a href="/ZNZ/zarzadzanie_grupa/semestr_nast_stan?yearId<c:out value="${yearId}" />&id=<c:out value="${semester.semestrId}"/>">Rozpocznij zapisy</a></td>
         <td><a href="/ZNZ/zarzadzanie_grupa/argumenty?semId=<c:out value="${semester.semestrId}"/>">Argumenty</a></td>
        </c:when>

        <c:when test="${semester.stan == 1}">
        <td>Zapisy rozpoczete</td>
        <td>-</td> 
        <td>-</td>
         <td><a href="/ZNZ/zarzadzanie_grupa/semestr_nast_stan?id=<c:out value="${semester.semestrId}"/>">Zakoncz zapisy</a></td>
        </c:when>

        <c:when test="${semester.stan == 2}">
        <td>Zapisy zakonczone</td>
        <td>-</td>
        <td><a href="/ZNZ/zarzadzanie_grupa/plan_semestr?id=<c:out value="${semester.semestrId}"/>">Plan(edycja)</a></td>
         <td><a href="/ZNZ/zarzadzanie_grupa/semestr_nast_stan?id=<c:out value="${semester.semestrId}"/>">Zakoncz zamiany</a><td>
        </c:when>

        <c:when test="${semester.stan == 3}">
         <td>Zamiany zakonczone, moderacja</td>
        </td>-</td>
        <td><a href="/ZNZ/zarzadzanie_grupa/plan_semestr?id=<c:out value="${semester.semestrId}"/>">Plan(edycja)</a></td>
         <td><a href="/ZNZ/zarzadzanie_grupa/semestr_nast_stan?id=<c:out value="${semester.semestrId}"/>">Zakoncz zamiany</a></td>
        </c:when>

        <c:when test="${semester.stan == 4}">
        <td>Semestr trwa</td> 
        <td>-</td>
        <td><a href="/ZNZ/zarzadzanie_grupa/plan_semestr?id=<c:out value="${semester.semestrId}"/>">Plan(podglad)</a></td> 
        <td><a href="/ZNZ/zarzadzanie_grupa/semestr_nast_stan?id=<c:out value="${semester.semestrId}"/>">Zakoncz semestr</a></td>
        </c:when>

        <c:when test="${semester.stan == 5}">
        <td>Semestr zakonczony</td>  <td>-</td>
        <td>- </td>
        <td><a href="/ZNZ/zarzadzanie_grupa/plan_semestr?id=<c:out value="${semester.semestrId}"/>">Plan(podglad)</a></td> <td>
        -</td>
        </c:when>

    </c:choose>

</c:forEach>
</tr>
      </tbody>
    </table>

<br>
<c:if test="${canCreateNewSemester}">
<form action="/ZNZ/zarzadzanie_grupa/nowy_semestr" method="get" >
  <input type="hidden" name="id" value="<c:out value="${yearId}" />"><br>
  Liczba przedmiotow: <select name="liczbaPrzedmiotow" class="form-control">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">7</option>
                      <option value="7">8</option>
                      <option value="8">9</option>
                      </select>
                      <br>
  <input type="submit" class="btn btn-success btn-lg btn-primary" value="Stworz">
</form>

</c:if>

<c:if test="${not canCreateNewSemester}">
Nie mozna utworzyc nowego semestru gdy poprzedni trwa.
</c:if>

</div>
</div>
</body>
</html>