<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%-- zmienne --%>

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="xxx.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>
<body>
<%@ include file="subhead.jspf" %>
    <h1><c:out value="${pageTitle}" /></h1><br><br>

    <c:forEach var="argument" items="${arguments}">
    nazwa: <c:out value="${argument.nazwa}" /> |
    priorytet: <c:out value="${argument.priorytet}" /> |
    <a href="/ZNZ/zarzadzanie_grupa/argument?id=<c:out value="${argument.argumentId}" />">edytuj</a>
    <br>
    </c:forEach>
    <br>
    <a href="/ZNZ/zarzadzanie_grupa/nowy_argument?semId=<c:out value="${semId}" />">nowy</a>



</body>
</html>