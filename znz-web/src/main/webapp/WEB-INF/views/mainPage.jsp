<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- zmienne --%>
<c:url var="submitAnonymousRegistrationForm" value="/registration.html" />

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="mainPage.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>


</head>
<body>
<style>

body {
  padding-top: 50px;

}
.row{margin-bottom: 100px;}
select.form-control{ width:80%; margin-bottom: 20px; }

input.form-control{ width:80%; }

.top-buffer { margin-top:20px; }

</style>

<%@ include file="subhead.jspf" %>
   
<div class="jumbotron">
  <div class="container">
       <div class="row">
        <div class="col-md-7">    
        <h1><spring:message code="Zapisy na zajecia" /></h1>
        <p>Witaj w serwisie internetowych zapisow na zajecia!
          Aby utworzyc zapisy dla swojej grupy zaloguj sie lub zarejestruj, jesli jeszcze nie posiadasz konta.
          Jesli chcesz sie zapisac na zajecia mozesz sie zalogowac lub po prostu wybierz swoja grupe oraz podaj haslo podane przez Twojego staroste.</p>
        <p><a class="btn btn-primary btn-lg"role="button" href="${registration}"><spring:message code="subhead.register" /></a>
           </div>
          


        <div class="col-md-5">

    <form:form action="${submitAnonymousRegistrationForm}" modelAttribute="anonymousMainPageForm" class="form-signin" role="form">
        <h4><spring:message code="mainPage.label.selectYear" /></h4>
        <form:errors path="*">
            <spring:message code="error.global" />
        </form:errors>
        <form:select path="year" class="form-control"  items="${years}" />
        <form:errors path="year" htmlEscape="false" />

        <h4><spring:message code="mainPage.label.firstName" /></h4>
        <form:input path="firstName" class="form-control"/>
        <form:errors path="firstName" htmlEscape="false" />

        <h4><spring:message code="mainPage.label.lastName" /></h4>
        <form:input path="lastName" class="form-control"/>
        <form:errors path="lastName" htmlEscape="false" />

        <h4><spring:message code="mainPage.label.password" /><h4>
         <form:input path="password" class="form-control" /> 
        <form:errors path="password" htmlEscape="false" />
        <div class="top-buffer">
        <input type="submit" class="btn btn-info btn-lg" value="<spring:message code="mainPage.label.submit" />"></input>
    </div>
    </form:form>
      </div>
       </div>
        
        </div>
          
          </div>
</body>
</html>