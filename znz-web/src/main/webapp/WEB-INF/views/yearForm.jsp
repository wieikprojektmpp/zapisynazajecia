<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%-- Uzywane biblioteki --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<link href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>" rel="stylesheet">
<%-- zmienne --%>
<c:url var="submitYearForm" value="/nowa_grupa.html" />
<spring:message var="submit" code="yearForm.label.submit" />

<%-- zmienne na tresc strony w odpowiednim jezyku --%>
<spring:message var="pageTitle" code="yearForm.pageTitle" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><c:out value="${pageTitle}" /></title>
</head>

<style>
body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}



.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;

  
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}









</style>





<body>
<%@ include file="subhead.jspf" %>

<div class="container">
    <form:form action="${submitYearForm}" modelAttribute="yearForm" class="form-signin" role="form">
    <h2 class="form-signin-heading"><c:out value="${pageTitle}" /></h2>

 <br>
        <spring:message code="yearForm.label.schoolName" />
        <br>
        <form:input path="schoolName" class="form-control"/>
        <br>
        <spring:message code="yearForm.label.departmentName" />
        <br>
        <form:input path="departmentName" class="form-control"/>
        <br>
        <spring:message code="yearForm.label.courseName" />
        <br>
        <form:input path="courseName" class="form-control"/>
        <br>
        <spring:message code="yearForm.label.startYear" />
        <br>
        <form:select path="startYear" items="${years}" class="form-control" />
        <br>
        <spring:message code="yearForm.label.endYear" />
        <br>
        <form:select path="endYear" items="${years}" class="form-control" />
        <br>
        <input type="submit"  class="btn btn-lg btn-primary btn-block" value="${submit}"></input>
    </form:form>

</div>

</body>
</html>