package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class ArgumentForm {
    @NotEmpty
    @Size(min = 3, max = 50)
    String argument;

    @Size(min = 1, max = 10)
    int priority;

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
