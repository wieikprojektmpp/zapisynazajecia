package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * Created by Sebastian on 2015-01-08.
 */
public class AnonymousMainPageForm {
    @NotNull
    long year;

    @NotEmpty
    @Size(min = 1, max = 50)
    String firstName;

    @NotEmpty
    @Size(min = 1, max = 50)
    String lastName;

    @NotEmpty
    @Size(min = 5, max = 50)
    String password;

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
