package wieik.znz.web.form;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class AddingStudentToTermForm {
    int studentId;
    int termId;

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getTermId() {
        return termId;
    }

    public void setTermId(int termId) {
        this.termId = termId;
    }
}
