package wieik.znz.web.form;

/**
 * Created by Sebastian on 2015-01-28.
 */
public class SwapForm {
    private long studentId;
    private long fromAllocationId;
    private long toTermId;

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getFromAllocationId() {
        return fromAllocationId;
    }

    public void setFromAllocationId(long fromAllocationId) {
        this.fromAllocationId = fromAllocationId;
    }

    public long getToTermId() {
        return toTermId;
    }

    public void setToTermId(long toTermId) {
        this.toTermId = toTermId;
    }
}
