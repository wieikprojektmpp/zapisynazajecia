package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class ModeratorForm {
    @NotEmpty
    @Size(min = 1, max = 50)
    String firstName;

    @NotEmpty
    @Size(min = 1, max = 50)
    String lasttName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLasttName() {
        return lasttName;
    }

    public void setLasttName(String lasttName) {
        this.lasttName = lasttName;
    }
}
