package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class PersonalDataForm {
    @NotEmpty
    @Size(min = 1, max = 50)
    String firstName;

    @NotEmpty
    @Size(min = 1, max = 50)
    String lastName;

    private boolean reminder;
    private boolean notification;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public boolean isReminder() {
        return reminder;
    }

    public void setReminder(boolean reminder) {
        this.reminder = reminder;
    }

    public boolean isNotification() {
        return notification;
    }

    public void setNotification(boolean notification) {
        this.notification = notification;
    }
}
