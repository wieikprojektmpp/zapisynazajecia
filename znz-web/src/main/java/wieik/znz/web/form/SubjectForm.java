package wieik.znz.web.form;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-24.
 */
public class SubjectForm {
    private long id;
    private String name;
    private String description;
    private int minStudents;
    private int maxStudents;
    private List<TermForm> terms;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinStudents() {
        return minStudents;
    }

    public void setMinStudents(int minStudents) {
        this.minStudents = minStudents;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public List<TermForm> getTerms() {
        return terms;
    }

    public void setTerms(List<TermForm> terms) {
        this.terms = terms;
    }
}
