package wieik.znz.web.form;

import wieik.znz.api.domain.Subject;

import java.util.Map;

/**
 * Created by Sebastian on 2015-01-25.
 */
public class SubjectApplicationForm {
    private long id;
    private String name;
    private String desc;
    private int min;
    private int max;
    private long term1;
    private long arg1;
    private int type2;
    private long term2;
    private long arg2;
    private int type3;
    private long term3;
    private long arg3;

    Map<Long, String> terms;

    public SubjectApplicationForm() {
        type2 = 0;
        type3 = 0;
    }

    public SubjectApplicationForm(Subject subject, Map<Long, String> terms, long id) {
        this.id = subject.getPrzedmiotId();
        name = subject.getNazwa();
        desc = subject.getDodatkoweInfo();
        min = subject.getMinStudentow();
        max = subject.getMaxStudentow();
        this.terms = terms;
        type2 = 0;
        type3 = 0;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public long getTerm1() {
        return term1;
    }

    public void setTerm1(long term1) {
        this.term1 = term1;
    }

    public long getArg1() {
        return arg1;
    }

    public void setArg1(long arg1) {
        this.arg1 = arg1;
    }

    public int getType2() {
        return type2;
    }

    public void setType2(int type2) {
        this.type2 = type2;
    }

    public long getTerm2() {
        return term2;
    }

    public void setTerm2(long term2) {
        this.term2 = term2;
    }

    public long getArg2() {
        return arg2;
    }

    public void setArg2(long arg2) {
        this.arg2 = arg2;
    }

    public int getType3() {
        return type3;
    }

    public void setType3(int type3) {
        this.type3 = type3;
    }

    public long getTerm3() {
        return term3;
    }

    public void setTerm3(long term3) {
        this.term3 = term3;
    }

    public long getArg3() {
        return arg3;
    }

    public void setArg3(long arg3) {
        this.arg3 = arg3;
    }

    public Map<Long, String> getTerms() {
        return terms;
    }

    public void setTerms(Map<Long, String> terms) {
        this.terms = terms;
    }
}
