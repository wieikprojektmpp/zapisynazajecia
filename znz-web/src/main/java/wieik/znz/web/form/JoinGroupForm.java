package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class JoinGroupForm {
    int groupId;

    @NotEmpty
    @Size(min = 1, max = 50)
    String password;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
