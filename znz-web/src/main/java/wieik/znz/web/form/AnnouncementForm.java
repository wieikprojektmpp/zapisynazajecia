package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class AnnouncementForm {

    private long announcementId;
    private long yearId;

    @NotEmpty
    @Size(min = 5, max = 50)
    String title;

    @NotEmpty
    @Size(min = 5, max = 255)
    String content;


    public long getYearId() {
        return yearId;
    }

    public long getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(long announcementId) {
        this.announcementId = announcementId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
