package wieik.znz.web.form;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class ChangeTermForm {
    int oldTermId;
    int newTermId;

    public int getOldTermId() {
        return oldTermId;
    }

    public void setOldTermId(int oldTermId) {
        this.oldTermId = oldTermId;
    }

    public int getNewTermId() {
        return newTermId;
    }

    public void setNewTermId(int newTermId) {
        this.newTermId = newTermId;
    }
}
