package wieik.znz.web.form;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class SwapStudentsForm {
    int term1Id;
    int student1Id;
    int term2Id;
    int student2Id;

    public int getTerm1Id() {
        return term1Id;
    }

    public void setTerm1Id(int term1Id) {
        this.term1Id = term1Id;
    }

    public int getStudent1Id() {
        return student1Id;
    }

    public void setStudent1Id(int student1Id) {
        this.student1Id = student1Id;
    }

    public int getTerm2Id() {
        return term2Id;
    }

    public void setTerm2Id(int term2Id) {
        this.term2Id = term2Id;
    }

    public int getStudent2Id() {
        return student2Id;
    }

    public void setStudent2Id(int student2Id) {
        this.student2Id = student2Id;
    }
}
