package wieik.znz.web.form;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class RegistrationForm {
    public static class Subject {
        int term1Id;
        int arg1Id;
        int opt1;
        int term2Id;
        int arg2Id;
        int opt2;
        int term3Id;
        int arg3Id;
    }

    private List<Subject> subjects;

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
