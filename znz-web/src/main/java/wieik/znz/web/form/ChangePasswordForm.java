package wieik.znz.web.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2015-01-08.
 */
@ScriptAssert(
        lang = "javascript",
        script = "_this.confirmPassword != null && _this.confirmPassword.equals(_this.password)",
        message = "account.password.mismatch.message")
public class ChangePasswordForm {
    @NotEmpty
    @Size(min = 1, max = 50)
    private String oldPassword;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String password;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String confirmPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
