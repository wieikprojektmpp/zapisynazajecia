package wieik.znz.web.form;

import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2015-01-25.
 */
public class SemesterApplicationForm {
    private long studentId;
    private long id;
    private List<SubjectApplicationForm> subjects;
    private Map<Long, String> arguments;

    public SemesterApplicationForm() {
    }

    public SemesterApplicationForm(List<SubjectApplicationForm> subjects, Map<Long, String> arguments, long studentId, long id) {
        this.subjects = subjects;
        this.arguments = arguments;
        this.studentId = studentId;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public List<SubjectApplicationForm> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectApplicationForm> subjects) {
        this.subjects = subjects;
    }

    public Map<Long, String> getArguments() {
        return arguments;
    }

    public void setArguments(Map<Long, String> arguments) {
        this.arguments = arguments;
    }
}
