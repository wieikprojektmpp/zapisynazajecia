package wieik.znz.web.form;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-08.
 */

public class SemesterForm {
    private List<SubjectForm> subjects;

    public List<SubjectForm> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectForm> subjects) {
        this.subjects = subjects;
    }
}
