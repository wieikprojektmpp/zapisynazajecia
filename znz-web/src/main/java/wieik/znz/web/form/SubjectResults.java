package wieik.znz.web.form;

import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2015-01-25.
 */
public class SubjectResults {
    private String name;
    private List<TermResults> terms;
    private SwapForm swapForm;
    private Map<Long, String> availableTerms;

    public SubjectResults() {
    }

    public SubjectResults(List<TermResults> terms, String name, SwapForm swapForm, Map<Long, String> availableTerms) {
        this.terms = terms;
        this.name = name;
        this.swapForm = swapForm;
        this.availableTerms = availableTerms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<TermResults> getTerms() {
        return terms;
    }

    public void setTerms(List<TermResults> terms) {
        this.terms = terms;
    }

    public SwapForm getSwapForm() {
        return swapForm;
    }

    public void setSwapForm(SwapForm swapForm) {
        this.swapForm = swapForm;
    }

    public Map<Long, String> getAvailableTerms() {
        return availableTerms;
    }

    public void setAvailableTerms(Map<Long, String> availableTerms) {
        this.availableTerms = availableTerms;
    }
}
