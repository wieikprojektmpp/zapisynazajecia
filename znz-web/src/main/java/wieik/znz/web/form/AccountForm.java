package wieik.znz.web.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.ScriptAssert;

import javax.validation.constraints.Size;

/**
 * Created by Sebastian on 2014-11-13.
 */

@ScriptAssert(
        lang = "javascript",
        script = "_this.confirmPassword != null && _this.confirmPassword.equals(_this.password)",
        message = "account.password.mismatch.message")
public class AccountForm {

    @NotEmpty
    @Size(min = 5, max = 50)
    private String username;

    @NotEmpty
    @Size(min = 2, max = 50)
    private String firstName;

    @NotEmpty
    @Size(min = 2, max = 50)
    private String lastName;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String password;

    @NotEmpty
    @Size(min = 1, max = 50)
    private String confirmPassword;

    @NotEmpty
    @Size(min = 6, max = 50)
    @Email
    private String email;

    private boolean reminder;
    private boolean notification;


    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isReminder() {
        return reminder;
    }
    public void setReminder(boolean reminder) {
        this.reminder = reminder;
    }

    public boolean isNotification() {
        return notification;
    }
    public void setNotification(boolean notification) {
        this.notification = notification;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
