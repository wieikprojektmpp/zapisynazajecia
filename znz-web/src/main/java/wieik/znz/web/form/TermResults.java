package wieik.znz.web.form;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-25.
 */
public class TermResults {
    private String name;
    private List<String> students;

    public TermResults() {
    }

    public TermResults(String name, List<String> students) {
        this.name = name;
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getStudents() {
        return students;
    }

    public void setStudents(List<String> students) {
        this.students = students;
    }
}
