package wieik.znz.web.form;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-25.
 */
public class SemesterResults {
    private List<SubjectResults> subjects;

    public SemesterResults() {
    }

    public SemesterResults(List<SubjectResults> subjects) {
        this.subjects = subjects;
    }

    public List<SubjectResults> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectResults> subjects) {
        this.subjects = subjects;
    }
}
