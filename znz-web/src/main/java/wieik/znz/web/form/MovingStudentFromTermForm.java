package wieik.znz.web.form;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class MovingStudentFromTermForm {
    int fromTerm;
    int studentId;
    int toTerm;

    public int getFromTerm() {
        return fromTerm;
    }

    public void setFromTerm(int fromTerm) {
        this.fromTerm = fromTerm;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getToTerm() {
        return toTerm;
    }

    public void setToTerm(int toTerm) {
        this.toTerm = toTerm;
    }
}
