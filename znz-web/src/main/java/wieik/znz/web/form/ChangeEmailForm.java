package wieik.znz.web.form;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;

/**
 * Created by Sebastian on 2015-01-08.
 */
public class ChangeEmailForm {
    @NotNull
    @Email
    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
