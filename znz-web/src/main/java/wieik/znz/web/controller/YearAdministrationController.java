package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.znz.api.domain.*;
import wieik.znz.api.services.*;
import wieik.znz.web.form.*;

import javax.validation.Valid;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-23.
 */
@Controller
@RequestMapping("/zarzadzanie_grupa")
public class YearAdministrationController {
    @Autowired
    private YearService yearService;
    @Autowired
    private AnnouncementService announcementService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private ModeratorService moderatorService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TermService termService;
    @Autowired
    private ArgumentService argumentService;

    private String getYearName(Year year) {
        try {
            Course course = courseService.getCourseForYear(year);
            Department department = departmentService.getDepartmentForCourse(course);
            School school = schoolService.getSchoolForDepartment(department);
            return school.getNazwaUczelni() + ", " + department.getNazwa() + ", " + course.getNazwa() + " " + year.getRokRozp() + "-" + year.getRokZak();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @RequestMapping(value = "", method = RequestMethod.GET, params = {"id"})
    public String yearAdministrationPage(@RequestParam(value = "id") long id, Model model) {
        try {
            Year year = yearService.getYear(id);
            if (year == null) {
                model.addAttribute("error", "No year with id: " + id);
                return "errorPage";
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!moderatorService.isStudentModeratorOfYear(student, year)) {
                model.addAttribute("error", "You are not moderator of this year");
                return "errorPage";
            }

            model.addAttribute("yearName", getYearName(year));

            int studentsNumber = studentService.countStudentsForYear(year);
            model.addAttribute("studentsNumber", studentsNumber);

            Semester semester = semesterService.getNewestSemester(year);
            if (semester != null)
                model.addAttribute("semesterNumber", semester.getNrSemestru());
            else
                model.addAttribute("semesterNumber", 0);
            model.addAttribute("password", year.getPassword());
            model.addAttribute("yearId", year.getRokId());

        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("error", e.toString());
            return "errorPage";
        }
        return "yearAdministrationPage";
    }

    @RequestMapping(value = "/ogloszenia", method = RequestMethod.GET, params = {"id"})
    public String yearAnnouncementsAdministrationPage(@RequestParam(value = "id") long id, Model model) {
        try {
            Year year = yearService.getYear(id);
            if (year == null) {
                model.addAttribute("error", "No year with id: " + id);
                return "errorPage";
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!moderatorService.isStudentModeratorOfYear(student, year)) {
                model.addAttribute("error", "You are not moderator of this year");
                return "errorPage";
            }

            model.addAttribute("yearId", year.getRokId());
            model.addAttribute("yearName", getYearName(year));
            List<Announcement> announcements = announcementService.getAnnouncementForYear(year);
System.out.println(announcements.toString());
            model.addAttribute("announcements", announcements);


        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("error", e.toString());
            return "errorPage";
        }
        return "yearAnnouncementsAdministrationPage";
    }

    @RequestMapping(value = "/formularz_ogloszenia", method = RequestMethod.GET, params = {"rokId"})
    public String newAnnouncementForm(@RequestParam(value = "rokId") long id, Model model) {
        AnnouncementForm form = new AnnouncementForm();
        form.setYearId(id);
        System.out.println("year id przy tworzeniu formularza: " + form.getYearId());
        model.addAttribute("form", form);
        model.addAttribute("yearId", id);
        model.addAttribute("submitUrl", "/ZNZ/zarzadzanie_grupa/formularz_ogloszenia?yearId=" + id + "&id=" + 0);
        return "announcementForm";
    }


    @RequestMapping(value = "/formularz_ogloszenia", method = RequestMethod.GET, params = {"id"})
    public String editAnnouncementForm(@RequestParam(value = "id") long id, Model model) {
        try {
            AnnouncementForm form = new AnnouncementForm();
            Announcement announcement = announcementService.getAnnouncement(id);
            if (announcement==null) {
                model.addAttribute("error", "No announcement with this id");
                return "errorPage";
            }
            form.setYearId(0);
            form.setContent(announcement.getTresc());
            form.setTitle(announcement.getNazwa());
            form.setAnnouncementId(id);
            model.addAttribute("submitUrl", "/ZNZ/zarzadzanie_grupa/formularz_ogloszenia?yearId=" + 0 + "&id=" + id);
            model.addAttribute("form", form);
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("error", e.toString());
            return "errorPage";
        }
        return "announcementForm";
    }

    @RequestMapping(value = "/formularz_ogloszenia", method = RequestMethod.POST, params = {"yearId", "id"})
    public String postAnnouncementForm(@RequestParam(value = "yearId") long yearId, @RequestParam(value = "id") long id, @Valid @ModelAttribute("form") AnnouncementForm form, BindingResult bindingResult) {
        System.out.println("!!! postAnnouncementForm");
        if (bindingResult.hasErrors()) {
            System.err.println(bindingResult.toString());
            return "announcementForm";
        }
        try {
            if (yearId != 0) {
                System.out.println("!!! nowe ogloszenie");
                Announcement announcement = new Announcement();
                announcement.setNazwa(form.getTitle());
                announcement.setDataPublikacji(new Date());
                announcement.setRokId(yearService.getYear(yearId));
                announcement.setTresc(form.getContent());
                System.out.println("YEAR: " + yearId);

                announcementService.createAnnouncement(announcement);
            }
            else {
                System.out.println("!!! edycja ogloszenia id:" + id);
                Announcement announcement = announcementService.getAnnouncement(id);
                announcement.setTresc(form.getContent());
                announcementService.updateAnnouncement(announcement);
            }
            return "redirect:/zarzadzanie_grupa?id="+yearId;
        }catch (Exception e) {
            e.printStackTrace();
            return "announcementForm";
        }

    }

    @RequestMapping(value = "/usun_ogloszenie", method = RequestMethod.GET, params = {"id"})
    public String deleteAnnouncement(@RequestParam(value = "id") long id, Model model) {
        try {
            System.out.println("DELETING id: " + id);
            long yearId = yearService.getYearForAnnouncementWithId(id);
            announcementService.deleteAnnouncementById(id);
            return "redirect:/zarzadzanie_grupa?id="+yearId;
        }catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }



    @RequestMapping(value = "/semestry", method = RequestMethod.GET, params = {"id"})
    public String semestersAdministrationPage(@RequestParam(value = "id") long id, Model model) {
        try {
            Year year = yearService.getYear(id);
            if (year == null) {
                model.addAttribute("error", "No year with id: " + id);
                return "errorPage";
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!moderatorService.isStudentModeratorOfYear(student, year)) {
                model.addAttribute("error", "You are not moderator of this year");
                return "errorPage";
            }

            model.addAttribute("yearId", year.getRokId());
            model.addAttribute("yearName", getYearName(year));

            List<Semester> semesters = semesterService.getSemestersForYear(year);
            model.addAttribute("semesters", semesters);

            boolean canCreateNewSemester = true;
            for (Semester s : semesters) {
                if (s.getStan()!=5)
                    canCreateNewSemester = false;
            }
            if (semesters == null || semesters.size()==0)
                canCreateNewSemester = true;
            System.out.println("semesters = " + semesters);
            System.out.println("canCreateNewSemester = " + canCreateNewSemester);
            model.addAttribute("canCreateNewSemester", canCreateNewSemester);
            return "semestersAdministrationPage";

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/semestr_nast_stan", method = RequestMethod.GET, params = {"id"})
    public String nextState(@RequestParam(value = "id") long id, Model model) {
        try {
            Semester semester = semesterService.getSemester(id);
            semester.nastepnyStan();
            semesterService.updateSemester(semester);
        }catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/nowy_semestr", method = RequestMethod.GET, params = {"id", "liczbaPrzedmiotow"})
    public String semesterForm(@RequestParam(value = "id") long id,
                               @RequestParam(value = "liczbaPrzedmiotow") int subjectsNum,
                               Model model) {
        try {
            Year year = yearService.getYear(id);
            if (year == null) {
                model.addAttribute("error", "No year with id: " + id);
                return "errorPage";
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!moderatorService.isStudentModeratorOfYear(student, year)) {
                model.addAttribute("error", "You are not moderator of this year");
                return "errorPage";
            }

            SemesterForm semesterForm = new SemesterForm();
            List<SubjectForm> subjectForms = new ArrayList<>(subjectsNum);
            for (int i = 0; i < subjectsNum; ++i) {
                List<TermForm> termForms = new ArrayList<>(5);
                for (int j = 0; j < 5; ++j) {
                    termForms.add(new TermForm());
                }
                SubjectForm sf = new SubjectForm();
                sf.setTerms(termForms);
                subjectForms.add(sf);
            }
            semesterForm.setSubjects(subjectForms);
            System.out.println("subjects: " + subjectForms.toString());

            model.addAttribute("semesterForm", semesterForm);
            model.addAttribute("postUrl", "/ZNZ/zarzadzanie_grupa/semestr?id="+id+"&update=0");
            return "semesterForm";


        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/edytuj_semestr", method = RequestMethod.GET, params = {"yearId", "id"})
    public String semesterEditForm(@RequestParam(value = "yearId") long yearId, @RequestParam(value = "id") long id,
                               Model model) {
        try {
            Year year = yearService.getYear(yearId);
            Semester semester = semesterService.getSemester(id);
            if (year == null || semester == null) {
                model.addAttribute("error", "No year/semester with id: " + id);
                return "errorPage";
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!moderatorService.isStudentModeratorOfYear(student, year)) {
                model.addAttribute("error", "You are not moderator of this year");
                return "errorPage";
            }

            List<Subject> subjects = subjectService.getSubjectsForSemester(semester);

            SemesterForm semesterForm = new SemesterForm();
            List<SubjectForm> subjectForms = new ArrayList<>(subjects.size());
            for (Subject s : subjects) {
                List<Term> terms = termService.getTermsForSubject(s);
                List<TermForm> termForms = new ArrayList<>(terms.size());
                for (Term t : terms) {
                    TermForm tf = new TermForm();
                    tf.setId(t.getTerminId());
                    tf.setEnds(((Integer) t.getGodzZak().getHours()).toString() + ":" + ((Integer) t.getGodzZak().getMinutes()).toString());
                    tf.setStarts(((Integer) t.getGodzRozp().getHours()).toString() + ":" + ((Integer) t.getGodzRozp().getMinutes()).toString());
                    tf.setWeek(t.getRodzajTygodnia());
                    tf.setDay(t.getDzien());
                    termForms.add(tf);
                }
                SubjectForm sf = new SubjectForm();
                sf.setId(s.getPrzedmiotId());
                sf.setName(s.getNazwa());
                sf.setDescription(s.getDodatkoweInfo());
                sf.setMaxStudents(s.getMaxStudentow());
                sf.setMinStudents(s.getMinStudentow());
                sf.setTerms(termForms);
                subjectForms.add(sf);
            }
            semesterForm.setSubjects(subjectForms);
            System.out.println("subjects: " + subjectForms.toString());

            model.addAttribute("semesterForm", semesterForm);
            model.addAttribute("postUrl", "/ZNZ/zarzadzanie_grupa/semestr?id="+id+"&update="+id);
            return "semesterForm";


        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/semestr", method = RequestMethod.POST, params = {"id", "update"})
    public String postSemester(@RequestParam(value = "id") long id,@RequestParam(value = "update") long update, SemesterForm semesterForm, BindingResult bindingResult) {
        try {
            Year year = yearService.getYear(id);
            if (year == null) {
                return "redirect:/";
            }

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!moderatorService.isStudentModeratorOfYear(student, year)) {
                return "redirect:/";
            }
            if (bindingResult.hasErrors()) {
                System.err.println(bindingResult.toString());
                return "semesterForm";
            }

            if (update==0) {
                // OK!
                Semester semester = new Semester();
                semester.setRokId(year);
                Semester lastSemester = semesterService.getNewestSemester(year);
                if (lastSemester == null)
                    semester.setNrSemestru(0);
                else
                    semester.setNrSemestru(lastSemester.getNrSemestru() + 1);
                semester.setStan(0);
                semester.setRozpoczecieZapisow(new Date());
                semester.setZakonczenieZamian(new Date());
                semester.setZakonczenieZapisow(new Date());
                long semesterId = semesterService.createSemester(semester);
                semester = semesterService.getSemester(semesterId);

                for (SubjectForm subjectForm : semesterForm.getSubjects()) {
                    Subject subject = new Subject();
                    subject.setSemesterId(semester);
                    subject.setNazwa(subjectForm.getName());
                    subject.setDodatkoweInfo(subjectForm.getDescription());
                    subject.setMinStudentow(subjectForm.getMinStudents());
                    subject.setMaxStudentow(subjectForm.getMaxStudents());

                    long subjectId = subjectService.createSubject(subject);
                    subject = subjectService.getSubject(subjectId);

                    for (TermForm termForm : subjectForm.getTerms()) {
                        if (!(termForm.getStarts() == null) && !(termForm.getStarts().equals(""))) {
                            Term term = new Term();
                            term.setDzien(termForm.getDay());
                            term.setPrzedmiotId(subject);
                            term.setRodzajTygodnia(termForm.getWeek());
                            term.setGodzRozp(stringToTime(termForm.getStarts()));
                            term.setGodzZak(stringToTime(termForm.getEnds()));

                            termService.createTerm(term);
                        }
                    }


                }
            }
            else {
                Semester semester = semesterService.getSemester(update);

                for (SubjectForm subjectForm : semesterForm.getSubjects()) {
                    Subject subject = subjectService.getSubject(subjectForm.getId());
                    if (subject == null){
                        System.out.println("!!! NULL SUBJECT");
                        return "redirect:/";
                    }
                    subject.setSemesterId(semester);
                    subject.setNazwa(subjectForm.getName());
                    subject.setDodatkoweInfo(subjectForm.getDescription());
                    subject.setMinStudentow(subjectForm.getMinStudents());
                    subject.setMaxStudentow(subjectForm.getMaxStudents());

                    subjectService.updateSubject(subject);

                    for (TermForm termForm : subjectForm.getTerms()) {
                        Term term = termService.getTerm(termForm.getId());
                        if (term == null) {
                            System.out.println("!!! NULL TERM");
                            return "redirect:/";
                        }
                        term.setDzien(termForm.getDay());
                        term.setPrzedmiotId(subject);
                        term.setRodzajTygodnia(termForm.getWeek());
                        term.setGodzRozp(stringToTime(termForm.getStarts()));
                        term.setGodzZak(stringToTime(termForm.getEnds()));

                        termService.updateTerm(term);
                    }
                }
                semesterService.updateSemester(semester);

            }

            return "redirect:/zarzadzanie_grupa?id="+id;
        }catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }

    }

    private static Time stringToTime(String starts) {
        int i = starts.indexOf(':');
        String s = starts.substring(0, i);
        int h = Integer.parseInt(s);
        int m = Integer.parseInt(starts.substring(i+1));
        return new Time(h, m, 0);
    }

    @RequestMapping(value = "/nowy_argument", method = RequestMethod.GET, params = {"semId"})
    public String newArgument(@RequestParam(value = "semId") long semId, Model model) {
        ArgumentForm argumentForm = new ArgumentForm();
        model.addAttribute("argumentForm", argumentForm);
        model.addAttribute("postUrl", "/ZNZ/zarzadzanie_grupa/argument?semId="+semId+"&update=0");
        return "argumentForm";
    }

    @RequestMapping(value = "/argument", method = RequestMethod.GET, params = {"id"})
    public String editArgument(@RequestParam(value = "id") long id, Model model) {
        try {
            ArgumentForm argumentForm = new ArgumentForm();
            Argument argument = argumentService.getArgument(id);
            argumentForm.setArgument(argument.getNazwa());
            argumentForm.setPriority(argument.getPriorytet());
            model.addAttribute("argumentForm", argumentForm);
            model.addAttribute("postUrl", "/ZNZ/zarzadzanie_grupa/argument?semId=" + 0 + "&update=" + id);
            return "argumentForm";
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }

    @RequestMapping(value = "/argument", method = RequestMethod.POST, params = {"semId", "update"})
    public String postArgument(@RequestParam(value = "semId") long semId,@RequestParam(value = "update") long update, ArgumentForm argumentForm, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            try {
                if (update == 0) {
                    Semester semester = semesterService.getSemester(semId);
                    Argument argument = new Argument();
                    argument.setSemestrId(semester);
                    argument.setNazwa(argumentForm.getArgument());
                    argument.setPriorytet(argumentForm.getPriority());
                    argumentService.createArgument(argument);
                } else {
                    Argument argument = argumentService.getArgument(update);
                    argument.setNazwa(argumentForm.getArgument());
                    argument.setPriorytet(argumentForm.getPriority());
                    argumentService.updateArgument(argument);
                }
                return "redirect:/zarzadzanie_grupa?id="+yearService.getYearBySemesterId(semId).getRokId();
            } catch (Exception e) {
                e.printStackTrace();
                return "redirect:/";
            }
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/argumenty", method = RequestMethod.GET, params = {"semId"})
    public String arguments(@RequestParam(value = "semId") long id, Model model) {
        try {
            List<Argument> arguments = argumentService.getArgumentsForSemester(semesterService.getSemester(id));
            model.addAttribute("arguments", arguments);
            model.addAttribute("semId", id);
            return "argumentsList";
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }
}
