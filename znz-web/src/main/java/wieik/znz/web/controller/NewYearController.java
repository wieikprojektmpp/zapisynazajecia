package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wieik.znz.api.domain.*;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.*;
import wieik.znz.web.form.CreateYearForm;

import java.util.*;

/**
 * Created by Sebastian on 2015-01-19.
 */
@Controller
@RequestMapping("/nowa_grupa")
public class NewYearController {
    static final String symbols = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ#";

    @Autowired
    SchoolService schoolService;
    @Autowired
    StudentService studentService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    CourseService courseService;
    @Autowired
    YearService yearService;
    @Autowired
    GroupService groupService;
    @Autowired
    ModeratorService moderatorService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getYearForm(Model model) {
        model.addAttribute("yearForm", new CreateYearForm());
        Map<Integer, Integer> years = new LinkedHashMap<>();
        for (int i = 2010; i <= 2100; ++i)
            years.put(i,i);
        model.addAttribute("years", years);
        return "yearForm";
    }

    private static String generatePassword() {
        Random random = new Random(new Date().getTime());
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 12; ++i) {
            stringBuilder.append(symbols.charAt(random.nextInt(symbols.length()-1)));
        }
        return stringBuilder.toString();
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postYearForm(@ModelAttribute("yearForm") CreateYearForm yearForm, BindingResult result) {
        if (!result.hasErrors()) {
            try {
                Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                String username = auth.getName();
                School school = schoolService.getSchoolByName(yearForm.getSchoolName());
                Student student = studentService.getStudentByUsername(username);
                if (student == null) {
                    //TODO blad!
                    return "redirect:/";
                }

                Department department = null;
                long id;

                if (school==null) {
                    // TWORZENIE UCZELNI
                    school = new School();
                    school.setNazwaUczelni(yearForm.getSchoolName());
                    school.setMiejscowosc("brak");
                    school.setWydzialy(null);

                    id = schoolService.createSchool(school);
                    school = schoolService.getSchool(id);
                } else {
                    department = departmentService.getDepartmentByNameAndSchoolId(yearForm.getDepartmentName(), school.getUczelniaId());
                }

                Course course = null;

                if (department == null) {
                    // TWORZENIE WYDZIALU
                    department = new Department();
                    department.setKierunki(null);
                    department.setUczelniaId(school);
                    department.setNazwa(yearForm.getDepartmentName());

                    id = departmentService.createDepartment(department);
                    department = departmentService.getDepartment(id);
                } else {
                    course = courseService.getCourseByNameAndDepartmentId(yearForm.getCourseName(), department.getWydzialId());
                }

                Year year = null;

                if (course==null) {
                    // TWORZENIE KIERUNKU
                    course = new Course();
                    course.setLata(null);
                    course.setNazwa(yearForm.getCourseName());
                    course.setWydzialId(department);

                    id = courseService.createCourse(course);
                    course = courseService.getCourse(id);
                }
                else {
                    List<Year> years = yearService.getYearsForCourse(course);
                    for (Year y : years) {
                        if (y.getRokRozp() == yearForm.getStartYear() && y.getRokZak() == yearForm.getEndYear()) {
                            year = y;
                        }
                    }
                }

                Group group = null;

                if (year==null) {
                    // TWORZENIE ROKU
                    year = new Year();
                    year.setRokRozp(yearForm.getStartYear());
                    year.setRokZak(yearForm.getEndYear());
                    year.setKierunekId(course);
                    year.setPassword(generatePassword());

                    id = yearService.createYear(year);
                    year = yearService.getYear(id);

                }
                else {
                    //TODO: GRUPA JUZ ISTNIEJE
                    return "redirect:/";
                }

                List<Group> groups = groupService.getGroupsForYear(year);
                if (groups != null && groups.size()>0) {
                    group = groups.get(0);
                }
                if (group==null) {
                    // TWORZENIE GRUPY
                    group = new Group();
                    group.setNazwa("def");
                    group.setRokId(year);

                    id = groupService.createGroup(group);
                    group = groupService.getGroup(id);
                }

                groupService.addStudentToGroup(student, group);



                // TWORZENIE STAROSTY
                Moderator moderator = new Moderator();
                moderator.setRokId(year);
                moderator.setStudent(student);
                moderator.setCzyZastepca(false);

                moderatorService.createModerator(moderator);


                return "redirect:/grupa?id="+year.getRokId();
            } catch (ZNZException e) {
                //TODO przekierowania po bledach
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return "yearForm";
    }
}
