package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wieik.znz.api.domain.Account;
import wieik.znz.api.domain.Student;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.AccountService;
import wieik.znz.web.form.AccountForm;

import javax.validation.Valid;

/**
 * Created by Sebastian on 2014-11-13.
 */
@Controller
@RequestMapping("/registration")
public class AccountController {
    private static final String REGISTRATION_FORM = "registrationForm";
    private static final String REGISTRATION_OK = "redirect:/";

    // usługa AccountService wsktrzykiwana przy tworzeniu kontrolera
    @Autowired
    AccountService accountService;

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

/*    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;*/

    // ktore pola mozna wypelniac
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields(new String[]{
                "username", "password", "confirmPassword", "email", "reminder", "notification"
        });
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String getRegistrationForm(Model model) {
        model.addAttribute("accountForm", new AccountForm());
        return REGISTRATION_FORM;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postRegistrationForm(@Valid AccountForm form, BindingResult result) {
        System.err.println(result.getErrorCount());
        convertPasswordError(result);
        /*String password = form.getPassword();*/
        try {
            accountService.registerAccount(toAccount(form), form.getPassword(), result, toStudent(form));
        } catch (ZNZException e) {
            //TODO
        }

/*        if (!result.hasErrors()) {
            Authentication authRequest = new UsernamePasswordAuthenticationToken(form.getUsername(), password);
            Authentication authResult = authenticationManager.authenticate(authRequest);
            SecurityContextHolder.getContext().setAuthentication(authResult);
        }*/

        return (result.hasErrors() ? REGISTRATION_FORM : REGISTRATION_OK);
    }

    private static void convertPasswordError(BindingResult result) {
        for (ObjectError error : result.getGlobalErrors()) {
            String msg = error.getDefaultMessage();
            if ("account.password.mismatch.message".equals(msg)) {
                if (!result.hasFieldErrors("password")) {
                    result.rejectValue("password", "error.mismatch");
                }
            }
        }
    }

    private static Account toAccount(AccountForm form) {
        Account account = new Account();
        account.setUsername(form.getUsername());
        account.setEmail(form.getEmail());
        account.setEnabled(true);
        account.setNotification(form.isNotification());
        account.setReminder(form.isReminder());
        account.setAuthority("user");
        return account;
    }

    private static Student toStudent(AccountForm form) {
        Student student = new Student();
        student.setImie(form.getFirstName());
        student.setNazwisko(form.getLastName());
        return student;
    }

}
