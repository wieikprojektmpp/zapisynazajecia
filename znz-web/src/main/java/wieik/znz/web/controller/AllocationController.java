package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.znz.api.domain.*;
import wieik.znz.api.services.*;
import wieik.znz.web.form.SemesterResults;
import wieik.znz.web.form.SubjectResults;
import wieik.znz.web.form.SwapForm;
import wieik.znz.web.form.TermResults;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2015-01-25.
 */@Controller
   @RequestMapping("/wyniki")
public class AllocationController {
    @Autowired
    private YearService yearService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TermService termService;
    @Autowired
    private AllocationService allocationService;
    @Autowired
    private ModeratorService moderatorService;
    @Autowired
    private SwapService swapService;

    @RequestMapping(value = "", method = RequestMethod.GET, params = {"id"})
    public String semesterForm(@RequestParam(value = "id") long id,
                               Model model) {
        try {
            // pobranie semestru i roku
            Semester semester = semesterService.getSemester(id);
            if (semester == null) {
                model.addAttribute("error", "No semester with id: " + id);
                return "errorPage";
            }
            Year year = yearService.getYearBySemesterId(semester.getSemestrId());

            // pobranie danych uzytkownika
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student studentOnline = studentService.getStudentByUsername(username);

            // sprawdzenie czy student jest czlonkiem roku
            if (!studentService.isInYear(studentOnline, year)) {
                model.addAttribute("error", "You are not member of this year");
                return "errorPage";
            }

            // sprawdzenie czy jest starosta
            boolean isModerator = moderatorService.isStudentModeratorOfYear(studentOnline, year);

            // wczytanie przydzialow
            List<Subject> subjects = subjectService.getSubjectsForSemester(semester);
            List<SubjectResults> subjectResults = new LinkedList<>();
            for(Subject s : subjects) {
                List<Term> terms = termService.getTermsForSubject(s);
                List<TermResults> termResults = new LinkedList<>();

                //formularz zamiany
                SwapForm swapForm = new SwapForm();
                Map<Long, String> availableTerms = new HashMap<>();
                for (Term t : terms) {
                    List<String> students = new LinkedList<>();
                    List<Allocation> allocations = allocationService.getAllForTerm(t);
                    boolean myTerm = false;
                    for(Allocation a : allocations) {
                        Student student = studentService.getStudentForAllocation(a);
                        students.add(student.getImie()+ " " + student.getNazwisko());
                        // jesli to jest termin studenta zapisujemy do ziarna formularza
                        if (student.getStudentId()==studentOnline.getStudentId()) {
                            swapForm.setFromAllocationId(a.getPrzydzialId());
                            myTerm = true;
                        }
                    }
                    if (!myTerm) {
                        availableTerms.put(t.getTerminId(), termToString(t));
                    }
                    termResults.add(new TermResults(termToString(t), students ));
                }

                subjectResults.add(new SubjectResults(termResults, s.getNazwa(), swapForm, availableTerms));
            }
            SemesterResults semesterResults = new SemesterResults(subjectResults);
            model.addAttribute("semester", semesterResults);

            model.addAttribute("state", semester.getStan());
            model.addAttribute("isModerator", isModerator);


            return "allocationsPage";

        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postWantsToSwap(SwapForm swapForm, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                return "allocationsPage";
            }

            Allocation allocation = allocationService.getAllocation(swapForm.getFromAllocationId());
            Term term = termService.getTerm(swapForm.getToTermId());
            Student student = studentService.getStudent(swapForm.getStudentId());
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student studentOnline = studentService.getStudentByUsername(username);

            if (allocation == null || term == null || student.getStudentId()!=studentOnline.getStudentId()) {
                return "redirect:/";
            }
            Swap swap = new Swap();
            swap.setTerminId(term);
            swap.setPrzydzialId(allocation);
            swap.setZamkniete(false);

            swapService.createSwap(swap);

            long semesterId = semesterService.getSemesterIdByTerm(term);
            return "redirect:/wyniki?id=" + semesterId;
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }

    }



    private static String termToString(Term t) {
        return intToDay(t.getDzien(), t.getRodzajTygodnia()) + ", " + t.getGodzRozp().getHours() + ":" + t.getGodzRozp().getMinutes() + " - " + t.getGodzZak().getHours() + ":" + t.getGodzZak().getMinutes();
    }

    private static String intToDay(int i) {
        switch (i) {
            case 0 : return "poniedzialek";
            case 1 : return "wtorek";
            case 2 : return "sroda";
            case 3 : return "czwartek";
            case 4 : return "piatek";
            case 5 : return "sobota";
            default:  return "niedziela";
        }
    }

    private static String intToDay(int day, int week) {
        String s = intToDay(day);
        if (week == 1) {
            s.concat(", nieparzysty");
        }
        if (week == 2) {
            s.concat(", parzysty");
        }
        return s;
    }
}
