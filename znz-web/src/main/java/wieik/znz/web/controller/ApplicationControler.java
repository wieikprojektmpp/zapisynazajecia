package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.znz.api.domain.*;
import wieik.znz.api.services.*;
import wieik.znz.web.form.SemesterApplicationForm;
import wieik.znz.web.form.SubjectApplicationForm;

import java.util.*;

/**
 * Created by Sebastian on 2015-01-25.
 */
@Controller
@RequestMapping("/zapisy")
public class ApplicationControler {
    @Autowired
    private YearService yearService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private SchoolService schoolService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TermService termService;
    @Autowired
    private ArgumentService argumentService;
    @Autowired
    private ApplicationService applicationService;

    @RequestMapping(value = "", method = RequestMethod.GET, params = {"id"})
    public String semesterForm(@RequestParam(value = "id") long id,
                               Model model) {
        try {
            Semester semester = semesterService.getSemester(id);
            if (semester == null) {
                model.addAttribute("error", "No semester with id: " + id);
                return "errorPage";
            }
            Year year = yearService.getYearBySemesterId(id);


            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);

            if (!studentService.isInYear(student,year)) {
                model.addAttribute("error", "You are not member of this year");
                return "errorPage";
            }

            //OK tworzymy formularz
            Map<Long, String> args = new HashMap<>();
            List<Argument> arguments = argumentService.getArgumentsForSemester(semester);
            for (Argument a : arguments) {
                args.put(a.getArgumentId(), a.getNazwa());
            }

            List<Subject> subjects = subjectService.getSubjectsForSemester(semester);
            List<SubjectApplicationForm> subAppForms = new ArrayList<>(subjects.size());
            for (Subject s : subjects) {
                List<Term> terms = termService.getTermsForSubject(s);
                Map<Long, String> termsMap = new HashMap<>();
                for (Term t : terms) {
                    termsMap.put(t.getTerminId(), intToDay(t.getDzien(), t.getRodzajTygodnia()) + ", " + t.getGodzRozp().getHours()+":"+t.getGodzRozp().getMinutes()+" - " + t.getGodzZak().getHours()+":"+t.getGodzZak().getMinutes());
                }
                subAppForms.add(new SubjectApplicationForm(s, termsMap, s.getPrzedmiotId()));
            }
            SemesterApplicationForm semesterApplicationForm = new SemesterApplicationForm(subAppForms, args, student.getStudentId(), semester.getSemestrId());
            model.addAttribute("form", semesterApplicationForm);

            return "applicationForm";
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }

    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postApplication(SemesterApplicationForm form, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors())
                return "applicationForm";

            Semester semester = semesterService.getSemester(form.getId());
            Student student = studentService.getStudent(form.getStudentId());

            List<Application> applications = new LinkedList<>();

            for (SubjectApplicationForm saf : form.getSubjects()) {
                Application app1 = new Application();
                app1.setArgumentId(argumentService.getArgument(saf.getArg1()));
                app1.setStudentId(student);
                app1.setTerminId(termService.getTerm(saf.getTerm1()));
                app1.setTyp_wyboru(selectionType(saf.getType2(), saf.getType2(), 1));

                Application app2 = new Application();
                app2.setArgumentId(argumentService.getArgument(saf.getArg2()));
                app2.setStudentId(student);
                app2.setTerminId(termService.getTerm(saf.getTerm2()));
                app2.setTyp_wyboru(selectionType(saf.getType2(), saf.getType2(), 2));

                Application app3 = new Application();
                app3.setArgumentId(argumentService.getArgument(saf.getArg3()));
                app3.setStudentId(student);
                app3.setTerminId(termService.getTerm(saf.getTerm3()));
                app3.setTyp_wyboru(selectionType(saf.getType2(), saf.getType2(), 3));

                applications.add(app1);
                applications.add(app2);
                applications.add(app3);
            }

            applicationService.createApplications(applications);

            return "redirect:/";
        } catch (Exception e) {
            e.printStackTrace();
            return "redirect:/";
        }
    }

    private static int selectionType(int type2, int type3, int selectionNum) {
        if (type2 == 0) {
            if (type3 == 0)
                return selectionNum;
            else
                return 3+selectionNum;
        }
        else {
            if (type3 == 0)
                return 6+selectionNum;
            else
                return 9+selectionNum;
        }
    }


    private static String intToDay(int i) {
        switch (i) {
            case 0 : return "poniedzialek";
            case 1 : return "wtorek";
            case 2 : return "sroda";
            case 3 : return "czwartek";
            case 4 : return "piatek";
            case 5 : return "sobota";
            default:  return "niedziela";
        }
    }

    private static String intToDay(int day, int week) {
        String s = intToDay(day);
        if (week == 1) {
            s.concat(", nieparzysty");
        }
        if (week == 2) {
            s.concat(", parzysty");
        }
        return s;
    }


}
