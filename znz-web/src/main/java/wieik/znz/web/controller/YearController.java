package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import wieik.znz.api.domain.Announcement;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.services.*;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-21.
 */
@Controller
@RequestMapping("/grupa")
public class YearController {
    @Autowired
    private GroupService groupService;
    @Autowired
    private AnnouncementService announcementService;
    @Autowired
    private SemesterService semesterService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private YearService yearService;
    @Autowired
    private ModeratorService moderatorService;

    @RequestMapping(value = "", method = RequestMethod.GET, params = {"id"})
    public String getYear(@RequestParam(value = "id") long id, Model model) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Student student = studentService.getStudentByUsername(username);
            Year year = yearService.getYear(id);
            // czy istnieje rok
            if (year==null) {
                model.addAttribute("error", "No year with id: " + id);
                return "errorPage";
            }

            // czy student nalezy do roku
            if (!studentService.isInYear(student, year)) {
                model.addAttribute("error", "You are not member of this year");
                return "errorPage";
            }

            List<Announcement> announcements = announcementService.getAnnouncementForYear(year);
System.out.println(announcements.toString());
            model.addAttribute("announcements", announcements);
            model.addAttribute("year", year);
            boolean isModerator = moderatorService.isStudentModeratorOfYear(student, year);
            model.addAttribute("moderator", isModerator);

            Semester semester = semesterService.getNewestSemester(year);

            if (semester != null) {
                System.out.println("!!! YearController: semester state: " + semester.getStan());
                model.addAttribute("state", semester.getStan());

            }
            else
                System.out.println("!!! YearController: semester is null!");
            model.addAttribute("semester", semester);


            return "yearPage";
        } catch (Exception e) {
            e.printStackTrace();
            model.addAttribute("error", e.toString());
            return "errorPage";
        }
    }



}
