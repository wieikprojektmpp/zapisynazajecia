package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.services.*;
import wieik.znz.web.form.JoinGroupForm;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2015-01-21.
 */
@Controller
@RequestMapping("/dolacz_do_grupy")
public class JoinGroupController {
    @Autowired
    SchoolService schoolService;
    @Autowired
    StudentService studentService;
    @Autowired
    DepartmentService departmentService;
    @Autowired
    CourseService courseService;
    @Autowired
    YearService yearService;
    @Autowired
    GroupService groupService;

    @ModelAttribute("years")
    public Map<Long, String> addYearsCombobox() {
        Map<Long, String> yearsCombobox = new HashMap<>();
        try {
            List<Year> years = yearService.getAllYears();
            if (years != null) {
                for (Year year : years) {
                    Course course = courseService.getCourseForYear(year);
                    yearsCombobox.put(year.getRokId(), course.getNazwa() + year.getRokRozp());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return yearsCombobox;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getForm(Model model) {
        try {
            model.addAttribute("form", new JoinGroupForm());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "joinGroupForm";
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postYearForm(@ModelAttribute("form") @Valid JoinGroupForm form, BindingResult result) {
        if (!result.hasErrors()) try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            Year year = yearService.getYear(form.getGroupId());
            Student student = studentService.getStudentByUsername(username);
            Group group = groupService.getGroupsForYear(year).get(0);
            if (form.getPassword().equals(year.getPassword())) {

                groupService.addStudentToGroup(student, group);
            } else {
                ObjectError error = new ObjectError("password", "Zle haslo.");
                result.addError(error);
                return "joinGroupForm";
            }

            return "redirect:/grupa?id="+year.getRokId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "joinGroupForm";

    }
}
