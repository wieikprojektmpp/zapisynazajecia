package wieik.znz.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import wieik.znz.api.domain.*;
import wieik.znz.api.exceptions.ZNZException;
import wieik.znz.api.services.*;
import wieik.znz.web.form.AnonymousMainPageForm;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/")
public class MainController {
    @Autowired
    private AnnouncementService announcementService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private YearService yearService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private SchoolService schoolService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView mainPage() {
        Map<String, Object> map = new HashMap<>();
        // jesli jest zalogowany
        boolean authenticated = false;
        for (GrantedAuthority a :  SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if (a.getAuthority().equals("user"))
                authenticated = true;
        }
        if ( authenticated ) try {
            Student student = studentService.getStudentByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            List<Announcement> announcements = announcementService.getAnnouncementForStudent(student);
System.out.println(announcements.toString());
            map.put("announcements", announcements);

            List<Year> years = yearService.getYearsForStudent(student);
            Map<Long, String> yearsMap = new LinkedHashMap<>();
            Course course;
            Department department;
            School school;
            for (Year year : years) {
                course = courseService.getCourseForYear(year);
                department = departmentService.getDepartmentForCourse(course);
                school = schoolService.getSchoolForDepartment(department);

                yearsMap.put(year.getRokId(), school.getNazwaUczelni() + ", " + department.getNazwa() + ", " + course.getNazwa() + ", " + year.getRokRozp());
            }
            map.put("yearsMap", yearsMap);

            return new ModelAndView("authenticatedMainPage", map);
        } catch (ZNZException e) {
            return new ModelAndView("errorPage", "error", e.toString());
        }

        // nie jest zalogowany
        else {
            try {
                List<Year> years = yearService.getAllYears();
                Map<Long, String> yearsMap = new LinkedHashMap<>();
                Course course;
                Department department;
                School school;
                for (Year year : years) {
                    course = courseService.getCourseForYear(year);
                    department = departmentService.getDepartmentForCourse(course);
                    school = schoolService.getSchoolForDepartment(department);

                    yearsMap.put(year.getRokId(), school.getNazwaUczelni() + ", " + department.getNazwa() + ", " + course.getNazwa() + ", " + year.getRokRozp());
                }
                map.put("years", yearsMap);
                map.put("anonymousMainPageForm", new AnonymousMainPageForm());

            } catch (ZNZException e) {
                return new ModelAndView("errorPage", "error", e.toString());
            }
            return new ModelAndView("mainPage", map);
        }
    }

    //TODO
    @RequestMapping(value = "anonymous", method = RequestMethod.POST)
    public ModelAndView anonymousRegistration(@Valid AnonymousMainPageForm form, BindingResult result) {

        return new ModelAndView("mainPage");
    }

}