-
-- Host: 127.0.0.1
-- Czas generowania: 24 Sty 2015, 22:21
-- Wersja serwera: 5.6.21
-- Wersja PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `znz`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `account`
--

CREATE TABLE IF NOT EXISTS `account` (
`id` int(10) unsigned NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  `authority` varchar(5) NOT NULL DEFAULT 'user',
  `enabled` tinyint(1) NOT NULL,
  `reminder` tinyint(1) NOT NULL,
  `notification` tinyint(1) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `student_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `argument`
--

CREATE TABLE IF NOT EXISTS `argument` (
`argument_id` int(11) unsigned NOT NULL,
  `semestr_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(64) NOT NULL,
  `priorytet` tinyint(2) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chec_zmiany`
--

CREATE TABLE IF NOT EXISTS `chec_zmiany` (
`chec_zmiany_id` int(11) unsigned NOT NULL,
  `przydzial_id` int(11) unsigned NOT NULL,
  `termin_id` int(11) unsigned NOT NULL,
  `zamkniete` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grupa`
--

CREATE TABLE IF NOT EXISTS `grupa` (
`grupa_id` int(11) unsigned NOT NULL,
  `rok_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `grupa`
--

INSERT INTO `grupa` (`grupa_id`, `rok_id`, `nazwa`) VALUES
(1, 2, '31i'),
(2, 2, '32i'),
(3, 2, '33i');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grupa_student`
--

CREATE TABLE IF NOT EXISTS `grupa_student` (
  `student_id` int(11) unsigned NOT NULL,
  `grupa_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kierunek`
--

CREATE TABLE IF NOT EXISTS `kierunek` (
`kierunek_id` int(11) unsigned NOT NULL,
  `wydzial_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kierunek`
--

INSERT INTO `kierunek` (`kierunek_id`, `wydzial_id`, `nazwa`) VALUES
(1, 1, 'Architektura'),
(2, 1, 'Architektura Krajobrazu'),
(3, 2, 'Fizyka Techniczna'),
(4, 2, 'Informatyka'),
(5, 2, 'Matematyka'),
(6, 2, 'Nanotechnologie Inanomateriały'),
(7, 3, 'Elektrotechnika'),
(8, 3, 'Energetyka'),
(9, 3, 'Informatyka'),
(10, 4, 'Biotechnologia'),
(11, 4, 'Chemia budowlana'),
(12, 4, 'InżynieriaChemicznaI Procesowa'),
(13, 4, 'Nanotechnologie i Nanomateriał'),
(14, 4, 'Technologia chemiczna'),
(15, 5, 'Budownictwo'),
(16, 5, 'Budownictwo (w J angielskim)'),
(17, 5, 'Gospodarka Przestrzenna'),
(18, 5, 'Transport'),
(19, 6, 'Budownictwo'),
(20, 6, 'Gospodarka Przestrzenna'),
(21, 6, 'Inżynieria Środowiska'),
(22, 6, 'Ochrona Środowiska'),
(23, 7, 'Automatyka i Robotyka'),
(24, 7, 'Energetyka'),
(25, 7, 'Informatyka Stosowana'),
(26, 7, 'Inżynieria Bezpieczeństwa'),
(27, 7, 'Inżynieria Biomedyczna'),
(28, 7, 'Inżynieria Materiałowa'),
(29, 7, 'Inżynieria Wzornictwa Przemysł'),
(30, 7, 'Mechanika i Budowa Maszyn'),
(31, 7, 'Transport'),
(32, 7, 'Zarządzanie i Inżynieria Prod.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ogloszenie`
--

CREATE TABLE IF NOT EXISTS `ogloszenie` (
`ogloszenie_id` int(11) unsigned NOT NULL,
  `rok_id` int(11) unsigned NOT NULL,
  `tytul` varchar(30) NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_publikacji` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ogloszenie`
--

INSERT INTO `ogloszenie` (`ogloszenie_id`, `rok_id`, `tytul`, `tresc`, `data_publikacji`) VALUES
(1, 2, 'Uwaga! Koniec zapisów!', 'Użytkowniku! Uprzejmie przypominamy o zbliżającym się końcu zapisów na Twoje zajęcia. Czym prędzej dokończ wszystkie czynności związane z zapisami. Więcej szczegółów szukaj u Swojego starosty. Pozdrawiamy i życzymy miłego dnia :)', '2015-01-28 00:00:00'),
(2, 2, 'Uwaga! Koniec edycji!', 'Użytkowniku! Uprzejmie przypominamy o zbliżającym się końcu możliwości wprowadzania zmian na Twoje zajęcia. Czym prędzej dokończ wszystkie czynności związane z edycją. Więcej szczegółów szukaj u Swojego starosty. Pozdrawiamy i życzymy miłego dnia :)', '2015-02-04 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ogloszenie_systemowe`
--

CREATE TABLE IF NOT EXISTS `ogloszenie_systemowe` (
`ogloszenie_systemowe_id` int(11) unsigned NOT NULL,
  `tytul` varchar(30) NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_publikacji` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `powiadomienie`
--

CREATE TABLE IF NOT EXISTS `powiadomienie` (
`powiadomienie_id` int(11) unsigned NOT NULL,
  `rok_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_wyslania` datetime NOT NULL,
  `typ` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmiot`
--

CREATE TABLE IF NOT EXISTS `przedmiot` (
`przedmiot_id` int(11) unsigned NOT NULL,
  `semestr_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(64) NOT NULL,
  `min_studentow` tinyint(2) unsigned NOT NULL,
  `max_studentow` tinyint(2) unsigned NOT NULL,
  `dodatkowe_info` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `przedmiot`
--

INSERT INTO `przedmiot` (`przedmiot_id`, `semestr_id`, `nazwa`, `min_studentow`, `max_studentow`, `dodatkowe_info`) VALUES
(1, 2, 'Inżynieria Programowania', 0, 18, 'Zajęcia co 2 tygodnie dla każdej z grup. Egzamin + Projekt.'),
(2, 2, 'Programowanie w języku Java', 0, 18, 'Zajęcia co 2 tygodnie dla każdej z grup. Projekt.'),
(3, 2, 'Systemy Wbudowane', 0, 12, 'Zajęcia co tydzień. Egzamin + Projekt.'),
(4, 2, 'Symulacje Komputerowe', 0, 20, 'Zajęcia co dwa tygodnie. Projekt.'),
(5, 2, 'Komputerowe Systemy Wspomagania Decyzji', 0, 16, 'Zajęcia co tydzień. Projekt.'),
(6, 2, 'Współczesne Bazy Danych', 0, 18, 'Zajęcia co dwa tygodnie. Projekt.');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przydzial`
--

CREATE TABLE IF NOT EXISTS `przydzial` (
`przydzial_id` int(11) unsigned NOT NULL,
  `student_id` int(11) unsigned NOT NULL,
  `termin_id` int(11) unsigned NOT NULL,
  `finalny` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `przydzial`
--

INSERT INTO `przydzial` (`przydzial_id`, `student_id`, `termin_id`, `finalny`) VALUES
(1, 1, 1, 1),
(2, 1, 5, 1),
(3, 1, 9, 1),
(4, 1, 12, 1),
(5, 1, 14, 1),
(6, 1, 16, 1),
(7, 2, 2, 1),
(8, 2, 6, 1),
(9, 2, 10, 1),
(10, 2, 13, 1),
(11, 2, 15, 1),
(12, 2, 17, 1),
(13, 3, 3, 1),
(14, 3, 7, 1),
(15, 3, 11, 1),
(16, 3, 12, 1),
(17, 3, 14, 1),
(18, 3, 16, 1),
(19, 4, 4, 1),
(20, 4, 8, 1),
(21, 4, 10, 1),
(22, 4, 13, 1),
(23, 4, 15, 1),
(24, 4, 17, 1),
(25, 5, 1, 1),
(26, 5, 5, 1),
(27, 5, 10, 1),
(28, 5, 12, 1),
(29, 5, 14, 1),
(30, 5, 16, 1),
(31, 6, 2, 1),
(32, 6, 6, 1),
(33, 6, 11, 1),
(34, 6, 13, 1),
(35, 6, 15, 1),
(36, 6, 17, 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rok`
--

CREATE TABLE IF NOT EXISTS `rok` (
`rok_id` int(11) unsigned NOT NULL,
  `kierunek_id` int(11) unsigned NOT NULL,
  `haslo` varchar(50) NOT NULL,
  `rok_rozp` int(4) NOT NULL,
  `rok_zak` int(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `rok`
--

INSERT INTO `rok` (`rok_id`, `kierunek_id`, `haslo`, `rok_rozp`, `rok_zak`) VALUES
(1, 9, 'RokCzwarty', 2011, 2015),
(2, 9, 'RokTrzeci', 2012, 2016),
(3, 9, 'RokDrugi', 2013, 2017),
(4, 9, 'RokPierwszy', 2014, 2018);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `semestr`
--

CREATE TABLE IF NOT EXISTS `semestr` (
`semestr_id` int(11) unsigned NOT NULL,
  `rok_id` int(11) unsigned NOT NULL,
  `nr_semestru` tinyint(2) unsigned NOT NULL,
  `rozpoczecie_zapisow` date NOT NULL,
  `zakonczenie_zapisow` date NOT NULL,
  `zakonczenie_zamian` date NOT NULL,
  `stan` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `semestr`
--

INSERT INTO `semestr` (`semestr_id`, `rok_id`, `nr_semestru`, `rozpoczecie_zapisow`, `zakonczenie_zapisow`, `zakonczenie_zamian`, `stan`) VALUES
(1, 1, 7, '2015-01-25', '2015-01-31', '2015-02-06', '5'),
(2, 2, 5, '2015-01-25', '2015-01-31', '2015-02-07', '5'),
(3, 2, 6, '2015-01-25', '2015-01-31', '2015-02-07', '5'),
(4, 3, 4, '2015-03-01', '2015-03-14', '2015-03-21', '5');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `starosta`
--

CREATE TABLE IF NOT EXISTS `starosta` (
`starosta_id` int(11) unsigned NOT NULL,
  `student_id` int(11) unsigned NOT NULL,
  `rok_id` int(11) unsigned NOT NULL,
  `czy_zastepca` tinyint(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `starosta`
--

INSERT INTO `starosta` (`starosta_id`, `student_id`, `rok_id`, `czy_zastepca`) VALUES
(1, 11, 2, 0),
(2, 12, 2, 0),
(3, 13, 2, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `student`
--

CREATE TABLE IF NOT EXISTS `student` (
`student_id` int(11) unsigned NOT NULL,
  `imie` varchar(20) NOT NULL,
  `nazwisko` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `student`
--

INSERT INTO `student` (`student_id`, `imie`, `nazwisko`) VALUES
(1, 'Adam', 'Baca'),
(2, 'Adam', 'Dyr'),
(3, 'Adrian', 'Marzec'),
(4, 'Adrian', 'Zabawa'),
(5, 'Andrzej', 'Lekki'),
(6, 'Andrzej', 'Olkiewicz'),
(7, 'Arkadiusz ', 'Chudoba'),
(8, 'Arkadiusz', 'Lupa'),
(9, 'Bartosz', 'Odziomek'),
(10, 'Damian', 'Marszałek'),
(11, 'Damian', 'Nowak'),
(12, 'Jakub', 'Kmiecik'),
(13, 'Łukasz', 'Pytlak');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `termin`
--

CREATE TABLE IF NOT EXISTS `termin` (
`termin_id` int(11) unsigned NOT NULL,
  `przedmiot_id` int(11) unsigned NOT NULL,
  `dzien` date NOT NULL,
  `godzina_rozp` time NOT NULL,
  `godzina_zak` time NOT NULL,
  `rodz_tyg` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `termin`
--

INSERT INTO `termin` (`termin_id`, `przedmiot_id`, `dzien`, `godzina_rozp`, `godzina_zak`, `rodz_tyg`) VALUES
(1, 1, '2015-01-28', '07:30:00', '09:00:00', '0'),
(2, 1, '2015-01-28', '09:15:00', '10:45:00', '0'),
(3, 1, '2015-02-04', '07:30:00', '09:00:00', '0'),
(4, 1, '2015-02-04', '09:15:00', '10:45:00', '0'),
(5, 2, '2015-01-27', '12:45:00', '14:15:00', '0'),
(6, 2, '2015-01-27', '14:30:00', '16:00:00', '0'),
(7, 2, '2015-02-03', '12:45:00', '14:15:00', '0'),
(8, 2, '2015-02-03', '14:30:00', '16:00:00', '0'),
(9, 3, '2015-01-27', '12:45:00', '14:15:00', '0'),
(10, 3, '2015-01-27', '14:30:00', '16:00:00', '0'),
(11, 3, '2015-01-27', '16:15:00', '17:45:00', '0'),
(12, 4, '2015-01-26', '16:15:00', '17:45:00', '0'),
(13, 4, '2015-01-27', '14:30:00', '16:00:00', '0'),
(14, 5, '2015-01-26', '14:30:00', '16:00:00', '0'),
(15, 5, '2015-01-26', '18:00:00', '19:30:00', '0'),
(16, 6, '2015-01-29', '09:15:00', '10:45:00', '0'),
(17, 6, '2015-02-05', '09:15:00', '10:45:00', '0');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczelnia`
--

CREATE TABLE IF NOT EXISTS `uczelnia` (
`id` int(11) unsigned NOT NULL,
  `nazwa_uczelni` varchar(64) NOT NULL,
  `miejscowowsc` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `uczelnia`
--

INSERT INTO `uczelnia` (`id`, `nazwa_uczelni`, `miejscowowsc`) VALUES
(1, 'Politechnika Krakowska im. Tadeusza Kościuszki', 'Kraków'),
(2, 'Akademia Górniczo-Hutnicza im. Stanisława Staszica', 'Kraków');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wydzial`
--

CREATE TABLE IF NOT EXISTS `wydzial` (
`wydzial_id` int(11) unsigned NOT NULL,
  `uczelnia_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(64) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `wydzial`
--

INSERT INTO `wydzial` (`wydzial_id`, `uczelnia_id`, `nazwa`) VALUES
(1, 1, 'Wydział Architektury'),
(2, 1, 'Wydział Fizyki, Matematyki i Informatyki'),
(3, 1, 'Wydział Inżynierii Elektrycznej i Komputerowej'),
(4, 1, 'Wydział Inżynierii i Technologii Chemicznej'),
(5, 1, 'Wydział Inżynierii Lądowej'),
(6, 1, 'Wydział Inżynierii Środowiska'),
(7, 1, 'Wydział Mechaniczny'),
(8, 2, 'Wydział Górnictwa i Geoinżynierii'),
(9, 2, 'Wydział Inżynierii Metali\r\ni Informatyki Przemysłowej'),
(10, 2, 'Wydział Elektrotechniki,Automatyki, Informatyki iI.Biomedycznej'),
(11, 2, 'Wydział Informatyki, Elektroniki\r\ni Telekomunikacji'),
(12, 2, 'Wydział Inżynierii Mechanicznej\r\ni Robotyki'),
(13, 2, 'Wydział Geologii, Geofizyki\r\ni Ochrony Środowiska'),
(14, 2, 'Wydział Geodezji Górniczej\r\ni Inżynierii Środowiska'),
(15, 2, 'Wydział Inżynierii Materiałowej\r\ni Ceramiki'),
(16, 2, 'Wydział Odlewnictwa'),
(17, 2, 'Wydział Metali Nieżelaznych'),
(18, 2, 'Wydział Wiertnictwa, Nafty i Gazu'),
(19, 2, 'Wydział Zarządzania'),
(20, 2, 'Wydział Energetyki i Paliw'),
(21, 2, 'Wydział Fizyki i Informatyki Stosowanej'),
(22, 2, 'Wydział Matematyki Stosowanej'),
(23, 2, 'Wydział Humanistyczny');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zgloszenie`
--

CREATE TABLE IF NOT EXISTS `zgloszenie` (
`zgloszenie_id` int(11) unsigned NOT NULL,
  `student_id` int(11) unsigned NOT NULL,
  `termin_id` int(11) unsigned NOT NULL,
  `argument_id` int(11) unsigned NOT NULL,
  `typ_wyboru` int(2) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD KEY `account_ibfk_1` (`student_id`);

--
-- Indexes for table `argument`
--
ALTER TABLE `argument`
 ADD PRIMARY KEY (`argument_id`), ADD KEY `semestr_id` (`semestr_id`);

--
-- Indexes for table `chec_zmiany`
--
ALTER TABLE `chec_zmiany`
 ADD PRIMARY KEY (`chec_zmiany_id`), ADD KEY `termin_id` (`termin_id`), ADD KEY `przydzial_id` (`przydzial_id`);

--
-- Indexes for table `grupa`
--
ALTER TABLE `grupa`
 ADD PRIMARY KEY (`grupa_id`), ADD KEY `rok_id` (`rok_id`);

--
-- Indexes for table `grupa_student`
--
ALTER TABLE `grupa_student`
 ADD KEY `grupa_id` (`grupa_id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `kierunek`
--
ALTER TABLE `kierunek`
 ADD PRIMARY KEY (`kierunek_id`), ADD KEY `wydzial_id` (`wydzial_id`);

--
-- Indexes for table `ogloszenie`
--
ALTER TABLE `ogloszenie`
 ADD PRIMARY KEY (`ogloszenie_id`), ADD KEY `rok_id` (`rok_id`);

--
-- Indexes for table `ogloszenie_systemowe`
--
ALTER TABLE `ogloszenie_systemowe`
 ADD PRIMARY KEY (`ogloszenie_systemowe_id`);

--
-- Indexes for table `powiadomienie`
--
ALTER TABLE `powiadomienie`
 ADD PRIMARY KEY (`powiadomienie_id`), ADD KEY `rok_id` (`rok_id`);

--
-- Indexes for table `przedmiot`
--
ALTER TABLE `przedmiot`
 ADD PRIMARY KEY (`przedmiot_id`), ADD KEY `semestr_id` (`semestr_id`);

--
-- Indexes for table `przydzial`
--
ALTER TABLE `przydzial`
 ADD PRIMARY KEY (`przydzial_id`), ADD KEY `termin_id` (`termin_id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `rok`
--
ALTER TABLE `rok`
 ADD PRIMARY KEY (`rok_id`), ADD UNIQUE KEY `haslo` (`haslo`), ADD KEY `kierunek_id` (`kierunek_id`);

--
-- Indexes for table `semestr`
--
ALTER TABLE `semestr`
 ADD PRIMARY KEY (`semestr_id`), ADD KEY `rok_id` (`rok_id`);

--
-- Indexes for table `starosta`
--
ALTER TABLE `starosta`
 ADD PRIMARY KEY (`starosta_id`), ADD KEY `rok_id` (`rok_id`), ADD KEY `starosta_ibfk_1` (`student_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `termin`
--
ALTER TABLE `termin`
 ADD PRIMARY KEY (`termin_id`), ADD KEY `przedmiot_id` (`przedmiot_id`);

--
-- Indexes for table `uczelnia`
--
ALTER TABLE `uczelnia`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wydzial`
--
ALTER TABLE `wydzial`
 ADD PRIMARY KEY (`wydzial_id`), ADD KEY `uczelnia_id` (`uczelnia_id`);

--
-- Indexes for table `zgloszenie`
--
ALTER TABLE `zgloszenie`
 ADD PRIMARY KEY (`zgloszenie_id`), ADD KEY `argument_id` (`argument_id`), ADD KEY `termin_id` (`termin_id`), ADD KEY `student_id` (`student_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `account`
--
ALTER TABLE `account`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `argument`
--
ALTER TABLE `argument`
MODIFY `argument_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `chec_zmiany`
--
ALTER TABLE `chec_zmiany`
MODIFY `chec_zmiany_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `grupa`
--
ALTER TABLE `grupa`
MODIFY `grupa_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `kierunek`
--
ALTER TABLE `kierunek`
MODIFY `kierunek_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT dla tabeli `ogloszenie`
--
ALTER TABLE `ogloszenie`
MODIFY `ogloszenie_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `ogloszenie_systemowe`
--
ALTER TABLE `ogloszenie_systemowe`
MODIFY `ogloszenie_systemowe_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `powiadomienie`
--
ALTER TABLE `powiadomienie`
MODIFY `powiadomienie_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT dla tabeli `przedmiot`
--
ALTER TABLE `przedmiot`
MODIFY `przedmiot_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT dla tabeli `przydzial`
--
ALTER TABLE `przydzial`
MODIFY `przydzial_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT dla tabeli `rok`
--
ALTER TABLE `rok`
MODIFY `rok_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `semestr`
--
ALTER TABLE `semestr`
MODIFY `semestr_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `starosta`
--
ALTER TABLE `starosta`
MODIFY `starosta_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `student`
--
ALTER TABLE `student`
MODIFY `student_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT dla tabeli `termin`
--
ALTER TABLE `termin`
MODIFY `termin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `uczelnia`
--
ALTER TABLE `uczelnia`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `wydzial`
--
ALTER TABLE `wydzial`
MODIFY `wydzial_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT dla tabeli `zgloszenie`
--
ALTER TABLE `zgloszenie`
MODIFY `zgloszenie_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `account`
--
ALTER TABLE `account`
ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `argument`
--
ALTER TABLE `argument`
ADD CONSTRAINT `argument_ibfk_1` FOREIGN KEY (`semestr_id`) REFERENCES `semestr` (`semestr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `chec_zmiany`
--
ALTER TABLE `chec_zmiany`
ADD CONSTRAINT `chec_zmiany_ibfk_1` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`termin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `chec_zmiany_ibfk_2` FOREIGN KEY (`przydzial_id`) REFERENCES `przydzial` (`przydzial_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `grupa`
--
ALTER TABLE `grupa`
ADD CONSTRAINT `grupa_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `grupa_student`
--
ALTER TABLE `grupa_student`
ADD CONSTRAINT `grupa_student_ibfk_1` FOREIGN KEY (`grupa_id`) REFERENCES `grupa` (`grupa_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `grupa_student_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kierunek`
--
ALTER TABLE `kierunek`
ADD CONSTRAINT `kierunek_ibfk_1` FOREIGN KEY (`wydzial_id`) REFERENCES `wydzial` (`wydzial_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ogloszenie`
--
ALTER TABLE `ogloszenie`
ADD CONSTRAINT `ogloszenie_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `powiadomienie`
--
ALTER TABLE `powiadomienie`
ADD CONSTRAINT `powiadomienie_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `przedmiot`
--
ALTER TABLE `przedmiot`
ADD CONSTRAINT `przedmiot_ibfk_1` FOREIGN KEY (`semestr_id`) REFERENCES `semestr` (`semestr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `przydzial`
--
ALTER TABLE `przydzial`
ADD CONSTRAINT `przydzial_ibfk_1` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`termin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `przydzial_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `rok`
--
ALTER TABLE `rok`
ADD CONSTRAINT `rok_ibfk_1` FOREIGN KEY (`kierunek_id`) REFERENCES `kierunek` (`kierunek_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `semestr`
--
ALTER TABLE `semestr`
ADD CONSTRAINT `semestr_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `starosta`
--
ALTER TABLE `starosta`
ADD CONSTRAINT `starosta_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `starosta_ibfk_2` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `termin`
--
ALTER TABLE `termin`
ADD CONSTRAINT `termin_ibfk_1` FOREIGN KEY (`przedmiot_id`) REFERENCES `przedmiot` (`przedmiot_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wydzial`
--
ALTER TABLE `wydzial`
ADD CONSTRAINT `wydzial_ibfk_1` FOREIGN KEY (`uczelnia_id`) REFERENCES `uczelnia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zgloszenie`
--
ALTER TABLE `zgloszenie`
ADD CONSTRAINT `zgloszenie_ibfk_1` FOREIGN KEY (`argument_id`) REFERENCES `argument` (`argument_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `zgloszenie_ibfk_2` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`termin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `zgloszenie_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
