-- Database: znz

-- !WAZNE! utworzyc role znz_dev, haslo: znzDev123
-- CREATE USER 'znz_dev'@'localhost' IDENTIFIED BY 'znzDev123';
-- GRANT ALL ON znz.* TO 'znz_dev'@'localhost';
DROP DATABASE IF EXISTS znz;
create database znz DEFAULT COLLATE = utf8_polish_ci;

-- UZYTKOWNICY --
create table znz.account (
    id int unsigned not null auto_increment primary key,
    username varchar(50) unique not null,
    email varchar(50) not null,
    password varchar(64),
	authority varchar(5) not null default 'user',
    enabled boolean not null,
	reminder boolean not null,
	notification boolean not null,
    date_created timestamp default 0,
    date_modified timestamp default current_timestamp on update current_timestamp,
	deleted boolean not null default false,
	student_id int(11) unsigned
);



SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


--
-- Struktura tabeli dla tabeli `argument`
--

CREATE TABLE IF NOT EXISTS znz.`argument` (
  `argument_id` int(11) unsigned auto_increment NOT NULL primary key,
  `semestr_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(64) NOT NULL,
  `priorytet` tinyint(2) unsigned NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chec_zmiany`
--

CREATE TABLE IF NOT EXISTS znz.`chec_zmiany` (
  `chec_zmiany_id` int(11) unsigned auto_increment NOT NULL primary key,
  `przydzial_id` int(11) unsigned NOT NULL,
  `termin_id` int(11) unsigned NOT NULL,
  `zamkniete` tinyint(1) unsigned NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grupa`
--

CREATE TABLE IF NOT EXISTS znz.`grupa` (
  `grupa_id` int(11) unsigned auto_increment NOT NULL primary key, 
  `rok_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(6) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `grupa_student`
--

CREATE TABLE IF NOT EXISTS znz.`grupa_student` (
  `student_id` int(11) unsigned NOT NULL,
  `grupa_id` int(11) unsigned NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kierunek`
--

CREATE TABLE IF NOT EXISTS znz.`kierunek` (
  `kierunek_id` int(11) unsigned auto_increment NOT NULL primary key,
  `wydzial_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(30) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ogloszenie`
--

CREATE TABLE IF NOT EXISTS znz.`ogloszenie` (
  `ogloszenie_id` int(11) unsigned auto_increment NOT NULL primary key,
  `rok_id` int(11) unsigned NOT NULL,
  `tytul` varchar(30) NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_publikacji` datetime NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ogloszenie_systemowe`
--

CREATE TABLE IF NOT EXISTS znz.`ogloszenie_systemowe` (
  `ogloszenie_systemowe_id` int(11) unsigned auto_increment NOT NULL primary key,
  `tytul` varchar(30) NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_publikacji` datetime NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `powiadomienie`
--

CREATE TABLE IF NOT EXISTS znz.`powiadomienie` (
  `powiadomienie_id` int(11) unsigned auto_increment NOT NULL primary key,
  `rok_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `tresc` varchar(255) NOT NULL,
  `data_wyslania` datetime NOT NULL,
  `typ` varchar(30) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przedmiot`
--

CREATE TABLE IF NOT EXISTS znz.`przedmiot` (
  `przedmiot_id` int(11) unsigned auto_increment NOT NULL primary key,
  `semestr_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(64) NOT NULL,
  `min_studentow` tinyint(2) unsigned NOT NULL,
  `max_studentow` tinyint(2) unsigned NOT NULL,
  `dodatkowe_info` varchar(64) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przydzial`
--

CREATE TABLE IF NOT EXISTS znz.`przydzial` (
  `przydzial_id` int(11) unsigned auto_increment NOT NULL primary key,
  `student_id` int(11) unsigned NOT NULL,
  `termin_id` int(11) unsigned NOT NULL,
  `finalny` tinyint(1) unsigned NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `rok`
--

CREATE TABLE IF NOT EXISTS znz.`rok` (
  `rok_id` int(11) unsigned auto_increment NOT NULL primary key,
  `kierunek_id` int(11) unsigned NOT NULL,
  `haslo` varchar(50) unique not null,
  `rok_rozp` int(4) NOT NULL,
  `rok_zak` int(4) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `semestr`
--

CREATE TABLE IF NOT EXISTS znz.`semestr` (
  `semestr_id` int(11) unsigned auto_increment NOT NULL primary key,
  `rok_id` int(11) unsigned NOT NULL,
  `nr_semestru` tinyint(2) unsigned NOT NULL,
  `rozpoczecie_zapisow` date NOT NULL,
  `zakonczenie_zapisow` date NOT NULL,
  `zakonczenie_zamian` date NOT NULL,
  `stan` tinyint(1) not null
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `starosta`
--

CREATE TABLE IF NOT EXISTS znz.`starosta` (
  `starosta_id` int(11) unsigned auto_increment NOT NULL primary key,
  `student_id` int(11) unsigned NOT NULL,
  `rok_id` int(11) unsigned NOT NULL,
  `czy_zastepca` tinyint(1) unsigned NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `student`
--

CREATE TABLE IF NOT EXISTS znz.`student` (
  `student_id` int(11) unsigned auto_increment NOT NULL primary key,
  `imie` varchar(20) NOT NULL,
  `nazwisko` varchar(20) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `termin`
--

CREATE TABLE IF NOT EXISTS znz.`termin` (
  `termin_id` int(11) unsigned auto_increment NOT NULL primary key,
  `przedmiot_id` int(11) unsigned NOT NULL,
  `dzien` tinyint(1) NOT NULL,
  `godzina_rozp` time NOT NULL,
  `godzina_zak` time NOT NULL,
  `rodz_tyg` tinyint(1) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczelnia`
--

CREATE TABLE IF NOT EXISTS znz.`uczelnia` (
  `id` int(11) unsigned auto_increment NOT NULL primary key,
  `nazwa_uczelni` varchar(64) NOT NULL,
  `miejscowowsc` varchar(30) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wydzial`
--

CREATE TABLE IF NOT EXISTS znz.`wydzial` (
  `wydzial_id` int(11) unsigned auto_increment NOT NULL primary key,
  `uczelnia_id` int(11) unsigned NOT NULL,
  `nazwa` varchar(64) NOT NULL
);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zgloszenie`
--

CREATE TABLE IF NOT EXISTS znz.`zgloszenie` (
  `zgloszenie_id` int(11) unsigned auto_increment NOT NULL primary key,
  `student_id` int(11) unsigned NOT NULL,
  `termin_id` int(11) unsigned NOT NULL,
  `argument_id` int(11) unsigned NOT NULL,
  `typ_wyboru` int(2) unsigned NOT NULL
);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `argument`
--
ALTER TABLE znz.`argument`
 ADD KEY `semestr_id` (`semestr_id`);

--
-- Indexes for table `chec_zmiany`
--
ALTER TABLE znz.`chec_zmiany`
 ADD KEY `termin_id` (`termin_id`), ADD KEY `przydzial_id` (`przydzial_id`);

--
-- Indexes for table `grupa`
--
ALTER TABLE znz.`grupa`
 ADD KEY `rok_id` (`rok_id`);

--
-- Indexes for table `grupa_student`
--
ALTER TABLE znz.`grupa_student`
 ADD KEY `grupa_id` (`grupa_id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `kierunek`
--
ALTER TABLE znz.`kierunek`
 ADD KEY `wydzial_id` (`wydzial_id`);

--
-- Indexes for table `ogloszenie`
--
ALTER TABLE znz.`ogloszenie`
ADD KEY `rok_id` (`rok_id`);



--
-- Indexes for table `powiadomienie`
--
ALTER TABLE znz.`powiadomienie`
 ADD KEY `rok_id` (`rok_id`);

--
-- Indexes for table `przedmiot`
--
ALTER TABLE znz.`przedmiot`
 ADD KEY `semestr_id` (`semestr_id`);

--
-- Indexes for table `przydzial`
--
ALTER TABLE znz.`przydzial`
 ADD KEY `termin_id` (`termin_id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `rok`
--
ALTER TABLE znz.`rok`
 ADD KEY `kierunek_id` (`kierunek_id`);

--
-- Indexes for table `semestr`
--
ALTER TABLE znz.`semestr`
 ADD KEY `rok_id` (`rok_id`);


--

 --
-- Indexes for table `starosta`
--
ALTER TABLE znz.`starosta`
 ADD KEY `rok_id` (`rok_id`);


--
-- Indexes for table `termin`
--
ALTER TABLE znz.`termin`
 ADD KEY `przedmiot_id` (`przedmiot_id`);



--
-- Indexes for table `wydzial`
--
ALTER TABLE znz.`wydzial`
  ADD KEY `uczelnia_id` (`uczelnia_id`);

--
-- Indexes for table `zgloszenie`
--
ALTER TABLE znz.`zgloszenie`
  ADD KEY `argument_id` (`argument_id`), ADD KEY `termin_id` (`termin_id`), ADD KEY `student_id` (`student_id`);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `argument`
--
ALTER TABLE znz.`argument`
ADD CONSTRAINT `argument_ibfk_1` FOREIGN KEY (`semestr_id`) REFERENCES `semestr` (`semestr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `chec_zmiany`
--
ALTER TABLE znz.`chec_zmiany`
ADD CONSTRAINT `chec_zmiany_ibfk_1` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`termin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `chec_zmiany_ibfk_2` FOREIGN KEY (`przydzial_id`) REFERENCES `przydzial` (`przydzial_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `grupa`
--
ALTER TABLE znz.`grupa`
ADD CONSTRAINT `grupa_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `grupa_student`
--
ALTER TABLE znz.`grupa_student`
ADD CONSTRAINT `grupa_student_ibfk_1` FOREIGN KEY (`grupa_id`) REFERENCES `grupa` (`grupa_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `grupa_student_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `kierunek`
--
ALTER TABLE znz.`kierunek`
ADD CONSTRAINT `kierunek_ibfk_1` FOREIGN KEY (`wydzial_id`) REFERENCES `wydzial` (`wydzial_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `ogloszenie`
--
ALTER TABLE znz.`ogloszenie`
ADD CONSTRAINT `ogloszenie_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `powiadomienie`
--
ALTER TABLE znz.`powiadomienie`
ADD CONSTRAINT `powiadomienie_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `przedmiot`
--
ALTER TABLE znz.`przedmiot`
ADD CONSTRAINT `przedmiot_ibfk_1` FOREIGN KEY (`semestr_id`) REFERENCES `semestr` (`semestr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `przydzial`
--
ALTER TABLE znz.`przydzial`
ADD CONSTRAINT `przydzial_ibfk_1` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`termin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `przydzial_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `rok`
--
ALTER TABLE znz.`rok`
ADD CONSTRAINT `rok_ibfk_1` FOREIGN KEY (`kierunek_id`) REFERENCES `kierunek` (`kierunek_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `semestr`
--
ALTER TABLE znz.`semestr`
ADD CONSTRAINT `semestr_ibfk_1` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `starosta`
--
ALTER TABLE znz.`starosta`
ADD CONSTRAINT `starosta_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `starosta_ibfk_2` FOREIGN KEY (`rok_id`) REFERENCES `rok` (`rok_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `termin`
--
ALTER TABLE znz.`termin`
ADD CONSTRAINT `termin_ibfk_1` FOREIGN KEY (`przedmiot_id`) REFERENCES `przedmiot` (`przedmiot_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `wydzial`
--
ALTER TABLE znz.`wydzial`
ADD CONSTRAINT `wydzial_ibfk_1` FOREIGN KEY (`uczelnia_id`) REFERENCES `uczelnia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `zgloszenie`
--
ALTER TABLE znz.`zgloszenie`
ADD CONSTRAINT `zgloszenie_ibfk_1` FOREIGN KEY (`argument_id`) REFERENCES `argument` (`argument_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `zgloszenie_ibfk_2` FOREIGN KEY (`termin_id`) REFERENCES `termin` (`termin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `zgloszenie_ibfk_3` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE znz.`account`
ADD CONSTRAINT `account_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE;