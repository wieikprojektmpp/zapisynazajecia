package wieik.znz.api.exceptions;

/**
 * Created by Sebastian on 2015-01-15.
 */
public class NotFoundException extends ZNZException {
    public NotFoundException() {
        super();
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}
