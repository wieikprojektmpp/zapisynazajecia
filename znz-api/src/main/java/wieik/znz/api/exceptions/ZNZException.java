package wieik.znz.api.exceptions;

/**
 * Created by Sebastian on 2014-10-31.
 */
public class ZNZException extends Exception {
    public ZNZException() {
        super();
    }

    public ZNZException(String message) {
        super(message);
    }

    public ZNZException(String message, Throwable cause) {
        super(message, cause);
    }

    public ZNZException(Throwable cause) {
        super(cause);
    }
}
