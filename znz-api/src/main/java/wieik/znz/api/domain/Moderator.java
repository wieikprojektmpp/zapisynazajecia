package wieik.znz.api.domain;

import javax.persistence.*;

;

/**
 * Created by Sebastian on 2015-01-09.
 * Starosta
 */
@Entity
@Table(name = "starosta")
@NamedQueries({
        @NamedQuery(
                name = "FindModeratorsForYearId",
                query = "select m from Moderator m where m.rokId.rokId = :yearId")
})
public class Moderator {
    Year rokId;
    Student student;
    private long starostaId;
    private boolean czyZastepca;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "starosta_id")
    public long getStarostaId() {
        return starostaId;
    }

    public void setStarostaId(long starostaId) {
        this.starostaId = starostaId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rok_id")
    public Year getRokId() {
        return rokId;
    }

    public void setRokId(Year rokId) {
        this.rokId = rokId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Column(name = "czy_zastepca")
    public boolean isCzyZastepca() {
        return czyZastepca;
    }

    public void setCzyZastepca(boolean czyZastepca) {
        this.czyZastepca = czyZastepca;
    }
}
