package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Grupa
 */
@Entity
@Table(name = "grupa")
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "addStudentToGroup",
                query = "insert into grupa_student values (:studentId, :groupId)",
                resultSetMapping = "hibernateBug")
})
@NamedQueries({
        @NamedQuery(
                name = "FindGroupsForStudent",
                query = "select g from Group g join g.studenci s where s.studentId = :studentId"),
        @NamedQuery(
                name = "FindGroupsForYear",
                query = "select g from Group g where g.rokId.rokId = :yearId")
})
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "hibernateBug")
})
public class Group {
    private long grupaId;
    private Year rokId;
    private String nazwa;
    private Set<Student> studenci;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "grupy")
    public Set<Student> getStudenci() {
        return studenci;
    }

    public void setStudenci(Set<Student> studenci) {
        this.studenci = studenci;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "grupa_id")
    public long getGrupaId() {
        return grupaId;
    }

    public void setGrupaId(long grupaId) {
        this.grupaId = grupaId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rok_id")
    public Year getRokId() {
        return rokId;
    }

    public void setRokId(Year rokId) {
        this.rokId = rokId;
    }


    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
