package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Wydzial
 */
@Entity
@Table(name = "wydzial")
@NamedQueries({
        @NamedQuery(
                name = "FindDepartmentByNameAndSchoolId",
                query = "select d from Department d where d.nazwa = :nazwa and d.uczelniaId.uczelniaId = :schoolId"),
        @NamedQuery(
                name = "FindDepartmentsForSchoolId",
                query = "select d from Department d where d.uczelniaId.uczelniaId = :schoolId"),
        @NamedQuery(
                name = "FindDepartmentByCourseId",
                query = "select distinct c from Department c join c.kierunki y where y.kierunekId = :courseId")
})
public class Department {
    School uczelniaId;
    Set<Course> kierunki;
    private long wydzialId;
    private String nazwa;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "wydzial_id")
    public long getWydzialId() {
        return wydzialId;
    }

    public void setWydzialId(long wydzialId) {
        this.wydzialId = wydzialId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uczelnia_id")
    public School getUczelniaId() {
        return uczelniaId;
    }

    public void setUczelniaId(School uczelniaId) {
        this.uczelniaId = uczelniaId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wydzialId")
    public Set<Course> getKierunki() {
        return kierunki;
    }

    public void setKierunki(Set<Course> kierunki) {
        this.kierunki = kierunki;
    }


    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
}
