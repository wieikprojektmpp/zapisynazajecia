package wieik.znz.api.domain;

import javax.persistence.*;

/**
 * Created by Sebastian on 2015-01-09.
 * Chec zmiany
 */
@Entity
@Table(name = "chec_zmiany")
@NamedQueries({
        @NamedQuery(
                name = "getSwapsForStudent",
                query = "select s from Swap s where s.przydzialId.studentId.studentId = :studentId"
        ),
        @NamedQuery(
                name = "getSwapsForTerm",
                query = "select s from Swap s where s.terminId = :termId"
        ),
        @NamedQuery(
                name = "getSwapForAllocation",
                query = "select s from Swap s where s.przydzialId.przydzialId = :allocationId"
        ),
        @NamedQuery(
                name = "getSwapsForAllocation",
                query = "select s from Swap s where s.przydzialId.przydzialId = :allocationId and s.zamkniete = 0"
        ),
        @NamedQuery(
                name = "getFreeToSwapsForTerm",
                query = "select s from Swap s where s.terminId.terminId = :termId and s.zamkniete = 0"
        ),
        @NamedQuery(
                name = "getFreeFromSwapsForTerm",
                query = "select s from Swap s join s.przydzialId a join a.terminId t where t.terminId = :termId and s.zamkniete = 0"
        )
})
public class Swap {
    Allocation przydzialId;
    Term terminId;
    private long checZmianyId;
    private boolean zamkniete;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "chec_zmiany_id")
    public long getChecZmianyId() {
        return checZmianyId;
    }

    public void setChecZmianyId(long checZmianyId) {
        this.checZmianyId = checZmianyId;
    }

    @Column(name = "zamkniete")
    public boolean isZamkniete() {
        return zamkniete;
    }

    public void setZamkniete(boolean zamkniete) {
        this.zamkniete = zamkniete;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "przydzial_id")
    public Allocation getPrzydzialId() {
        return przydzialId;
    }

    public void setPrzydzialId(Allocation przydzialId) {
        this.przydzialId = przydzialId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "termin_id")
    public Term getTerminId() {
        return terminId;
    }

    public void setTerminId(Term terminId) {
        this.terminId = terminId;
    }
}
