package wieik.znz.api.domain;

import javax.persistence.*;
import java.sql.Time;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Termin
 */
@Entity
@Table(name = "termin")
@NamedQueries({
        @NamedQuery(
                name = "getTermsForSubject",
                query = "select t from Term t where t.przedmiotId.przedmiotId = :subjectId"
        ),
        @NamedQuery(
                name = "getToTermForSwap",
                query = "select t from Term t join t.checiZamiany s where s.checZmianyId = :swapId"
        ),
        @NamedQuery(
                name = "getFromTermForSwap",
                query = "select t from Term t join t.przydzialy a join a.checiZmiany s where s.checZmianyId = :swapId"
        )
})
public class Term {
    Subject przedmiotId;
    Set<Swap> checiZamiany;
    Set<Allocation> przydzialy;
    Set<Application> zgloszenia;
    private long terminId;
    private int dzien;
    private Time godzRozp;
    private Time godzZak;

    private int rodzajTygodnia;//0-oba, 1- nieparzysty, 2 - parzysty

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "terminId")
    public Set<Application> getZgloszenia() {
        return zgloszenia;
    }

    public void setZgloszenia(Set<Application> zgloszenia) {
        this.zgloszenia = zgloszenia;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "terminId")
    public Set<Swap> getCheciZamiany() {
        return checiZamiany;
    }

    public void setCheciZamiany(Set<Swap> checiZamiany) {
        this.checiZamiany = checiZamiany;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "terminId")
    public Set<Allocation> getPrzydzialy() {
        return przydzialy;
    }

    public void setPrzydzialy(Set<Allocation> przydzialy) {
        this.przydzialy = przydzialy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "termin_id")
    public long getTerminId() {
        return terminId;
    }

    public void setTerminId(long terminId) {
        this.terminId = terminId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "przedmiot_id")
    public Subject getPrzedmiotId() {
        return przedmiotId;
    }

    public void setPrzedmiotId(Subject przedmiotId) {
        this.przedmiotId = przedmiotId;
    }

    @Column(name = "dzien")
    public int getDzien() {
        return dzien;
    }

    public void setDzien(int dzien) {
        this.dzien = dzien;
    }

    @Column(name = "godzina_rozp")
    public Time getGodzRozp() {
        return godzRozp;
    }

    public void setGodzRozp(Time godzRozp) {
        this.godzRozp = godzRozp;
    }

    @Column(name = "godzina_zak")
    public Time getGodzZak() {
        return godzZak;
    }

    public void setGodzZak(Time godzZak) {
        this.godzZak = godzZak;
    }

    @Column(name = "rodz_tyg")
    public int getRodzajTygodnia() {
        return rodzajTygodnia;
    }

    public void setRodzajTygodnia(int rodzajTygodnia) {
        this.rodzajTygodnia = rodzajTygodnia;
    }
}