package wieik.znz.api.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Uczelnia
 */
@Entity
@Table(name = "uczelnia")
@NamedQueries({
        @NamedQuery(
                name = "findSchoolByName",
                query = "select s from School s where s.nazwaUczelni = :nazwaUczelni"),
        @NamedQuery(
                name = "FindSchoolByDepartmentId",
                query = "select distinct c from School c join c.wydzialy y where y.wydzialId = :departmentId")
})
public class School {
    Set<Department> wydzialy;
    private long uczelniaId;
    private String nazwaUczelni, miejscowosc;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "uczelniaId")
    public Set<Department> getWydzialy() {
        return wydzialy;
    }

    public void setWydzialy(Set<Department> wydzialy) {
        this.wydzialy = wydzialy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    public long getUczelniaId() {
        return uczelniaId;
    }

    public void setUczelniaId(long uczelniaId) {
        this.uczelniaId = uczelniaId;
    }

    @NotNull
    @Size(min = 2, max = 64)
    @Column(name = "nazwa_uczelni")
    public String getNazwaUczelni() {
        return nazwaUczelni;
    }

    public void setNazwaUczelni(String nazwaUczelni) {
        this.nazwaUczelni = nazwaUczelni;
    }

    @NotNull
    @Size(min = 3, max = 30)
    @Column(name = "miejscowowsc")
    public String getMiejscowosc() {
        return miejscowosc;
    }

    public void setMiejscowosc(String miejscowosc) {
        this.miejscowosc = miejscowosc;
    }

    @Override
    public String toString() {
        return "School{" +
                "uczelniaId=" + uczelniaId +
                ", nazwaUczelni='" + nazwaUczelni + '\'' +
                ", miejscowosc='" + miejscowosc + '\'' +
                '}';
    }
}
