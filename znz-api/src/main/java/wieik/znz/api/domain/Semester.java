package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Semestr
 */
@Entity
@Table(name = "semestr")
@NamedQueries({
        @NamedQuery(
                name = "getNewestSemester",
                query = "select s from Semester s where s.rokId.rokId = :yearId and s.stan != 5"
        ),
        @NamedQuery(
                name = "getSemestersForYear",
                query = "select s from Semester s where s.rokId.rokId = :yearId"
        ),
        @NamedQuery(
                name = "getSemesterByTerm",
                query = "select s from Semester s join s.przedmioty su join su.terminy t where t.terminId = :termId"
        )
})
public class Semester {
    Set<Subject> przedmioty;
    Year rokId;
    Set<Argument> argumenty;
    private long semestrId;
    private int nrSemestru;
    private Date rozpoczecieZapisow;
    private Date zakonczenieZapisow;
    private Date zakonczenieZamian;
    /**
     * 0 - w przygotowaniu, 1 - rozpoczete zapisy, 2 - zakonczone zapisy, 3 - zakonczone zamiany, 4 - trwanie semestru, 5 - semestr zamkniety
     */
    private int stan;

    @Column(name = "rozpoczecie_zapisow")
    public Date getRozpoczecieZapisow() {
        return rozpoczecieZapisow;
    }

    public void setRozpoczecieZapisow(Date rozpoczecieZapisow) {
        this.rozpoczecieZapisow = rozpoczecieZapisow;
    }

    @Column(name = "zakonczenie_zapisow")
    public Date getZakonczenieZapisow() {
        return zakonczenieZapisow;
    }

    public void setZakonczenieZapisow(Date zakonczenieZapisow) {
        this.zakonczenieZapisow = zakonczenieZapisow;
    }

    @Column(name = "zakonczenie_zamian")
    public Date getZakonczenieZamian() {
        return zakonczenieZamian;
    }

    public void setZakonczenieZamian(Date zakonczenieZamian) {
        this.zakonczenieZamian = zakonczenieZamian;
    }

    @Column(name = "stan")
    public int getStan() {
        return stan;
    }

    public void setStan(int stan) {
        this.stan = stan;
    }

    public void nastepnyStan() {
        if(stan < 5)
            ++stan;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "semestr_id")
    public long getSemestrId() {
        return semestrId;
    }

    public void setSemestrId(long semestrId) {
        this.semestrId = semestrId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rok_id")
    public Year getRokId() {
        return rokId;
    }

    public void setRokId(Year rokId) {
        this.rokId = rokId;
    }


    @Column(name = "nr_semestru")
    public int getNrSemestru() {
        return nrSemestru;
    }

    public void setNrSemestru(int nrSemestru) {
        this.nrSemestru = nrSemestru;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "semesterId")
    public Set<Subject> getPrzedmioty() {
        return przedmioty;
    }

    public void setPrzedmioty(Set<Subject> przedmioty) {
        this.przedmioty = przedmioty;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "semestrId")
    public Set<Argument> getArgumenty() {
        return argumenty;
    }

    public void setArgumenty(Set<Argument> argumenty) {
        this.argumenty = argumenty;
    }
}
