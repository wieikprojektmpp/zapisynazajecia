package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Przydzial
 */
@Entity
@Table(name = "przydzial")
@NamedQueries({
        @NamedQuery(
                name = "findAllocationByTerm",
                query = "select a from Allocation a where a.terminId.terminId = :terminId"),
        @NamedQuery(
                name = "findAllocationByStudent",
                query = "select a from Allocation a where a.studentId = :studentId"),
        @NamedQuery(
                name = "getAllocationForSwap",
                query = "select a from Allocation a join a.checiZmiany s where s.checZmianyId = :swapId "
        )
})
public class Allocation {
    Student studentId;
    Term terminId;
    Set<Swap> checiZmiany;
    private long przydzialId;
    private boolean finalny;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "przydzial_id")
    public long getPrzydzialId() {
        return przydzialId;
    }

    public void setPrzydzialId(long przydzialId) {
        this.przydzialId = przydzialId;
    }


    @Column(name = "finalny")
    public boolean isFinalny() {
        return finalny;
    }

    public void setFinalny(boolean finalny) {
        this.finalny = finalny;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    public Student getStudentId() {
        return studentId;
    }

    public void setStudentId(Student studentId) {
        this.studentId = studentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "termin_id")
    public Term getTerminId() {
        return terminId;
    }

    public void setTerminId(Term terminId) {
        this.terminId = terminId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "przydzialId")
    public Set<Swap> getCheciZmiany() {
        return checiZmiany;
    }

    public void setCheciZmiany(Set<Swap> checiZmiany) {
        this.checiZmiany = checiZmiany;
    }
}