package wieik.znz.api.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Student
 */
@Entity
@Table(name = "student")
@NamedQueries({
        @NamedQuery(
                name = "findStudentsForYear",
                query = "select a from Student a join a.grupy b where b.rokId.rokId = :rokId"),
        @NamedQuery(
                name = "findStudentByName",
                query = "select s from Student s where s.imie = :imie and s.nazwisko = :nazwisko"),
        @NamedQuery(
                name = "findStudentByUsername",
                query = "select distinct a from Student a, Account b where b.student.studentId = a.studentId and b.username = :username"),
        @NamedQuery(
                name = "FindStudentsForGroup",
                query = "select s from Student s join s.grupy g where g.grupaId = :groupId"),
        @NamedQuery(
                name = "getStudentModerator",
                query = "select s from Moderator m join m.student s where m.starostaId = :moderatorId"
        ),
        @NamedQuery(
                name = "countStudentsForYear",
                query = "select count(a) from Student a join a.grupy b where b.rokId.rokId = :rokId"),
        @NamedQuery(
                name = "getStudentForAllocation",
                query = "select s from Student s join s.przydzialy a where a.przydzialId = :allocationId"
        ),
        @NamedQuery(
                name="getStudentForSwap",
                query = "select s from Student s join s.przydzialy a join a.checiZmiany sw where sw.checZmianyId = :swapId"
        )
})
public class Student {
    Set<Allocation> przydzialy;
    Set<Application> zgloszenia;
    Set<Group> grupy;
    private long studentId;
    private String imie, nazwisko;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "grupa_student",
            joinColumns = {
                    @JoinColumn(name = "student_id", nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "grupa_id", nullable = false, updatable = false)})
    public Set<Group> getGrupy() {
        return grupy;
    }

    public void setGrupy(Set<Group> grupy) {
        this.grupy = grupy;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "studentId")
    public Set<Application> getZgloszenia() {
        return zgloszenia;
    }

    public void setZgloszenia(Set<Application> zgloszenia) {
        this.zgloszenia = zgloszenia;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "studentId")
    public Set<Allocation> getPrzydzialy() {
        return przydzialy;
    }

    public void setPrzydzialy(Set<Allocation> przydzialy) {
        this.przydzialy = przydzialy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "student_id")
    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    @NotNull
    @Size(min = 3, max = 20)
    @Column(name = "imie")
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    @NotNull
    @Size(min = 3, max = 20)
    @Column(name = "nazwisko")
    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }


}
