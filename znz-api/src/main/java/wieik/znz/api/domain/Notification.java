package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sebastian on 2015-01-09.
 * Powiadomienie
 */
@Entity
@Table(name = "powiadomienie")
@NamedQueries({
        @NamedQuery(
                name = "getNotificationsForYear",
                query = "select n from Notification n where n.rokId.rokId = :yearId"
        )
})
public class Notification {
    Year rokId;
    private long powiadomienieId;
    private String nazwa, tresc, typ;
    private Date dataWyslania;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "powiadomienie_id")
    public long getPowiadomienieId() {
        return powiadomienieId;
    }

    public void setPowiadomienieId(long powiadomienieId) {
        this.powiadomienieId = powiadomienieId;
    }

    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Column(name = "tresc")
    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    @Column(name = "typ")
    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    @Column(name = "data_wyslania")
    public Date getDataWyslania() {
        return dataWyslania;
    }

    public void setDataWyslania(Date dataWyslania) {
        this.dataWyslania = dataWyslania;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rok_id")
    public Year getRokId() {
        return rokId;
    }

    public void setRokId(Year rokId) {
        this.rokId = rokId;
    }
}
