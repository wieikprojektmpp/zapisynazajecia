package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Rok
 */
@Entity
@Table(name = "rok")
@NamedQueries({
        @NamedQuery(
                name = "FindYearsForGroup",
                query = "select y from Year y join y.grupy g where g.grupaId = :groupId"
        ),
        @NamedQuery(
                name = "FindYearsForCourse",
                query = "select y from Year y where y.kierunekId.kierunekId = :coursetId"
        ),
        @NamedQuery(
                name = "getYearBySemesterId",
                query = "select y from Year y join y.semestry s where s.semestrId = :semesterId"
        ),
        @NamedQuery(
                name = "getYearForAnnouncementWith",
                query = "select y from Year y join y.ogloszenia a where a.ogloszenieId = :announcementId"
        )
})
public class Year {
    Course kierunekId;
    Set<Semester> semestry;
    Set<Announcement> ogloszenia;
    Set<Moderator> starosci;
    Set<Notification> powiadomienia;
    Set<Group> grupy;
    private long rokId;
    private int rokRozp, rokZak;
    private String password;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rokId")
    public Set<Group> getGrupy() {
        return grupy;
    }

    public void setGrupy(Set<Group> grupy) {
        this.grupy = grupy;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rok_id")
    public long getRokId() {
        return rokId;
    }

    public void setRokId(long rokId) {
        this.rokId = rokId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kierunek_id")
    public Course getKierunekId() {
        return kierunekId;
    }

    public void setKierunekId(Course kierunekId) {
        this.kierunekId = kierunekId;
    }


    @Column(name = "rok_rozp")
    public int getRokRozp() {
        return rokRozp;
    }

    public void setRokRozp(int rokRozp) {
        this.rokRozp = rokRozp;
    }

    @Column(name = "rok_zak")
    public int getRokZak() {
        return rokZak;
    }

    public void setRokZak(int rokZak) {
        this.rokZak = rokZak;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rokId")
    public Set<Semester> getSemestry() {
        return semestry;
    }

    public void setSemestry(Set<Semester> semestry) {
        this.semestry = semestry;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rokId")
    public Set<Announcement> getOgloszenia() {
        return ogloszenia;
    }

    public void setOgloszenia(Set<Announcement> ogloszenia) {
        this.ogloszenia = ogloszenia;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rokId")
    public Set<Moderator> getStarosci() {
        return starosci;
    }

    public void setStarosci(Set<Moderator> starosci) {
        this.starosci = starosci;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rokId")
    public Set<Notification> getPowiadomienia() {
        return powiadomienia;
    }

    public void setPowiadomienia(Set<Notification> powiadomienia) {
        this.powiadomienia = powiadomienia;
    }

    @Column(name = "haslo")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
