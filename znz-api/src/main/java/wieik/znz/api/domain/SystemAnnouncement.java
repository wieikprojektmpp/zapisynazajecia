package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sebastian on 2015-01-09.
 * Ogloszenie systemowe
 */
@Entity
@Table(name = "ogloszenie_systemowe")
public class SystemAnnouncement {
    private long ogloszenieSystemoweId;
    private String tytul, tresc;
    private Date dataPublikacji;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ogloszenie_systemowe_id")
    public long getOgloszenieSystemoweId() {
        return ogloszenieSystemoweId;
    }

    public void setOgloszenieSystemoweId(long ogloszenieSystemoweId) {
        this.ogloszenieSystemoweId = ogloszenieSystemoweId;
    }

    @Column(name = "tytul")
    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    @Column(name = "tresc")
    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    @Column(name = "data_publikacji")
    public Date getDataPublikacji() {
        return dataPublikacji;
    }

    public void setDataPublikacji(Date dataPublikacji) {
        this.dataPublikacji = dataPublikacji;
    }
}
