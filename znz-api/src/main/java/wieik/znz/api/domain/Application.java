package wieik.znz.api.domain;

import javax.persistence.*;

/**
 * Created by Sebastian on 2015-01-09.
 * Zgloszenie
 */
@Entity
@Table(name = "zgloszenie")
@NamedQueries({
        @NamedQuery(
                name = "getApplicationsForTerm",
                query = "select a from Application a where a.terminId.terminId = :termId"),
        @NamedQuery(
                name = "getApplicationsForStudentInSemester",
                query = "select a from Application a where a.studentId = :studentId and a.terminId.przedmiotId.semesterId.semestrId = :semesterId"),
        @NamedQuery(
                name = "countApplicationsForTerm",
                query = "select count(a) from Application a where a.terminId.terminId = :termId")
})
public class Application {
    Student studentId;
    Term terminId;
    Argument argumentId;
    private long zgloszenieId;
    private int typ_wyboru;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "zgloszenie_id")
    public long getZgloszenieId() {
        return zgloszenieId;
    }

    public void setZgloszenieId(long zgloszenieId) {
        this.zgloszenieId = zgloszenieId;
    }

    @Column(name = "typ_wyboru")
    public int getTyp_wyboru() {
        return typ_wyboru;
    }

    public void setTyp_wyboru(int typ_wyboru) {
        this.typ_wyboru = typ_wyboru;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    public Student getStudentId() {
        return studentId;
    }

    public void setStudentId(Student studentId) {
        this.studentId = studentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "termin_id")
    public Term getTerminId() {
        return terminId;
    }

    public void setTerminId(Term terminId) {
        this.terminId = terminId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "argument_id")
    public Argument getArgumentId() {
        return argumentId;
    }

    public void setArgumentId(Argument argumentId) {
        this.argumentId = argumentId;
    }


}
