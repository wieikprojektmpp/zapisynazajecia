package wieik.znz.api.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Przedmiot
 */
@Entity
@Table(name = "przedmiot")
@NamedQueries({
        @NamedQuery(
                name = "getSubjectsForSemester",
                query = "select s from Subject s where s.semesterId.semestrId = :semesterId"
        )
})
public class Subject {
    Set<Term> terminy;
    Semester semesterId;
    private long przedmiotId;
    private String nazwa, dodatkoweInfo;
    private int minStudentow, maxStudentow;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "przedmiot_id")
    public long getPrzedmiotId() {
        return przedmiotId;
    }

    public void setPrzedmiotId(long przedmiotId) {
        this.przedmiotId = przedmiotId;
    }

    @Size(min = 3, max = 64)
    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Column(name = "dodatkowe_info")
    public String getDodatkoweInfo() {
        return dodatkoweInfo;
    }

    public void setDodatkoweInfo(String dodatkoweInfo) {
        this.dodatkoweInfo = dodatkoweInfo;
    }

    @Column(name = "min_studentow")
    public int getMinStudentow() {
        return minStudentow;
    }

    public void setMinStudentow(int minStudentow) {
        this.minStudentow = minStudentow;
    }

    @Column(name = "max_studentow")
    public int getMaxStudentow() {
        return maxStudentow;
    }

    public void setMaxStudentow(int maxStudentow) {
        this.maxStudentow = maxStudentow;
    }


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "przedmiotId")
    public Set<Term> getTerminy() {
        return terminy;
    }

    public void setTerminy(Set<Term> terminy) {
        this.terminy = terminy;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "semestr_id")
    public Semester getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(Semester semesterId) {
        this.semesterId = semesterId;
    }
}
