package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Kierunek
 */
@Entity
@Table(name = "kierunek")
@NamedQueries({
        @NamedQuery(
                name = "findCourseByNameAndDepartmentId",
                query = "select c from Course c join c.wydzialId d where c.nazwa = :nazwa and d.wydzialId = :wydzialId"),
        @NamedQuery(
                name = "findCourseByName",
                query = "select c from Course c where c.nazwa = :nazwa"),
        @NamedQuery(
                name = "findCourseByDepartmentId",
                query = "select c from Course c where c.wydzialId.wydzialId = :departmentId"),
        @NamedQuery(
                name = "FindCourseByYearId",
                query = "select distinct c from Course c join c.lata y where y.rokId = :yearId")})
public class Course {
    Department wydzialId;
    Set<Year> lata;
    private long kierunekId;
    private String nazwa;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "kierunek_id")
    public long getKierunekId() {
        return kierunekId;
    }

    public void setKierunekId(long kierunekId) {
        this.kierunekId = kierunekId;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wydzial_id")
    public Department getWydzialId() {
        return wydzialId;
    }

    public void setWydzialId(Department wydzialId) {
        this.wydzialId = wydzialId;
    }

    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kierunekId")
    public Set<Year> getLata() {
        return lata;
    }

    public void setLata(Set<Year> lata) {
        this.lata = lata;
    }
}
