package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Sebastian on 2015-01-09.
 * Ogloszenie
 */
@Entity
@Table(name = "ogloszenie")
@NamedQueries({
        @NamedQuery(
                name = "findAnnouncementsByYearId",
                query = "select a from Announcement a where a.rokId.rokId = :yearId"),
        @NamedQuery(
                name = "getYearIdForAnnouncement",
                query = "select y.rokId from Year y join y.ogloszenia a where a.ogloszenieId = :announcementId"
        )
})
public class Announcement {
    Year rokId;
    private long ogloszenieId;
    private String nazwa, tresc;
    private Date dataPublikacji;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ogloszenie_id")
    public long getOgloszenieId() {
        return ogloszenieId;
    }

    public void setOgloszenieId(long ogloszenieId) {
        this.ogloszenieId = ogloszenieId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rok_id")
    public Year getRokId() {
        return rokId;
    }

    public void setRokId(Year rokId) {
        this.rokId = rokId;
    }

    @Column(name = "tytul")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Column(name = "tresc")
    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    @Column(name = "data_publikacji")
    public Date getDataPublikacji() {
        return dataPublikacji;
    }

    public void setDataPublikacji(Date dataPublikacji) {
        this.dataPublikacji = dataPublikacji;
    }

}
