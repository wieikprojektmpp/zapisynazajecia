package wieik.znz.api.domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Sebastian on 2015-01-09.
 * Argument
 */

@Entity
@Table(name = "argument")
@NamedQueries({
        @NamedQuery(
                name = "findArgumentByUsername",
                query = "select a from Argument a where a.nazwa = :nazwa"),
        @NamedQuery(
                name = "getArgumentsForSemester",
                query = "select a from Argument a where a.semestrId.semestrId = :semesterId")
})
public class Argument {
    Semester semestrId;
    Set<Application> zgloszenia;
    private long argumentId;
    private String nazwa;
    private int priorytet;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "argumentId")
    public Set<Application> getZgloszenia() {
        return zgloszenia;
    }

    public void setZgloszenia(Set<Application> zgloszenia) {
        this.zgloszenia = zgloszenia;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "argument_id")
    public long getArgumentId() {
        return argumentId;
    }

    public void setArgumentId(long argumentId) {
        this.argumentId = argumentId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "semestr_id")
    public Semester getSemestrId() {
        return semestrId;
    }

    public void setSemestrId(Semester semestrId) {
        this.semestrId = semestrId;
    }

    @Column(name = "nazwa")
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    @Column(name = "priorytet")
    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }
}
