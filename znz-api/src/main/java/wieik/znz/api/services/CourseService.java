package wieik.znz.api.services;

import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface CourseService {
    long createCourse(Course course) throws ZNZException;
    void updateCourse(Course course) throws ZNZException;
    void deleteCourse(Course course) throws ZNZException;
    void deleteCourseByID(long id) throws ZNZException;

    List<Course> getCoursesForDepartment(Department department) throws ZNZException;
    Course getCourse(long id) throws ZNZException;

    Course getCourseByNameAndDepartmentId(String courseName, long departmentId) throws ZNZException;

    Course getCourseForYear(Year year) throws ZNZException;
}
