package wieik.znz.api.services;

import wieik.znz.api.domain.Moderator;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface ModeratorService {
    long createModerator(Moderator moderator) throws ZNZException;
    void updateModerator(Moderator moderator) throws ZNZException;
    void deleteModerator(Moderator Moderator) throws ZNZException;
    void deleteModeratorById(long id) throws ZNZException;

    List<Moderator> getModeratorForYear(Year year) throws ZNZException;
    Moderator getModerator(long id) throws ZNZException;

    boolean isStudentModeratorOfYear(Student student, Year year) throws ZNZException;
}
