package wieik.znz.api.services;

import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface YearService {
    long createYear(Year year) throws ZNZException;
    void updateYear(Year year) throws ZNZException;
    void deleteYear(Year year) throws ZNZException;
    void deleteYearById(long id) throws ZNZException;

    Year getYear(long id) throws ZNZException;
    List<Year> getYearsForCourse(Course course) throws ZNZException;
    List<Year> getYearsForStudent(Student student) throws ZNZException;
    List<Year> getAllYears() throws ZNZException;

    Year getYearBySemesterId(long id) throws ZNZException;

    long getYearForAnnouncementWithId(long id) throws ZNZException;
}
