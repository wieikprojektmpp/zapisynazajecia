package wieik.znz.api.services;

import wieik.znz.api.domain.Subject;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface TermService {
    long createTerm(Term term) throws ZNZException;
    void updateTerm(Term term) throws ZNZException;
    void deleteTerm(Term term) throws ZNZException;
    void deleteTermById(long id) throws ZNZException;

    Term getTerm(long id) throws ZNZException;
    List<Term> getTermsForSubject(Subject subject) throws ZNZException;
}
