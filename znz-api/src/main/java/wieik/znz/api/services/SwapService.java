package wieik.znz.api.services;

import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface SwapService {
    long createSwap(Swap swap) throws ZNZException;
    void updateSwap(Swap swap) throws ZNZException;
    void deleteSwap(Swap swap) throws ZNZException;
    void deleteSwapById(long id) throws ZNZException;

    Swap getSwap(long id) throws ZNZException;
    List<Swap> getSwapsForStudent(Student student) throws ZNZException;
    List<Swap> getSwapsForTerm(Term term) throws ZNZException;
    Swap getSwapForAllocation(Allocation allocation) throws ZNZException;

    void realizeSwapsForTerm(Term term) throws ZNZException;
}
