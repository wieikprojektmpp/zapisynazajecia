package wieik.znz.api.services;

import wieik.znz.api.domain.Argument;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface ArgumentService {
    long createArgument(Argument argument) throws ZNZException;
    void updateArgument(Argument argument) throws ZNZException;

    Argument getArgument(long id) throws ZNZException;

    List<Argument> getArgumentsForSemester(Semester semester) throws ZNZException;
}
