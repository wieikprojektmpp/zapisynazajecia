package wieik.znz.api.services;

import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface GroupService {
    long createGroup(Group group) throws ZNZException;
    void updatGroup(Group group) throws ZNZException;
    void deleteGroup(Group group) throws ZNZException;
    void deleteGroupById(long id) throws ZNZException;

    List<Group> getGroupsForStudent(Student student) throws ZNZException;
    List<Group> getGroupsForYear(Year year) throws ZNZException;
    Group getGroup(long id) throws ZNZException;
    void addStudentToGroup(Student student, Group group) throws ZNZException;
}
