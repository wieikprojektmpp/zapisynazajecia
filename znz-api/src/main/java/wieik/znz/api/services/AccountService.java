package wieik.znz.api.services;

import org.springframework.validation.Errors;
import wieik.znz.api.domain.Account;
import wieik.znz.api.domain.Student;
import wieik.znz.api.exceptions.ZNZException;

/**
 * Created by Sebastian on 2014-10-31.
 */
public interface AccountService {

    /**
     * Rejestracja uzytkownika
     * @param account konto (bez hasla)
     * @param password haslo
     * @param errors bledy
     * @return true jesli sukces, w przeciwnym przypadku false
     */
    boolean registerAccount(Account account, String password, Errors errors, Student student) throws ZNZException;
}
