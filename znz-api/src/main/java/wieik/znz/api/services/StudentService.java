package wieik.znz.api.services;

import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-16.
 */
public interface StudentService {
    long createStudent(Student student) throws ZNZException;
    void updateStudent(Student student) throws ZNZException;
    void deleteStudent(Student student) throws ZNZException;
    void deleteStudentById(long id) throws ZNZException;

    Student getStudent(long id) throws ZNZException;
    Student getStudentByUsername(String username) throws ZNZException;
    List<Student> getStudentsForGroup(Group group) throws ZNZException;
    List<Student> getStudentsForYear(Year year) throws ZNZException;

    boolean isInYear(Student student, Year year) throws ZNZException;

    int countStudentsForYear(Year year) throws ZNZException;

    Student getStudentForAllocation(Allocation a) throws ZNZException;
}
