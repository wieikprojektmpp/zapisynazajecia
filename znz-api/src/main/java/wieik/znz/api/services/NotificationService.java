package wieik.znz.api.services;

import wieik.znz.api.domain.Notification;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface NotificationService {
    long createNotification(Notification notification) throws ZNZException;
    void updateNotification(Notification notification) throws ZNZException;
    void deleteNotification(Notification notification) throws ZNZException;
    void deleteNotificationById(long id) throws ZNZException;

    List<Notification> getNotificationsForYear(Year year) throws ZNZException;
    Notification getNotification(long id) throws ZNZException;
}
