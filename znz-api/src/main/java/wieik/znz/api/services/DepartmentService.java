package wieik.znz.api.services;

import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface DepartmentService {
    long createDepartment(Department department) throws ZNZException;
    void updateDepartment(Department department) throws ZNZException;
    void deleteDepartment(Department department) throws ZNZException;
    void deleteDepartmentByID(long id) throws ZNZException;

    List<Department> getDepartmentsForSchool(School school) throws ZNZException;
    Department getDepartment(long id) throws ZNZException;
    Department getDepartmentByNameAndSchoolId(String name, long schoolId) throws ZNZException;

    Department getDepartmentForCourse(Course course) throws ZNZException;
}
