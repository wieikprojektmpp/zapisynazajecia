package wieik.znz.api.services;

import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface AllocationService {
    long createAllocation(Allocation allocation) throws ZNZException;
    Allocation getAllocation(long id) throws ZNZException;
    List<Allocation> getAllForTerm(Term term) throws ZNZException;
    List<Allocation> getAllForStudent(Student student) throws ZNZException;
    void updateAllocation(Allocation allocation) throws ZNZException;
    void deleteAllocation(Allocation allocation) throws ZNZException;
    void deleteAllocationById(long id) throws ZNZException;
}
