package wieik.znz.api.services;

import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface SchoolService {
    long createSchool(School school) throws ZNZException;
    void updateSchool(School school) throws ZNZException;
    void deleteSchool(School school) throws ZNZException;


    List<School> getAllSchools() throws ZNZException;
    School getSchool(long id) throws ZNZException;
    School getSchoolByName(String name) throws ZNZException;

    School getSchoolForDepartment(Department department) throws ZNZException;
}
