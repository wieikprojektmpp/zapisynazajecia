package wieik.znz.api.services;

import wieik.znz.api.domain.Application;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Term;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface ApplicationService {
    // uzytkownik zapisujujac sie zglasza wiele "zgloszen", wiec lepiej wyslac liste do zapisu niz skakac miedzy warstwami
    void createApplications(List<Application> applications) throws ZNZException;
    void updateApplications(List<Application> applications) throws ZNZException;

    List<Application> getApplicationsForTerm(Term term) throws ZNZException;
    List<Application> getApplicationForStudentInSemester(Student student, Semester semester) throws ZNZException;
    int coutApplicationForTerm(Term term) throws ZNZException;

    void deleteApplication(Application application) throws ZNZException;
    void deleteApplicationById(long id) throws ZNZException;
}
