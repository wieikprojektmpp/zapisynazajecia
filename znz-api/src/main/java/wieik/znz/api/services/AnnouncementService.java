package wieik.znz.api.services;

import wieik.znz.api.domain.Announcement;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface AnnouncementService {
    long createAnnouncement(Announcement announcement) throws ZNZException;
    void updateAnnouncement(Announcement announcement) throws ZNZException;
    void deleteAnnouncement(Announcement Announcement) throws ZNZException;
    void deleteAnnouncementById(long id) throws ZNZException;
    List<Announcement> getAnnouncementForYear(Year year) throws ZNZException;
    List<Announcement> getAnnouncementForStudent(Student student) throws ZNZException;
    Announcement getAnnouncement(long id) throws ZNZException;

    long getYearIdForAnnouncement(Announcement announcement) throws ZNZException;
}
