package wieik.znz.api.services;

import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Subject;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface SubjectService {
    long createSubject(Subject subject) throws ZNZException;
    void updateSubject(Subject subject) throws ZNZException;
    void deleteSubject(Subject subject) throws ZNZException;
    void deleteSubjectById(long id) throws ZNZException;

    Subject getSubject(long id) throws ZNZException;
    List<Subject> getSubjectsForSemester(Semester semester) throws ZNZException;
}
