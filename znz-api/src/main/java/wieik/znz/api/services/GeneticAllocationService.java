package wieik.znz.api.services;

import wieik.znz.api.domain.Semester;
import wieik.znz.api.exceptions.ZNZException;

/**
 * Created by Sebastian on 2015-01-19.
 */
public interface GeneticAllocationService {
    void startAllocation(Semester semester) throws ZNZException;
}
