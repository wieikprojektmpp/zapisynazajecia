package wieik.znz.api.services;

import wieik.znz.api.domain.SystemAnnouncement;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface SystemAnnouncementService {
    long createSystemAnnouncement(SystemAnnouncement announcement) throws ZNZException;
    void updateSystemAnnouncement(SystemAnnouncement announcement) throws ZNZException;
    void deleteSystemAnnouncement(SystemAnnouncement Announcement) throws ZNZException;
    void deleteSystemAnnouncementById(long id) throws ZNZException;
    List<SystemAnnouncement> getAllSystemAnnouncements() throws ZNZException;
    SystemAnnouncement getAnnouncement(long id) throws ZNZException;
}
