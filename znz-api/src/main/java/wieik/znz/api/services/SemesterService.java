package wieik.znz.api.services;

import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Term;
import wieik.znz.api.domain.Year;
import wieik.znz.api.exceptions.ZNZException;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public interface SemesterService {
    long createSemester(Semester semester) throws ZNZException;
    void updateSemester(Semester semester) throws ZNZException;
    void deleteSemester(Semester semester) throws ZNZException;
    void deleteSemesterById(long id) throws ZNZException;

    Semester getSemester(long id) throws ZNZException;
    Semester getNewestSemester(Year year) throws ZNZException;
    List<Semester> getSemestersForYear(Year year) throws ZNZException;

    long getSemesterIdByTerm(Term term) throws ZNZException;
}
