package wieik.znz.dao;


import wieik.znz.api.domain.Moderator;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface ModeratorDao extends Dao<Moderator> {
    List<Moderator> getModeratorsForYear(Year year);
}
