package wieik.znz.dao;

import java.util.List;

/**
 * Created by Sebastian on 2014-10-31.
 */
public interface Dao<T extends Object> {
    long create(T t);
    T get(long id);
    List<T> getAll();
    void update(T t);
    void delete(T t);
    void deleteById(long id);
    void deleteAll();
    long count();
    boolean exists(long id);
}
