package wieik.znz.dao;

import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface SchoolDao extends Dao<School> {
    School findByName(String nazwaUczelni);

    School getSchoolForDepartment(Department department);
}
