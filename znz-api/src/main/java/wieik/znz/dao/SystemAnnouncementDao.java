package wieik.znz.dao;

import wieik.znz.api.domain.SystemAnnouncement;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface SystemAnnouncementDao  extends Dao<SystemAnnouncement> {
}
