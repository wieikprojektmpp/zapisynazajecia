package wieik.znz.dao;

import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface CourseDao extends Dao<Course>{
    Course findByName(String nazwa);
    Course findByNameAndDepartmentId(String name, long departmentId);

    List<Course> getCoursesForDepartment(Department department);

    Course getCourseForYear(Year year);
}
