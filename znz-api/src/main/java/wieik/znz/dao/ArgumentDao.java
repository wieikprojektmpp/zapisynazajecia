package wieik.znz.dao;

import wieik.znz.api.domain.Argument;
import wieik.znz.api.domain.Semester;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface ArgumentDao extends Dao<Argument> {
    Argument findByName(String nazwa);

    List<Argument> getArgumentsForSemester(Semester semester);
}
