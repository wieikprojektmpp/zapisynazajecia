package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;
import wieik.znz.dao.SwapDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnSwapDao extends AbstractHbnDao<Swap> implements SwapDao {
    public List<Swap> getSwapsForStudent(Student student) {
        Query q = getSession().getNamedQuery("getSwapsForStudent");
        q.setParameter("studentId", student.getStudentId());
        return (List<Swap>) q.list();
    }

    public List<Swap> getSwapsForTerm(Term term) {
        Query q = getSession().getNamedQuery("getSwapsForTerm");
        q.setParameter("termId", term.getTerminId());
        return (List<Swap>) q.list();
    }

    public Swap getSwapForAllocation(Allocation allocation) {
        Query q = getSession().getNamedQuery("getSwapForAllocation");
        q.setParameter("allocationId", allocation.getPrzydzialId());
        return (Swap) q.uniqueResult();
    }

    public List<Swap> getSwapsForAllocation(Allocation allocation) {
        Query q = getSession().getNamedQuery("getSwapsForAllocation");
        q.setParameter("allocationId", allocation.getPrzydzialId());
        return (List<Swap>) q.list();
    }

    public List<Swap> getFreeToSwapsForTerm(Term term) {
        Query q = getSession().getNamedQuery("getFreeToSwapsForTerm");
        q.setParameter("termId", term.getTerminId());
        return (List<Swap>) q.list();
    }

    public List<Swap> getFreeFromSwapsForTerm(Term term) {
        Query q = getSession().getNamedQuery("getFreeFromSwapsForTerm");
        q.setParameter("termId", term.getTerminId());
        return (List<Swap>) q.list();
    }
}
