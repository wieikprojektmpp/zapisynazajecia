package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Notification;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.NotificationDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnNotificationDao extends AbstractHbnDao<Notification> implements NotificationDao {
    public List<Notification> getNotificationsForYear(Year year) {
        Query q = getSession().getNamedQuery("getNotificationsForYear");
        q.setParameter("yearId", year.getRokId());
        return (List<Notification>) q.list();
    }
}
