package wieik.znz.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;
import wieik.znz.dao.Dao;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2014-10-31.
 */
public abstract class AbstractHbnDao<T extends Object> implements Dao<T> {
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());
    @Autowired private SessionFactory sessionFactory;
    private Class<T> domainClass;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @SuppressWarnings("unchecked")
    private Class<T> getDomainClass() {
        if (domainClass == null) {
            ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
            this.domainClass = (Class<T>) thisType.getActualTypeArguments()[0];
        }
        return domainClass;
    }

    private String getDomainClassName() { return getDomainClass().getName(); }

    @Override
    public long create(T t) {
        Method method = ReflectionUtils.findMethod(getDomainClass(), "setDateCreated", new Class[]{Date.class});
        if (method != null) {
            try {
                method.invoke(t, new Date());
            } catch (Exception e) { }
        }

            Serializable s = getSession().save(t);

        //fixme: przydkie obejscie problemu
            try {
                Long l = (Long) s;
                return l;
            } catch (ClassCastException e) {
                return (Integer) s;
            }

    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(long id) {
        T t = (T) getSession().get(getDomainClass(), id);
        if (t != null)
            System.out.println("get T: " + t.toString());
        else
            System.out.println("get T: null");
        return t;
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        return getSession()
                .createQuery("select x from " + getDomainClassName() + " x")
                .list();
    }

    @Override
    public void update(T t) { getSession().update(t); }

    @Override
    public void delete(T t) {
        getSession().delete(t);
    }

    @Override
    public void deleteById(long id) {
        System.out.println("deleting id: " + id);
        T t = get(id);
        delete(t);
    }

    @Override
    public void deleteAll() {
        //TODO: ustawianie deleted na 1
    }

    @Override
    public long count() {
        return (Long) getSession()
                .createQuery("select count(*) from " + getDomainClassName())
                .uniqueResult();
    }

    @Override
    public boolean exists(long id) { return (get(id) != null); }
}