package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Announcement;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.AnnouncementDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnAnnouncementDao extends AbstractHbnDao<Announcement> implements AnnouncementDao {
    public List<Announcement> getAnnouncementsForYear(Year year) {
        Query q = getSession().getNamedQuery("findAnnouncementsByYearId");
        System.out.println("AAA: " + year.getRokId());
        q.setParameter("yearId", year.getRokId());
        List<Announcement> announcements =  (List<Announcement>) q.list();
        System.out.println("AAA: " + announcements.toString());
        return announcements;
    }

    public List<Announcement> getAnnouncementsForStudent(Student student) {
        return null;
    }

    public long getYearIdForAnnouncement(Announcement announcement) {
        Query q = getSession().getNamedQuery("getYearIdForAnnouncement");
        q.setParameter("announcementId", announcement.getOgloszenieId());
        return  ((Number)q.uniqueResult()).longValue();
    }
}
