package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Moderator;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.ModeratorDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnModeratorDao extends AbstractHbnDao<Moderator> implements ModeratorDao {
    public List<Moderator> getModeratorsForYear(Year year) {
        Query q = getSession().getNamedQuery("FindModeratorsForYearId");
        q.setParameter("yearId", year.getRokId());
        return (List<Moderator>) q.list();
    }
}
