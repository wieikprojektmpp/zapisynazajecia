package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.GroupDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnGroupDao extends AbstractHbnDao<Group> implements GroupDao {
    public List<Group> getGroupsForStudent(Student student) {
        Query q = getSession().getNamedQuery("FindGroupsForStudent");
        q.setParameter("studentId", student.getStudentId());
        return (List<Group>) q.list();
    }

    public List<Group> getGroupsForYear(Year year) {
        Query q = getSession().getNamedQuery("FindGroupsForYear");
        q.setParameter("yearId", year.getRokId());
        return (List<Group>) q.list();
    }

    public void addStudentToGroup(Student student, Group group) {
        Query q = getSession().getNamedQuery("addStudentToGroup");
        q.setParameter("studentId", student.getStudentId());
        q.setParameter("groupId", group.getGrupaId());
        q.executeUpdate();
    }
}
