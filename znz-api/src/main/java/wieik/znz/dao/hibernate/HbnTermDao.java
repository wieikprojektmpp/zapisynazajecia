package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Subject;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;
import wieik.znz.dao.TermDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-15.
 */
public class HbnTermDao extends AbstractHbnDao<Term> implements TermDao {
    public List<Term> getTermsForSubject(Subject subject) {
        Query q = getSession().getNamedQuery("getTermsForSubject");
        q.setParameter("subjectId", subject.getPrzedmiotId());
        return (List<Term>) q.list();
    }

    public Term getToTermForSwap(Swap swap) {
        Query q = getSession().getNamedQuery("getToTermForSwap");
        q.setParameter("swapId", swap.getChecZmianyId());
        return (Term) q.uniqueResult();
    }

    public Term getFromTermForSwap(Swap swap) {
        Query q = getSession().getNamedQuery("getFromTermForSwap");
        q.setParameter("swapId", swap.getChecZmianyId());
        return (Term) q.uniqueResult();
    }
}
