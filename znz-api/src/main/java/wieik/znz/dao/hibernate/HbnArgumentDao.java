package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Argument;
import wieik.znz.api.domain.Semester;
import wieik.znz.dao.ArgumentDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnArgumentDao extends AbstractHbnDao<Argument> implements ArgumentDao {

    @Override
    public Argument findByName(String nazwa) {
        Query q = getSession().getNamedQuery("FindArgumentByName");
        q.setParameter("nazwa", nazwa);
        return (Argument) q.uniqueResult();
    }

    public List<Argument> getArgumentsForSemester(Semester semester) {
        Query q = getSession().getNamedQuery("getArgumentsForSemester");
        q.setParameter("semesterId", semester.getSemestrId());
        return (List<Argument>) q.list();
    }
}
