package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import wieik.znz.api.domain.Account;
import wieik.znz.dao.AccountDao;

/**
 * Created by Sebastian on 2014-10-31.
 */
public class HbnAccountDao extends AbstractHbnDao<Account> implements AccountDao{
    private static final String UPDATE_PASSWORD_SQL =
            "update account set password = ? where username = ?";

    @Autowired private JdbcTemplate jdbcTemplate;
    @Autowired private PasswordEncoder passwordEncoder;

    @Override
    public void create(Account account, String password) {
        LOG.debug("Tworzenie konta");
        try {
            long id = create(account);
            System.out.println(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOG.debug("Aktualizacja hasla");
        String encPassword = passwordEncoder.encode(password);
        jdbcTemplate.update(UPDATE_PASSWORD_SQL, encPassword, account.getUsername());
    }

    @Override
    public Account findByUsername(String username) {
        Query q = getSession().getNamedQuery("findAccountByUsername");
        q.setParameter("username", username);
        return (Account) q.uniqueResult();
    }
}
