package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;
import wieik.znz.dao.DepartmentDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnDepartmentDao extends AbstractHbnDao<Department> implements DepartmentDao {

    @Override
    public Department findByNameAndSchoolId(String nazwa, long schoolId) {
        Query q = getSession().getNamedQuery("FindDepartmentByNameAndSchoolId");
        q.setParameter("nazwa", nazwa);
        q.setParameter("schoolId", schoolId);
        return (Department) q.uniqueResult();
    }

    public List<Department> getDepartmentsForSchool(School school) {
        Query q = getSession().getNamedQuery("FindDepartmentsForSchoolId");
        q.setParameter("schoolId", school.getUczelniaId());
        return (List<Department>) q.list();
    }

    public Department getDepartmentForCourse(Course course) {
        Query q = getSession().getNamedQuery("FindDepartmentByCourseId");
        q.setParameter("courseId", course.getKierunekId());
        return (Department) q.uniqueResult();
    }
}
