package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.*;
import wieik.znz.dao.StudentDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnStudentDao extends AbstractHbnDao<Student> implements StudentDao {
    public List<Student> getStudentsForYear(Year year) {
        Query q = getSession().getNamedQuery("findStudentsForYear");
        q.setParameter("rokId", year.getRokId());
        return (List<Student>) q.list();
    }

    public Student getByName(String firstName, String lastName) {
        Query q = getSession().getNamedQuery("findStudentByName");
        q.setParameter("imie", firstName);
        q.setParameter("nazwisko", lastName);
        return (Student) q.uniqueResult();
    }

    public Student getStudentByUsername(String username) {
        Query q = getSession().getNamedQuery("findStudentByUsername");
        q.setParameter("username", username);
        return (Student) q.uniqueResult();
    }

    public List<Student> getStudentsForGroup(Group group) {
        Query q = getSession().getNamedQuery("FindStudentsForGroup");
        q.setParameter("groupId", group.getRokId());
        return (List<Student>) q.list();
    }

    public Student getModerator(Moderator m) {
        Query q = getSession().getNamedQuery("getStudentModerator");
        q.setParameter("moderatorId", m.getStarostaId());
        return (Student) q.uniqueResult();
    }

    public int countStudentsForYear(Year year) {
        Query q = getSession().getNamedQuery("countStudentsForYear");
        q.setParameter("rokId", year.getRokId());
        return ((Number)q.uniqueResult()).intValue();
    }

    public Student getStudentForAllocation(Allocation a) {
        Query q = getSession().getNamedQuery("getStudentForAllocation");
        q.setParameter("allocationId", a.getPrzydzialId());
        return (Student) q.uniqueResult();
    }

    public Student getStudentForSwap(Swap swap) {
        Query q = getSession().getNamedQuery("getStudentForSwap");
        q.setParameter("swapId", swap.getChecZmianyId());
        return (Student) q.uniqueResult();
    }
}
