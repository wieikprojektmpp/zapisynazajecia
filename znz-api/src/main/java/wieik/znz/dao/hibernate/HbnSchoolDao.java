package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;
import wieik.znz.dao.SchoolDao;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnSchoolDao extends AbstractHbnDao<School> implements SchoolDao {

    @Override
    public School findByName(String nazwaUczelni) {
        Query q = getSession().getNamedQuery("findSchoolByName");
        q.setParameter("nazwaUczelni", nazwaUczelni);
        return (School) q.uniqueResult();
    }

    public School getSchoolForDepartment(Department department) {
        Query q = getSession().getNamedQuery("FindSchoolByDepartmentId");
        q.setParameter("departmentId", department.getWydzialId());
        return (School) q.uniqueResult();
    }
}
