package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Application;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Term;
import wieik.znz.dao.ApplicationDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnApplicationDao extends AbstractHbnDao<Application> implements ApplicationDao {
    public List<Application> getApplicationsForTerm(Term term) {
        Query q = getSession().getNamedQuery("getApplicationsForTerm");
        q.setParameter("termId", term.getTerminId());
        return (List<Application>) q.list();
    }

    public List<Application> getApplicationsForStudentInSemester(Student student, Semester semester) {
        Query q = getSession().getNamedQuery("getApplicationsForStudentInSemester");
        q.setParameter("studentId", student.getStudentId());
        q.setParameter("semesterId", semester.getRokId());
        return (List<Application>) q.list();
    }

    public int countForTerm(Term term) {
        Query q = getSession().getNamedQuery("countApplicationsForTerm");
        q.setParameter("termId", term.getTerminId());
        return ((Number)q.uniqueResult()).intValue();
    }
}
