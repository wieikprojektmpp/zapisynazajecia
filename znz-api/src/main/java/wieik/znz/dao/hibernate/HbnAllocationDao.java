package wieik.znz.dao.hibernate;


import org.hibernate.Query;
import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;
import wieik.znz.dao.AllocationDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnAllocationDao extends AbstractHbnDao<Allocation> implements AllocationDao {

    public List<Allocation> getAllForTerm(Term term) {
        Query q = getSession().getNamedQuery("findAllocationByTerm");
        q.setParameter("terminId", term.getTerminId());
        return (List<Allocation>) q.list();
    }

    public List<Allocation> getAllForStudent(Student student) {
        Query q = getSession().getNamedQuery("findAllocationByStudent");
        q.setParameter("studentId", student.getStudentId());
        return (List<Allocation>) q.list();
    }

    public Allocation getAllocationForSwap(Swap swap) {
        Query q = getSession().getNamedQuery("getAllocationForSwap");
        q.setParameter("swapId", swap.getChecZmianyId());
        return (Allocation) q.uniqueResult();
    }
}
