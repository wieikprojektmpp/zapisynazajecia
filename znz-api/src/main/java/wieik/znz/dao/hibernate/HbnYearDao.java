package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.YearDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnYearDao extends AbstractHbnDao<Year> implements YearDao {
    public List<Year> getYearsForCourse(Course course) {
        Query q = getSession().getNamedQuery("FindYearsForCourse");
        q.setParameter("courseId", course.getKierunekId());
        return (List<Year>) q.list();
    }

    public Year getYearForGroup(Group group) {
        Query q = getSession().getNamedQuery("FindYearsForGroup");
        q.setParameter("groupId", group.getGrupaId());
        return (Year) q.uniqueResult();
    }

    public Year getYearBySemesterId(long id) {
        Query q = getSession().getNamedQuery("getYearBySemesterId");
        q.setParameter("semesterId", id);
        return (Year) q.uniqueResult();
    }

    public long getYearIdForAnnouncementWithId(long id) {
        Query q = getSession().getNamedQuery("getYearForAnnouncementWith");
        q.setParameter("announcementId", id);
        return ((Year) q.uniqueResult()).getRokId();
    }
}
