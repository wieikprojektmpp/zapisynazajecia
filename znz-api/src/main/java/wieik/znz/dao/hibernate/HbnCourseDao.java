package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.CourseDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnCourseDao extends AbstractHbnDao<Course> implements CourseDao {

    @Override
    public Course findByName(String nazwa) {
        Query q = getSession().getNamedQuery("FindCourseByName");
        q.setParameter("nazwa", nazwa);
        return (Course) q.uniqueResult();
    }

    public Course findByNameAndDepartmentId(String name, long departmentId) {
        Query q = getSession().getNamedQuery("findCourseByNameAndDepartmentId");
        q.setParameter("nazwa", name);
        q.setParameter("wydzialId", departmentId);
        return (Course) q.uniqueResult();
    }

    public List<Course> getCoursesForDepartment(Department department) {
        Query q = getSession().getNamedQuery("findCourseByDepartmentId");
        q.setParameter("departmentId", department.getWydzialId());
        return (List<Course>) q.list();
    }

    public Course getCourseForYear(Year year) {
        Query q = getSession().getNamedQuery("FindCourseByYearId");
        q.setParameter("yearId", year.getRokId());
        return (Course) q.uniqueResult();
    }
}
