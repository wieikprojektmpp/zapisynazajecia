package wieik.znz.dao.hibernate;

import wieik.znz.api.domain.SystemAnnouncement;
import wieik.znz.dao.SystemAnnouncementDao;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnSystemAnnouncementDao extends AbstractHbnDao<SystemAnnouncement> implements SystemAnnouncementDao {
}
