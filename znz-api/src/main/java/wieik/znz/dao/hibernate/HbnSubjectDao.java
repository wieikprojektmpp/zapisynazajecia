package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Subject;
import wieik.znz.dao.SubjectDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnSubjectDao extends AbstractHbnDao<Subject> implements SubjectDao {
    public List<Subject> getSubjectsForSemester(Semester semester) {
        Query q = getSession().getNamedQuery("getSubjectsForSemester");
        q.setParameter("semesterId", semester.getSemestrId());
        return (List<Subject>) q.list();
    }
}
