package wieik.znz.dao.hibernate;

import org.hibernate.Query;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Term;
import wieik.znz.api.domain.Year;
import wieik.znz.dao.SemesterDao;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public class HbnSemesterDao extends AbstractHbnDao<Semester> implements SemesterDao {
    public Semester getNewestSemester(Year year) {
        Query q = getSession().getNamedQuery("getNewestSemester");
        q.setParameter("yearId", year.getRokId());
        return (Semester) q.uniqueResult();
    }

    public List<Semester> getSemestersForYear(Year year) {
        Query q = getSession().getNamedQuery("getSemestersForYear");
        q.setParameter("yearId", year.getRokId());
        return (List<Semester>) q.list();
    }

    public long getSemesterIdByTerm(Term term) {
        Query q = getSession().getNamedQuery("getSemesterByTerm");
        q.setParameter("termId", term.getTerminId());
        return ((Semester) q.uniqueResult()).getSemestrId();
    }
}
