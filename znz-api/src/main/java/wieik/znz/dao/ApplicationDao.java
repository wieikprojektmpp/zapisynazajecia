package wieik.znz.dao;

import wieik.znz.api.domain.Application;
import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Term;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface ApplicationDao extends Dao<Application> {
    List<Application> getApplicationsForTerm(Term term);
    List<Application> getApplicationsForStudentInSemester(Student student, Semester semester);
    int countForTerm(Term term);
}
