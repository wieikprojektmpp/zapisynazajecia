package wieik.znz.dao;

import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Term;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface SemesterDao extends Dao<Semester> {
    Semester getNewestSemester(Year year);

    List<Semester> getSemestersForYear(Year year);

    long getSemesterIdByTerm(Term term);
}
