package wieik.znz.dao;

import wieik.znz.api.domain.Semester;
import wieik.znz.api.domain.Subject;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface SubjectDao  extends Dao<Subject> {
    List<Subject> getSubjectsForSemester(Semester semester);
}
