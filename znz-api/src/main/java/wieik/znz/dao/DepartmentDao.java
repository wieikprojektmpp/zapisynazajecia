package wieik.znz.dao;

import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Department;
import wieik.znz.api.domain.School;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface DepartmentDao extends Dao<Department> {
    Department findByNameAndSchoolId(String nazwa, long schoolId);

    List<Department> getDepartmentsForSchool(School school);

    Department getDepartmentForCourse(Course course);
}
