package wieik.znz.dao;

import wieik.znz.api.domain.*;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface StudentDao extends Dao<Student> {
    List<Student> getStudentsForYear(Year year);
    Student getByName(String firstName, String lastName);

    Student getStudentByUsername(String username);

    List<Student> getStudentsForGroup(Group group);

    Student getModerator(Moderator m);

    int countStudentsForYear(Year year);

    Student getStudentForAllocation(Allocation a);

    Student getStudentForSwap(Swap swapTo);
}
