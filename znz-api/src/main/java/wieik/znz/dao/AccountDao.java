package wieik.znz.dao;

import wieik.znz.api.domain.Account;

/**
 * Created by Sebastian on 2014-10-31.
 */
public interface AccountDao extends Dao<Account> {
    void create(Account account, String password);
    Account findByUsername(String username);
}
