package wieik.znz.dao;

import wieik.znz.api.domain.Subject;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface TermDao extends Dao<Term> {
    List<Term> getTermsForSubject(Subject subject);

    Term getToTermForSwap(Swap swap);

    Term getFromTermForSwap(Swap swapTo);
}
