package wieik.znz.dao;

import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface AllocationDao extends Dao<Allocation> {
    List<Allocation> getAllForTerm(Term term);
    List<Allocation> getAllForStudent(Student student);

    Allocation getAllocationForSwap(Swap swapTo);
}
