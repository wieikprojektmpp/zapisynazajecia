package wieik.znz.dao;

import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface GroupDao extends Dao<Group> {
    List<Group> getGroupsForStudent(Student student);

    List<Group> getGroupsForYear(Year year);

    void addStudentToGroup(Student student, Group group);
}
