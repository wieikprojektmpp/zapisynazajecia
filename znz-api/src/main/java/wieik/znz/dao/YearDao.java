package wieik.znz.dao;

import wieik.znz.api.domain.Course;
import wieik.znz.api.domain.Group;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface YearDao extends Dao<Year> {
    List<Year> getYearsForCourse(Course course);

   Year getYearForGroup(Group group);

    Year getYearBySemesterId(long id);

    long getYearIdForAnnouncementWithId(long id);
/*    List<Year> getYearsForStudent(Student student);*/
}
