package wieik.znz.dao;

import wieik.znz.api.domain.Allocation;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Swap;
import wieik.znz.api.domain.Term;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface SwapDao extends Dao<Swap> {
    List<Swap> getSwapsForStudent(Student student);

    List<Swap> getSwapsForTerm(Term term);

    Swap getSwapForAllocation(Allocation allocation);

    List<Swap> getSwapsForAllocation(Allocation a1);

    List<Swap> getFreeToSwapsForTerm(Term term);

    List<Swap> getFreeFromSwapsForTerm(Term term);
}