package wieik.znz.dao;

import wieik.znz.api.domain.Notification;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface NotificationDao extends Dao<Notification> {
    List<Notification> getNotificationsForYear(Year year);
}
