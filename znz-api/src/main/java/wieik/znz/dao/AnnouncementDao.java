package wieik.znz.dao;

import wieik.znz.api.domain.Announcement;
import wieik.znz.api.domain.Student;
import wieik.znz.api.domain.Year;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-09.
 */
public interface AnnouncementDao extends Dao<Announcement> {
    List<Announcement> getAnnouncementsForYear(Year year);

    List<Announcement> getAnnouncementsForStudent(Student student);

    long getYearIdForAnnouncement(Announcement announcement);
}
