package wieik.znz.genetic.phenotype;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class PhSubject {
    private final long id;
    private List<PhTerm> phTerms;

    public PhSubject(long id) {
        this.id = id;
        phTerms = new LinkedList<PhTerm>();
    }

    public PhSubject(long id, List<PhTerm> phTerms) {
        this.id = id;
        this.phTerms = phTerms;
    }

    public long getId() {
        return id;
    }

    public List<PhTerm> getPhTerms() {
        return phTerms;
    }

    public void setPhTerms(List<PhTerm> phTerms) {
        this.phTerms = phTerms;
    }
}
