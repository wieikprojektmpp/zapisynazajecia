package wieik.znz.genetic.phenotype;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class PhAllocation {
    public static int selectType2Points(int selectType) {
        switch (selectType) {
            case 1: return 20;
            case 2: return 15;
            case 3: return 8;
            case 4: return 20;
            case 5: return 15;
            case 6: return 15;
            case 7: return 20;
            case 8: return 20;
            case 9: return 13;
            case 10: return 20;
            case 11: return 20;
            case 12: return 20;
            default: return 20;
        }
    }

    private long studentId;
    private int selectType;
    private int argPriority;

    public PhAllocation(long studentId, int selectType, int argPriority) {
        this.studentId = studentId;
        this.selectType = selectType;
        this.argPriority = argPriority;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public int getSelectType() {
        return selectType;
    }

    public void setSelectType(int selectType) {
        this.selectType = selectType;
    }

    public int getArgPriority() {
        return argPriority;
    }

    public void setArgPriority(int argPriority) {
        this.argPriority = argPriority;
    }

    public int happyness() {
        return selectType2Points(selectType) + argPriority;
    }

}
