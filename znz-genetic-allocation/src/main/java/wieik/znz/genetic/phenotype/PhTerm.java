package wieik.znz.genetic.phenotype;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class PhTerm {
    private long id;
    private List<PhAllocation> phAllocations;

    public PhTerm(long id) {
        this.id = id;
        phAllocations = new LinkedList<PhAllocation>();
    }

    public PhTerm(long id, List<PhAllocation> phAllocations) {
        this.id = id;
        this.phAllocations = phAllocations;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<PhAllocation> getPhAllocations() {
        return phAllocations;
    }

    public void setPhAllocations(List<PhAllocation> phAllocations) {
        this.phAllocations = phAllocations;
    }
}