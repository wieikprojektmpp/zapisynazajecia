package wieik.znz.genetic;

import wieik.projekt.mp.genetic.CreateStartPopulation;
import wieik.projekt.mp.genetic.Population;
import wieik.projekt.mp.genetic.StopCondition;
import wieik.znz.genetic.vo.SemesterVO;

/**
 * Created by Sebastian on 2015-01-15.
 */
public final class GeneticAllocation {
    private final CreateStartPopulation<ZNZPhenotype> createStartPopulation;
    private final StopCondition<ZNZPhenotype> stopCondition;
    private final Population<ZNZPhenotype> population;
    private final SemesterVO semesterVO;


    public GeneticAllocation(SemesterVO semesterVO, int maxIteration) {
        stopCondition = new ZNZStopCondition();
        createStartPopulation = new ZNZCreateStartPopulation();
        population = new Population<ZNZPhenotype>(stopCondition, createStartPopulation, maxIteration);
        this.semesterVO = semesterVO;
    }

    public ZNZPhenotype evolve() {
        return population.evolve().getGenotype().getPhenotype();
    }

}
