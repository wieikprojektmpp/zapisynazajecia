package wieik.znz.genetic;

import wieik.znz.genetic.phenotype.PhAllocation;
import wieik.znz.genetic.phenotype.PhSubject;
import wieik.znz.genetic.phenotype.PhTerm;
import wieik.znz.genetic.vo.SemesterVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sebastian on 2015-01-15.
 */
//TODO
public class ZNZPhenotype {
    private List<PhSubject> phSubjects;
    private Map<Long, Integer> studentsHappyness;
    private SemesterVO semesterVO;


    public ZNZPhenotype(List<PhSubject> phSubjects, List<Long> studentsIds, SemesterVO semesterVO) {
        this.phSubjects = phSubjects;
        studentsHappyness = new HashMap<Long, Integer>(studentsIds.size());
        for (Long id : studentsIds) {
            studentsHappyness.put(id, 0);
        }
        this.semesterVO = semesterVO;
    }

    /**
     * Metoda obliczajaca wartosc przystosowania dla fenotypu. Jest to srednia zadowolenia kazdego studenta minus odchylenie standardowe tego zadowolenia.
     * Chcemy zeby srednie zadowolenie bylo jak najwyzsze, ale tez zeby nie bylo studentow bardzo poszkodowanych.
     * @return wartosc przystosowania fenotypu
     */
    public double fitness() {
        for (PhSubject phSubject : phSubjects) {
            for (PhTerm phTerm : phSubject.getPhTerms()) {
                for (PhAllocation phAllocation : phTerm.getPhAllocations()) {
                    int a = studentsHappyness.remove(phAllocation.getStudentId());
                    a += phAllocation.happyness();
                    studentsHappyness.put(phAllocation.getStudentId(), a);
                }
            }
        }
        double average = 0.0;
        for (Map.Entry<Long, Integer> entry : studentsHappyness.entrySet()) {
            average += entry.getValue();
        }
        average /= studentsHappyness.size();

        double stdDev = 0;
        for (Map.Entry<Long, Integer> entry : studentsHappyness.entrySet()) {
            stdDev += (average-entry.getValue())*(average-entry.getValue());
        }
        stdDev /= studentsHappyness.size();
        stdDev = Math.sqrt(stdDev);

        return average-stdDev;

    }

    public List<PhSubject> getPhSubjects() {
        return phSubjects;
    }
}
