package wieik.znz.genetic.vo;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class SubjectVO {
    private final long id;
    private final List<TermVO> terms;
    private final int minStudents;
    private final int maxStudents;

    public SubjectVO(long id, List<TermVO> terms, int minStudents, int maxStudents) {
        this.id = id;
        this.terms = terms;
        this.minStudents = minStudents;
        this.maxStudents = maxStudents;
    }

    public long getId() {
        return id;
    }

    public List<TermVO> getTerms() {
        return terms;
    }

    public int getMinStudents() {
        return minStudents;
    }

    public int getMaxStudents() {
        return maxStudents;
    }
}
