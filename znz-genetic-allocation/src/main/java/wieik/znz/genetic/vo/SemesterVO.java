package wieik.znz.genetic.vo;

import java.util.List;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class SemesterVO {
    private final long id;
    private final List<Long> studentsIds;
    private final List<SubjectVO> subjects;

    public SemesterVO(long id, List<Long> studentsIds, List<SubjectVO> subjects) {
        this.id = id;
        this.studentsIds = studentsIds;
        this.subjects = subjects;
    }

    public long getId() {
        return id;
    }

    public List<Long> getStudentsIds() {
        return studentsIds;
    }

    public List<SubjectVO> getSubjects() {
        return subjects;
    }
}


