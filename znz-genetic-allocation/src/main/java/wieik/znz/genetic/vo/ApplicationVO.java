package wieik.znz.genetic.vo;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class ApplicationVO {
    private final long id;
    private final long studentId;
    private final int selectionType;
    private final int argPrioryty;

    public ApplicationVO(long id, long studentId, int selectionType, int argPrioryty) {
        this.id = id;
        this.studentId = studentId;
        this.selectionType = selectionType;
        this.argPrioryty = argPrioryty;
    }

    public long getId() {
        return id;
    }

    public long getStudentId() {
        return studentId;
    }

    public int getSelectionType() {
        return selectionType;
    }

    public int getArgPrioryty() {
        return argPrioryty;
    }
}
