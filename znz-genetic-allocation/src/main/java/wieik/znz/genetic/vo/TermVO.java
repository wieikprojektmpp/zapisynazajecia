package wieik.znz.genetic.vo;

import java.util.Date;
import java.util.List;

/**
 * Created by Sebastian on 2015-01-19.
 */
public class TermVO {
    private final long id;
    private final int day;
    private final Date starts;
    private final Date ends;
    private final int parityWeek;
    private final List<ApplicationVO> applications;

    public TermVO(long id, int day, Date starts, Date ends, int parityWeek, List<ApplicationVO> applications) {
        this.id = id;
        this.day = day;
        this.starts = starts;
        this.ends = ends;
        this.parityWeek = parityWeek;
        this.applications = applications;
    }

    public long getId() {
        return id;
    }

    public int getDay() {
        return day;
    }

    public Date getStarts() {
        return starts;
    }

    public Date getEnds() {
        return ends;
    }

    public int getParityWeek() {
        return parityWeek;
    }

    public List<ApplicationVO> getApplications() {
        return applications;
    }
}
