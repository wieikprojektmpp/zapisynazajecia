package wieik.znz.genetic;

import wieik.projekt.mp.genetic.Genotype;

/**
 * Created by Sebastian on 2015-01-15.
 */
//TODO: implementacje
public class ZNZGenotype extends Genotype {

    ZNZGenotype(ZNZPhenotype znzPhenotype) {
        super(znzPhenotype);
    }

    @Override
    public double fitnessFunction() {
        return 0;
    }

    @Override
    public Genotype crossover(Genotype genotype) {
        return null;
    }

    @Override
    public void mutate() {

    }
}
